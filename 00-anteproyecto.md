# Nota de Trabajo Final de Graduación

## Nombre del Alumno
Ricardo Gustavo Miranda

## Denominación del Trabajo
> Jesslet: Jess como un Servicio

## Nombre del Profesor Guía
Dra. Rosanna Costaguta

##Nombre de los Asesores
P.U. Aldo Roldán, Dra. Florencia Coronel

## Resumen
*Jess* (*Java Expert System Shell*) es un *shell* inspirado en *CLIPS* y
escrito en lenguaje Java, el cual brinda un entorno para la creación y ejecución
de *sistemas expertos basados en reglas de inferencias*. En el siguiente
proyecto se buscará mejorar la forma en la que la funcionalidad de este shell
puede ser implementada en otros sistemas y aplicaciones sobre diferentes
plataformas.

Para conseguir esto se especificará y programará un *servicio web* sobre Jess
con métodos bien definidos, los cuales permitirán gestionar las reglas de
inferencia y los hechos de un sistema experto, y ejecutarlo para recuperar sus
recomendaciones.

Debido a que estos sistemas han expandido sus dominios de aplicación, se hizo
necesario su acceso desde un conjunto más diverso de usuarios y dispositivos.
El hecho de ser implementado como servicio web, al estar basado en estándares
abiertos, significaría tener un alto grado de compatibilidad con un gran número
de soportes y plataformas. De esta manera se abriría un gran abanico de
posibilidades en términos de medios para acceder y manejar un sistema experto.

### Palabras Claves
Sistema Experto, Shell, Jess, Servicio Web

## Introducción
En cualquier sistema experto es posible identificar ciertos componentes comunes,
a saber: la base de conocimientos, *la base de hechos*, *el motor de
inferencias*, y *el motor de explicación* \[[GARCIA2004]\]. A partir de esta
identificación y separación de componentes nació el concepto de shell, un tipo
especial de sistema experto que se caracteriza por poseer una base de
conocimientos vacía. Los shells agilizan el desarrollo de los sistemas experto
al permitir concentrar los esfuerzos sobre la obtención y representación del
conocimiento \[[GARCIA2004], [FRIEDMAN2003], [FRIEDMAN2008]\].

Jess (Java Expert System Shell), un shell inspirado en CLIPS \[[FRIEDMAN2003],
[FRIEDMAN2008]\] y escrito en lenguaje Java, presenta un ambiente sobre una
consola de comandos para crear y ejecutar sistemas expertos basados en reglas de
inferencias.

La implementación de Jess entraña ciertas dificultades e inconvenientes, los
cuales pueden obstaculizar el desarrollo de un sistema experto debido a la
heterogeneidad de los ámbitos en los cuales estos son aplicados. La creación de
un sistema experto sobre Jess requerirá de conocimientos profundos de Java si
quieren conseguirse tareas más complejas, tales como acceso a redes o bases de
datos. Por otro lado, un serio problema puede presentarse si Jess debe
interactuar con o incorporarse dentro de un sistema no basado en la tecnología
Java (un sistema heredado, por ejemplo).

En esta propuesta, Jess será utilizado como cimiento para el desarrollo del
**Jesslet**, un servicio web con las funcionalidades de Jess, y susceptible de
ser accedido desde múltiples sistemas y plataformas. Los estándares abiertos que
sirven de base para los servicios web (XML, SOAP, WSDL) eliminan las barreras
propias de la reutilización e integración de componentes, permitiendo agregar
las prestaciones del shell Jess dentro de otro sistema de manera sencilla.

El Jesslet será desarrollado utilizando el lenguaje Java empleando la
metodología *Programación Extrema*, adaptándola mediante ciertos patrones
propios del desarrollo de arquitecturas orientadas a servicios. Además,
utilizando Jess, se desarrollará un sistema experto basado en el calendario
oficial de vacunaciones, para la Ministerio de Salud y Desarrollo Social de la
Provincia de Santiago del Estero. Este sistema experto recomendará a sus
usuarios las vacunas que deberían aplicarse considerando las inmunizaciones
previamente recibidas. Para validar el correcto funcionamiento del servicio web
desarrollado, también se programarán aplicaciones clientes para demostrar cómo
se puede trabajar en un mismo sistema experto desde diferentes plataformas
(web, escritorio, móvil, etc.).

## Planteamiento y Formulación del Problema
Como consecuencia de estar escrito sobre Java, Jess puede ser implementado y
distribuido sobre una gran cantidad de plataformas. Una de las ventajas más
valoradas que posee Java es justamente su ubicuidad. Es esta característica,
innata de Jess, la cual lo hace idóneo para su trabajo con la gran cantidad
de ámbitos en las que deben implementarse los sistemas expertos. Encargados de
sistemas eléctricos \[[JAIN2008]\], bioquímicos  \[[ZHANG2000], [NAKAI2004]\], y
médicos \[[VELICER1999]\] han pasado a ser algunos de los nuevos operadores, y
sus tablets, teléfonos, laptops, y computadoras corporales (Google Glass, por
ejemplo) los nuevos dispositivos de operación.

La implementación de Jess puede hacerse de dos maneras:

1. Como un desarrollo directo sobre el shell utilizando el lenguaje Jess.
2. Utilizar a Jess como una librería externa desde una aplicación Java.

Sin embargo, ambas aproximaciones presentan ciertos retos, los cuales pueden
complicarse aún más dada la amplia variedad de dominios de aplicación en donde
aparecen los sistemas expertos como se dijo en un párrafo previo.
Realizar un desarrollo directo sobre Jess demanda, además de conocimiento sobre
Jess y su lenguaje, contar con experiencia en Java y sus librerías para poder
crear una funcionalidad con cierto grado de complejidad (acceso a una base de
datos, archivos, redes, componentes visuales, y otros requerimientos comunes en
la actualidad). En esta situación, la carencia de un entorno de desarrollo (IDE)
con soporte al shell en esta situación puede llevar a demoras en el desarrollo
por errores de programación \[[FRIEDMAN2003], [FRIEDMAN2008]\].

Por otro lado, ser incorporado como un componente externo en una aplicación Java
sólo requeriría conocer su API, pero así el uso de Jess estaría limitado sólo a
sistemas creados sobre esta tecnología. Esto podría ser un gran problema si es
obligatorio el trabajo con un sistema ya existente escrito en otro lenguaje.
Asimismo, y a pesar de su ubicuidad, Java puede no estar disponible en el ámbito
donde debe ejecutarse el sistema experto (por ejemplo, debido a políticas de
seguridad). Además, puede ocurrir que el sistema necesite estar instalado en
varios puntos. Desafortunadamente, cualquier modificación o actualización en el
código deberá entonces ser replicada en todas las estaciones de operación; algo
que puede llegar a ser costoso dependiendo del volumen de operaciones.

El desarrollo del Jesslet propuesto para la creación y operación de sistemas
expertos garantiza que los sistemas resultantes sean accesibles y fácilmente
incorporados como componentes en otros sistemas, ya que estará asentado sobre
los estándares abiertos propios de los servicios web \[[BOUGUETTAYA2014],
[KALIN2013], [CERAMI2002], [SNELL2001]\]. Además, se posibilitar así un acceso
concurrente a la funcionalidad.

## Justificación
El exponer a Jess como un servicio web posibilitará a programadores y
administradores la implementación de las capacidades de los shells y los
sistemas expertos dentro otros sistemas y/o plataformas con poco esfuerzo.
La razón principal de esto es que no cabría la necesidad de instalar o contar
con software especial, o el aprendizaje de un nuevo lenguaje o sintaxis. Los
estándares  SOAP, XML y WSDL, enviando los datos en texto plano y usando HTTP
como protocolo de transporte, permiten un acceso independiente a los métodos
para administrar las reglas de inferencia, la base de hechos, y la operación del
sistema. Cualquier sistema podrá incorporar las ventajas de un sistema experto
teniendo como componente externo al Jesslet e invocando a sus métodos y
procedimientos \[[BOUGUETTAYA2014], [KALIN2013], [CERAMI2002], [SNELL2001]\].

Además, tratándose de un servicio centralizado, toda actualización que sea
realizada sobre éste, repercutiría de manera instantánea sobre los sistemas
clientes. De esta manera se eliminan las actualizaciones máquina por máquina.

Como una consecuencia indirecta, al simplificarse y ampliar la accesibilidad de
Jess desde un conjunto heterogéneo de dispositivos y plataformas, los operadores
podrán trabajar con interfaces de usuario más diversas y ricas. La forma de
comunicación por defecto con el usuario en Jess es la consola. Tratándose de un
servicio web consumible desde cualquier otro entorno, esta característica queda
obsoleta.

El prototipo del Jesslet obtenido al finalizar este trabajo:

* Podrá ser incorporado o utilizado dentro de otro sistema de manera sencilla y
sin la necesidad de contar con software o componentes especiales.
* Posibilitará el trabajo con Jess y los sistemas expertos creados con éste
desde diferentes plataformas, sistemas y dispositivos de manera concurrente.
* Será un desarrollo abierto para su estudio y posible mejora posterior por
parte de estudiantes e investigadores.

La capacidad que presenta Jess de ser usado como una librería dentro de otra
aplicación Java es lo que lo hace adecuado para ser tomado como shell base para
el servicio web. Por otro lado, también puede afirmarse que su API
\[[FRIEDMAN2003], [FRIEDMAN2008]\] está adecuadamente documentada y es posible
obtener una licencia para uso académico con sólo solicitarla.

## Antecedentes
El único antecedente hallado en relación a la temática específica de proponer un
sistema experto basado en servicios web es el presentado por Chang y Hsieh
\[[CHANG2010]\]. Estos autores describen un sistemas ideado proponen un sistema
experto basado en servicios web para el diagnóstico de problemas en redes
inalámbricas. Los autores, además, establecen en su trabajo la importancia de
exponer a los sistemas expertos como un servicio. Sin embargo, en el presente
proyecto se irá un paso más allá al trabajar no sólo con los sistemas expertos,
sino con un shell en particular que podrá ser usado para la creación de los
mismo.

Por otra parte, los servicios web han sido ampliamente utilizados en diversos
campos, y sirven, en principio, para comunicar un sistema propio con software
externo o de terceros. Enumerar las implementaciones de estos servicios sería
imposible por los numerosas que resultan en la actualidad, pero entre algunas de
ellas se pueden mencionar las siguientes:

* Servicio web de la Administración Federal de Ingresos Públicos, que permite
realizar consultas de información (http://www.afip.gob.ar/ws/).
* Servicios web del Sistema Integrado de Información Sanitaria Argentino
(SIISA), desde donde puede hacerse consultas y cargar información referente a
pacientes, hospitales y establecimientos de salud, profesionales de la salud,
farmacias, etc. (https://sisa.msal.gov.ar/sisa/).
* Servicios web para la consulta y reserva de habitaciones en hoteles
(https://www.hoteldo.com/).
* Servicios web para la reserva de vuelos (http://www.amadeus.com/).

Además, y puntualmente, es posible afirmar que en el relevamiento bibliográfico
efectuado no se encontró ninguna implementación de Jess como un servicio web.

## Objetivos

### Objetivos Generales
1. Tender a facilitar el uso e implementación de las prestaciones de los
sistemas expertos dentro de otros sistemas y/o plataformas.

### Objetivos Específicos
1. Eliminar el acoplamiento no deseado entre Jess y otros sistemas al ser
utilizado como componente.
2. Incrementar la cohesión de Jess al integrarse dentro de otro sistema.

## Alcance
Los métodos que componen el contrato del Jesslet permitirán crear y ejecutar un
sistema experto. A su vez, proveerá de funciones que darán la posibilidad de
agregar, actualizar, eliminar y recuperar las reglas de inferencia y los datos
de estos sistemas. Este servicio web será programado utilizando el lenguaje
Java, e incorporará como un componente a la librería de Jess.

Una aplicación de escritorio cliente realizará todas las tareas antes
mencionadas sobre los sistemas expertos. La misma será programada usando el
lenguaje Python y sus librerías gráficas Tkinter. El objetivo principal de este
software es mostrar cómo se podría mejorar el trabajo de las personas encargadas
de ingresar en el shell el conocimiento experto disponible. Utilizando esta
aplicación se implementará el sistema de inmunizaciones del Ministerio de Salud
y Desarrollo Social.

Finalmente, se programará una extensión para el navegador Google Chrome, que
servirá de cliente para el sistema experto de inmunizaciones desarrollado. Esta
extensión se ejecuta sobre Javascript, y le permitirá al usuario buscar una
persona o beneficiario, consultar su historial de vacunaciones, y saber qué
vacunas le corresponden colocarse.

## Planificación del Proyecto
El trabajo estará principalmente dividido en cinco etapas:

### Primera Etapa. Investigación Preliminar
1. Estudiar las funcionalidades del shell Jess y su utilización para el
desarrollo de sistemas expertos.
2. Investigar metodologías, lineamientos y recomendaciones para el desarrollo de
servicios web.
3. Definir marco teórico.
4. Definir marco metodológico.
5. Definir marco empírico.

#### Técnicas y Herramientas
Shell Jess, mapas conceptuales, procesadores de texto, blog, redes sociales,
sistemas de almacenamiento de archivos en línea, editores de texto y código
fuente, XML, SOAP, WSDL.

### Segunda Etapa. Desarrollo del Jesslet
1. Especificar el servicio definiendo los métodos del contrato del servicio y
estableciendo los protocolos de comunicación que serán utilizados.
2. Programar la funcionalidad del servicio sobre Java incorporando a Jess como
una librería externa.
3. Crear el documento de ayuda y manual para el desarrollador. Éstos contendrán
la información necesaria para la correcta implementación de la interfaz sobre un
shell.
4. Desarrollar la aplicación cliente de escritorio que será utilizada para la
carga y gestión de las reglas de inferencia del sistema experto de
inmunizaciones a través del servicio web.
5. Desarrollar la extensión de Google Chrome cliente que podrá ejecutar el
sistema experto mediante el servicio web, y presentar las recomendaciones
correspondientes con cada consulta de usuario.

#### Técnicas y Herramientas
Jess, Mapas mentales y conceptuales, el entorno de desarrollo integrado
NetBeans, lJava, XML, SOAP, WSDL, herramientas para la prueba de servicios,
procesadores de texto, sistema de almacenamiento de archivos en línea, sistema
de control de versiones (SCV) Mercurial, aplicaciones o programas para la
creación de manuales de usuario o similar (por ejemplo, una Wiki), navegador
Google Chrome, Javascript, JSLint.

### Tercera Etapa. Prueba del Servicio
1. Crear un sistema experto de sugerencia de vacunas para el área de
Inmunizaciones del Ministerio de Salud y Desarrollo Social de la Provincia de
Santiago el Estero.
2. Implementar el sistema experto en Jess y utilizar la aplicación cliente
(creada en la etapa anterior) para cargar reglas.
3. Diseñar casos de prueba y experimentar con la extensión de Google Chrome
realizando diferentes consultas.

#### Técnicas y Herramientas
Metodologías de desarrollo de Sistemas Expertos (Ideal, Grover, KADS, etc.),
herramientas para el acceso a servicios (por ejemplo cURL), herramientas para la
prueba de servicios en línea, planillas de cálculos, tablas comparativas y
gráficos de resúmenes, sistemas de CVS Mercurial, el entorno de programación y
desarrollo NetBeans, Jess, Java, editores de texto y código fuente, XML, SOAP,
WSDL, navegador Google Chrome, Javascript, JSLint.

### Cuarta Etapa. Comunicación de resultados.
1. Mostrar los avances de la aplicación a través redes sociales y blogs.
2. Redactar documeto del Trabajo Final.
Algo que debe notarse sobre la última etapa de esta planificación es que no
representa una etapa secuencial como las anteriores, sino más bien una
transversal a las mismas. Es decir, será desarrollada a lo largo del proyecto y
no solamente cuando éste finalice.

#### Técnicas y Herramientas
Redes sociales, blogs y sitios de microblogging, sistema de CVS Mercurial en
BitBucket, procesador de texto.

## Cronograma de actividades

![Cronograma de Actividades](imgs/0_1_cronograma.png "Cronograma de
Actividades")

## Resultados Esperados
Finalizado el proyecto, se contará con un prototipo funcional del servicio web
Jesslet implementado sobre el shell Jess. Por otro lado, también se habrá
desarrollado una aplicación de escritorio que actúe como cliente principal del
prototipo y sirva de ejemplo base para futuros desarrollos, y un sistema experto
en vacunaciones. La extensión cliente se espera pueda ser luego utilizada como
complemento al Calendario Nacional de Vacunaciones oficial para consulta en
línea desde cualquier puesto sanitario o entidad efectora dependiente del
Ministerio de Salud y Desarrollo Social de la Provincia de Santiago del Estero.

## Referencias
1. \[[BOUGUETTAYA2014]\] Athman Bouguettaya, Quan Z. Sheng , Florian Daniel.
Advanced Web Services . Springer 2014. ISBN 978-1-4614-7535-4.
2. \[[CERAMI2002]\] Ethan Cerami. Web Services Essentials. O'Reilly 2002.
ISBN 0-596-00224-6.
3. \[[CHANG2010]\] Chung C Chang, Shih-Chieh Hsieh . A Wireless LAN Problem
Diagnosis Expert System Based on Web Services. 2010. International Symposium on
Parallel and Distributed Processing with Applications.
4. \[[FRIEDMAN2003]\] Ernest Friedman-Hill. Jess in Action: Ruled-Based Systems
in Java. Manning 2003. ISBN 1-930110-89-8.
5. \[[FRIEDMAN2008]\] Ernest Friedman-Hill. Jess The Rule Engine for the Java
Platform. Sandia National Laboratories.
6. \[[GARCIA2004]\] Ramón García Martínez, Paola Britos. Ingeniería de Sistemas
Expertos. 2004. Nueva Librería. ISBN:9789871104154.
7. \[[JAIN2008]\] Babita Jain. M, Amit Jain, M.B Srinivas. A Web based Expert
System Shell for Fault Diagnosis and Control of Power System Equipment. 2008.
International Conference on Condition Monitoring and Diagnosis.
8. \[[KALIN2013]\] Martin Kalin. Java Web Services: Up and Running.
O'Reilly 2013. ISBN 978-1-449-36511-0.
9. \[[NAKAI2004]\] Kenta Nakai, Dr. Minoru Kanehisa. Expert system for
predicting protein localization sites in gram-negative bacteria. 2004. Proteins:
Structure, Function, and Bioinformatics.
10. \[[SNELL2001]\] James Snell, Doug Tidwell , Pavel Kulchenko . Programming
Web Services with SOAP . O'Reilly 2001. ISBN 0-596-00095-2.
11. \[[VELICER1999]\] Wayne F. Velicer, James O. Prochaska, Jeffrey M. Bellis.
An expert system intervention for smoking cessation. 1999. US National Library
of Medicine.
12. \[[ZHANG2000]\] Wenzhu Zhang, Brian T. Chait. ProFound: An Expert System for
Protein Identification Using Mass Spectrometric Peptide Mapping
Information. 2000. American Chemical Society.

[JAIN2008]: http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=4580217&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D4580217 "Babita Jain. M, Amit Jain, M.B Srinivas. A Web based Expert System Shell for Fault Diagnosis and Control of Power System Equipment."

[ZHANG2000]: http://pubs.acs.org/doi/abs/10.1021/ac991363o "Wenzhu Zhang, Brian T. Chait. ProFound: An Expert System for Protein Identification Using Mass Spectrometric Peptide Mapping Information."

[FRIEDMAN2008]: http://www.jessrules.com/jess/docs/Jess71p2.pdf "Ernest Friedman-Hill. Jess The Rule Engine for the Java Platform."

[FRIEDMAN2003]: http://www.manning.com/friedman-hill/ "Ernest Friedman-Hill. Jess in Action: Ruled-Based Systems in Java."

[GARCIA2004]: http://www.cuspide.com/9789871104154/Ingenieria+De+Sistemas+Expertos/ "Ramón García Martínez, Paola Britos. Ingeniería de Sistemas Expertos."

[VELICER1999]: http://www.ncbi.nlm.nih.gov/pubmed/10223017 "Wayne F. Velicer, James O. Prochaska, Jeffrey M. Bellis. An expert system intervention for smoking cessation."

[BOUGUETTAYA2014]: http://www.springer.com/la/book/9781461475347 "Athman Bouguettaya, Quan Z. Sheng , Florian Daniel. Advanced Web Services."

[KALIN2013]: http://shop.oreilly.com/product/0636920029571.do "Martin Kalin. Java Web Services: Up and Running."

[CERAMI2002]: http://shop.oreilly.com/product/9780596002244.do "Ethan Cerami. Web Services Essentials."

[SNELL2001]: http://shop.oreilly.com/product/9780596000950.do "James Snell, Doug Tidwell , Pavel Kulchenko. Programming Web Services with SOAP."

[CHANG2010]: http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=5634349&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D5634349 "Chung C Chang, Shih-Chieh Hsieh . A Wireless LAN Problem Diagnosis Expert System Based on Web Services."

[NAKAI2004]: http://www.ncbi.nlm.nih.gov/pubmed/1946347 "Kenta Nakai, Dr. Minoru Kanehisa. Expert system for predicting protein localization sites in gram-negative bacteria."
