# Resumen

Este proyecto planteó el desarrollo de una **interfaz** orientada a servicios
(o **servicio web**), para el *shell **Jess***, mediante la cual sea posible la
creación, administración y ejecución de *sistemas expertos basados en reglas de
inferencias*. El objetivo principal de este software es el de mejorar la forma
en la que la funcionalidad de estos sistemas inteligentes puede ser implementada
en otros sistemas y aplicaciones. A partir de este objetivo, fueron planteadas
las hipótesis, las cuales enuncian que esta incorporación de un sistema experto
creado sobre el *shell Jess*, a través del servicio web, repercute con una
mejora en las métricas de **cohesión** y **acoplamiento**, 

El desarrollo del servicio web, denominado **Jesslet**, se realizó siguiendo las
recomendaciones propuestas por la metodología ágil *Programación Extrema*. Para
poder probar el funcionamiento de este nuevo software, se desarrolló un sistema
experto de ejemplo para la advertencia de aplicación de vacunas, el cual sirve
para indicar qué vacunas deben aplicarse en una fecha determinada, basándose en
la edad y el historial de inmunizaciones de una persona. A su vez, para
demostrar el funcionamiento del Jesslet desde múltiples plataformas, se
programaron dos aplicaciones clientes: una sobre *Python*, y la otra sobre
*Javascript* como una extensión del navegador web *Google Chrome*.

La contrastación de las hipótesis planteadas se hizo mediante pruebas
específicas para tal fin, obteniéndose mejoras sustanciales en la integración de
las capacidades inteligentes de un sistema experto sobre otros sistemas.
Puntualmente, se observó una notable reducción en el acoplamiento y en la
dependencia de componentes necesarios para la incorporación de la funcionalidad,
aunque, por otro lado, resultaron evidentes las limitaciones que el *Jesslet*
impone a las capacidades del _shell Jess_.

**Palabras Claves:** Sistema Experto, Shell, Jess, Servicio Web
