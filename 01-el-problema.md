# Formulación del Problema y Planteo de una Solución

Un __*shell*__ es un sistema experto carente de su base de conocimientos,
provisto de un *motor de inferencias*, interfaz de usuario, y un *trazador de
explicaciones*. El objetivo de un *shell* es la creación de sistemas expertos,
mediante la carga de conocimientos, con la capacidad de *"razonar"*. **Jess** es
un *shell* y un *entorno de programación* escrito en lenguaje *Java*, para el
desarrollo de sistemas expertos basados en reglas de inferencia
\[[FRIEDMAN2003], [FRIEDMAN2008]\].

Como consecuencia de estar escrito sobre Java, el *shell* Jess es susceptible de
ser implementado y distribuido sobre una variedad de plataformas, desde
computadoras personales hasta tecnología móvil. Justamente, una de las
cualidades más valoradas que posee Java es su capacidad de ser ejecutado en
diferentes sistemas operativos. Jess hereda esta característica, la cual lo
hace idóneo para enfrentar la creciente demanda de sistemas expertos en los más
diversos ámbitos. Encargados de sistemas eléctricos \[[JAIN2008]\], bioquímicos
\[[ZHANG2000], [NAKAI2004]\], y médicos \[[VELICER1999]\] han pasado a ser
algunos de los nuevos operadores, y sus tablets, teléfonos, laptops, y
computadoras corporales (los *smartwatch*, por ejemplo) los nuevos dispositivos
de operación.
 
La implementación de Jess puede hacerse de dos maneras:

1. Como un desarrollo directo sobre el shell utilizando el lenguaje propio de
Jess. 
2. Como una biblioteca externa desde una aplicación Java. 

Sin embargo, ambas aproximaciones presentan ciertos retos, los cuales pueden
complicarse aún más dada la amplia variedad de dominios de aplicación en donde
aparecen los sistemas expertos, como se mencionó previamente.

Realizar un desarrollo directo sobre Jess demanda, además de conocimiento sobre
el *shell* y su lenguaje, contar con experiencia en Java y sus bibliotecas para
poder crear una funcionalidad con cierto grado de complejidad. La mayor parte de
las aplicaciones actuales requieren acceso a base de datos, conexión a redes y
la utilización de una interfaz gráfica de usuario para la interacción con sus
operadores. En esta situación, se requeriría de un entorno de programación (IDE)
con soporte al *shell* para un proceso de desarrollo óptimo, pero en la
actualidad no existe ninguno. Pueden encontrarse diversos entornos que brindan
soporte para la creación de todo tipo de aplicaciones sobre Java (por ejemplo
Eclipse o Netbeans), aunque ninguno específico para Jess.

Por otro lado, ser incorporado como un componente externo en una aplicación Java
sólo requeriría conocer su API, pero así el uso de Jess estaría limitado sólo a
sistemas creados sobre esta tecnología. Esto podría ser un problema si lo
que se necesita es implementar tecnología de sistemas expertos en un sistema ya
existente escrito en otro lenguaje. Asimismo, y a pesar de su capacidad de ser
ejecutado en múltiples ambientes, Java puede no estar disponible en el ámbito
donde debe ejecutarse el sistema experto (por ejemplo, debido a políticas de
seguridad). Además, puede ocurrir que el sistema necesite estar instalado en
varios puntos. Con lo cual, cualquier modificación o actualización en el código
deberá entonces ser replicada en todas las estaciones de operación; algo que
puede llegar a ser costoso dependiendo del volumen de operaciones.

Este proyecto propone la creación de un servicio web (interfaz) entre el *shell*
Jess y las aplicaciones y sistemas que requieran incorporar su funcionalidad.
De esta forma, gracias a las características inherentes de los servicios web
\[[ver capítulo 4][servicios-web]\], se tiene un canal de comunicación que
abstrae y aísla la complejidad del *shell*. Esta interfaz (el **Jesslet**) hace
que estas *capacidades expertas* sean accesibles y de incorporación sencilla
como componentes en otros sistemas, ya que están asentadas sobre los estándares
abiertos propios de los servicios web \[[BOUGUETTAYA2014], [KALIN2013],
[CERAMI2002], [SNELL2001]\], dando la posibilidad, asimismo, de un acceso
concurrente a la funcionalidad.

## Justificación
La abstracción de Jess en un servicio web hace que la implementación de las
capacidades de los shells y los sistemas expertos dentro de otros sistemas se
realice con poco esfuerzo. Esto se debe, principalmente, a que se elimina la
necesidad de instalar o contar con software especial, o el aprendizaje de un
nuevo lenguaje o sintaxis. Los estándares sobre los que se asientan los
servicios web (SOAP, XML, WSDL y HTTP) son abiertos, y los datos se transmiten
en texto plano \[[BOUGUETTAYA2014], [KALIN2013], [CERAMI2002], [SNELL2001]\].
Así, se consigue que los métodos para la administración y ejecución de los
sistemas expertos sobre el shell, posean un alto grado de accesibilidad.

Además, tratándose de un servicio centralizado, toda actualización que sea
realizada sobre éste, repercute de manera instantánea sobre los sistemas
clientes. De esta manera, la depuración de errores e instalación de
actualizaciones en el software solo es necesaria en un único punto.

Como una consecuencia de la simplificación del acceso a Jess, se incrementa
notablemente el número de dispositivos para su operación. Los operadores pueden
trabajar con interfaces de usuario más diversas y ricas. 

El prototipo del Jesslet desarrollado en este trabajo:

* Puede ser incorporado o utilizado dentro de otro sistema de manera sencilla y
sin la necesidad de contar con software o componentes especiales. 
* Posibilita el trabajo con Jess y los sistemas expertos creados con éste
desde diferentes plataformas, sistemas y dispositivos de manera concurrente.
* Es un desarrollo abierto para su estudio y posible mejora posterior por
parte de estudiantes e investigadores. 

La capacidad que presenta Jess de ser usado como una biblioteca dentro de otra
aplicación Java es lo que lo hace adecuado para ser tomado como shell base para
el servicio web. Por otro lado, también puede afirmarse que su API está
adecuadamente documentada y es posible obtener una licencia para uso académico
con sólo solicitarla.

## Antecedentes 
Los servicios web han sido ampliamente utilizados en diversos campos, y sirven,
en principio, para interconectar dos o más sistemas o componentes de software.
Enumerar las implementaciones de estos servicios sería imposible por los
numerosos que resultan en la actualidad, pero entre algunas de ellas se pueden
mencionar las siguientes:

* Servicio web de la Administración Federal de Ingresos Públicos, que permite
realizar consultas de información (http://www.afip.gob.ar/ws/). 
* Servicios web del Sistema Integrado de Información Sanitaria Argentino
(SIISA), desde donde puede hacerse consultas y cargar información referente a
pacientes, hospitales y establecimientos de salud, profesionales de la salud,
farmacias, etc. (https://sisa.msal.gov.ar/sisa/). 
* Servicios web para la consulta y reserva de habitaciones en hoteles
(https://www.hoteldo.com/). 
* Servicios web para la reserva de vuelos (http://www.amadeus.com/).

A partir de la investigación bibliográfica realizada, se ha podido encontrar
algunas implementaciones de los sistemas expertos como servicios web. Por
ejemplo, en \[[CHANG2010]\] los autores proponen un sistema para el diagnóstico
de problemas en redes inalámbricas En este caso, se incorporan los servicios web
sobre sistemas expertos construidos para la detección de problemas en una LAN
inalámbrica, convirtiendo estos sistemas expertos en componentes estructurados
como servicios web.

Asimismo, existen implementaciones de sistemas expertos basados en web que
utilizan a *Jess* como base. En \[[HO2005]\] se introduce un sistema experto
basado en web denominado **planificador de programa de clases** (**CSP** por sus
siglas en inglés), el cual tiene el objetivo de brindar asistencia y ayudar con
la planificación de clases a estudiantes. Este desarrollo utiliza a **Jess**
como herramienta de procesamiento de hechos para la obtención de calendarios de
clases viables para los estudiantes. \[[KHADIR2009]\] presenta el diseño de un
sistema para el diagnóstico de fallas y mantenimiento en turbinas de vapor,
mientras que \[[MESTIZO2008]\] plantea el desarrollo de un sistema experto web
para brindar soporte técnico en línea a los usuarios del Sistema de Educación
Distribuida (EMINUS) de la Universidad Veracruzana. Ambos aplicativos utilizan
a Jess para realizar las inferencias y hacer sus recomendaciones. Finalmente,
\[[TRAPPEY2009]\] desarrolla un Sistema Médico Inteligente Móvil (MIMS por sus
siglas en inglés) que soporta aplicaciones médicas y de decisiones clínicas. En
dicho proyecto, Jess interactúa con aplicaciones móviles con tecnología RFID
para el monitoreo de instrumentos fisiológicos. Esta comunicación se realiza
mediante un módulo de recolección de datos (hechos), sobre el cual se pueden
emitir alertas y enviar mensajes de diagnóstico.

Sin embargo, en el presente proyecto se avanzó un paso más allá al trabajar no
sólo con los sistemas expertos, sino con un shell en particular que podrá ser
usado para la creación de los mismos. Las implementaciones de Jess encontradas
se diferencian del Jesslet creado en que plantean el desarrollo de un sistema
experto especialmente desarrollado para brindar una solución a un problema
puntual, mientras que el Jesslet permite la creación, administración y ejecución
de cualquier sistema experto cuyo conocimiento pueda ser representado con reglas
de inferencia.

## Objetivos
En base a la problemática arriba expuesta, se definió el objetivo general y los
objetivos específicos para guiar el desarrollo de este proyecto.

### Objetivo General
El objetivo general del trabajo es tender a facilitar el uso e implementación de
las prestaciones de los sistemas expertos dentro de otros sistemas y/o
plataformas. Es decir, esta orientado esencialmente a permitir que la
integración de funcionalidades tales como la creación y operación de sistemas
expertos se vea simplificada, independientemente del sistema operativo y del
lenguaje de programación sobre el cual se esté trabajando.

### Objetivos Específicos
Además se formularon dos objetivos específicos, a saber:

1. Eliminar el acoplamiento no deseado entre *Jess* y otros sistemas al ser
utilizado como componente.
2. Incrementar la cohesión de *Jess* al integrarse dentro de otro sistema.

## Resumen
En este capítulo se expuso la problemática detectada al momento de implementar
Jess, la solución propuesta, el enfoque tomado para el desarrollo del Jesslet,
su motivación, y porque resulta importante su abordaje. También se presentaron
brevemente los antecedentes encontrados respecto al uso del _shell_ Jess como un
servicio web o dentro de otro sistema. Por último, se plantearon los objetivos
definidos para la realización de este trabajo académico.

## Bibliografía

1. \[[BOUGUETTAYA2014]\] Athman Bouguettaya, Quan Z. Sheng , Florian Daniel.
Advanced Web Services . Springer 2014. ISBN 978-1-4614-7535-4.
2. \[[CERAMI2002]\] Ethan Cerami. Web Services Essentials. O'Reilly 2002.
ISBN 0-596-00224-6.
3. \[[CHANG2010]\] Chung C Chang, Shih-Chieh Hsieh . A Wireless LAN Problem
Diagnosis Expert System Based on Web Services. 2010. International Symposium on
Parallel and Distributed Processing with Applications.
4. \[[FRIEDMAN2003]\] Ernest Friedman-Hill. Jess in Action: Ruled-Based Systems
in Java. Manning 2003. ISBN 1-930110-89-8.
5. \[[FRIEDMAN2008]\] Ernest Friedman-Hill. Jess The Rule Engine for the Java
Platform. Sandia National Laboratories.
6. \[[GARCIA2004]\] Ramón García Martínez, Paola Britos. Ingeniería de Sistemas
Expertos. 2004. Nueva Librería. ISBN:9789871104154.
7. \[[HO2005]\] K. K. L. Ho, M. Lu. Web-based expert system for class schedule
planning using JESS. 2005. 2005 IEEE International Conference on Information
Reuse and Integration.
8. \[[JAIN2008]\] Babita Jain. M, Amit Jain, M.B Srinivas. A Web based Expert
System Shell for Fault Diagnosis and Control of Power System Equipment. 2008.
International Conference on Condition Monitoring and Diagnosis.
9. \[[KALIN2013]\] Martin Kalin. Java Web Services: Up and Running.
O'Reilly 2013. ISBN 978-1-449-36511-0.
10. \[[KHADIR2009]\] Mohamed Tarek Khadir,  Sihem Klai. A web-based fault
diagnostic maintenance system for steam turbines based on domain ontology and
expert system. 2009. 2009 International Conference on Multimedia Computing and
Systems.
11. \[[MESTIZO2008]\] Sonia Lilia Mestizo Gutiérrez, Alejandro Guerra Hernández,
Ramón Parra Loera. Desarrollo de un Centro de Ayuda Inteligente mediante el uso
de Tecnologías de Internet. Maestría en Inteligencia Artificial 2008. Veracruz,
México.
12. \[[NAKAI2004]\] Kenta Nakai, Dr. Minoru Kanehisa. Expert system for
predicting protein localization sites in gram-negative bacteria. 2004. Proteins:
Structure, Function, and Bioinformatics.
13. \[[SNELL2001]\] James Snell, Doug Tidwell , Pavel Kulchenko . Programming
Web Services with SOAP . O'Reilly 2001. ISBN 0-596-00095-2.
14. \[[TRAPPEY2009]\] Charles V. Trappey, C. S. Liu, Amy J. C. Trappey. Develop
Patient Monitoring and Support System Using Mobile Communication and Intelligent
Reasoning. 2009. Proceedings of the 2009 IEEE International Conference on
Systems, Man, and Cybernetics.
15. \[[VELICER1999]\] Wayne F. Velicer, James O. Prochaska, Jeffrey M. Bellis.
An expert system intervention for smoking cessation. 1999. US National Library
of Medicine.
16. \[[ZHANG2000]\] Wenzhu Zhang, Brian T. Chait. ProFound: An Expert System for
Protein Identification Using Mass Spectrometric Peptide Mapping
Information. 2000. American Chemical Society.

[JAIN2008]: http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=4580217&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D4580217 "Babita Jain. M, Amit Jain, M.B Srinivas. A Web based Expert System Shell for Fault Diagnosis and Control of Power System Equipment."

[ZHANG2000]: http://pubs.acs.org/doi/abs/10.1021/ac991363o "Wenzhu Zhang, Brian T. Chait. ProFound: An Expert System for Protein Identification Using Mass Spectrometric Peptide Mapping Information."

[FRIEDMAN2008]: http://www.jessrules.com/jess/docs/Jess71p2.pdf "Ernest Friedman-Hill. Jess The Rule Engine for the Java Platform."

[FRIEDMAN2003]: http://www.manning.com/friedman-hill/ "Ernest Friedman-Hill. Jess in Action: Ruled-Based Systems in Java."

[GARCIA2004]: http://www.cuspide.com/9789871104154/Ingenieria+De+Sistemas+Expertos/ "Ramón García Martínez, Paola Britos. Ingeniería de Sistemas Expertos."

[VELICER1999]: http://www.ncbi.nlm.nih.gov/pubmed/10223017 "Wayne F. Velicer, James O. Prochaska, Jeffrey M. Bellis. An expert system intervention for smoking cessation."

[BOUGUETTAYA2014]: http://www.springer.com/la/book/9781461475347 "Athman Bouguettaya, Quan Z. Sheng , Florian Daniel. Advanced Web Services."

[KALIN2013]: http://shop.oreilly.com/product/0636920029571.do "Martin Kalin. Java Web Services: Up and Running."

[CERAMI2002]: http://shop.oreilly.com/product/9780596002244.do "Ethan Cerami. Web Services Essentials."

[SNELL2001]: http://shop.oreilly.com/product/9780596000950.do "James Snell, Doug Tidwell , Pavel Kulchenko. Programming Web Services with SOAP."

[TRAPPEY2009]: http://ieeexplore.ieee.org/document/5345889/ "Develop patient monitoring and support system using mobile communication and intelligent reasoning"

[CHANG2010]: http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=5634349&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D5634349 "Chung C Chang, Shih-Chieh Hsieh . A Wireless LAN Problem Diagnosis Expert System Based on Web Services."

[NAKAI2004]: http://www.ncbi.nlm.nih.gov/pubmed/1946347 "Kenta Nakai, Dr. Minoru Kanehisa. Expert system for predicting protein localization sites in gram-negative bacteria."

[MESTIZO2008]: https://www.uv.mx/aguerra/documents/2008-mestizo.pdf "Desarrollo de un Centro de Ayuda Inteligente mediante el uso de Tecnologías de Internet"

[KHADIR2009]: http://ieeexplore.ieee.org/document/5256729/ "A web-based fault diagnostic maintenance system for steam turbines based on domain ontology and expert system"

[HO2005]: http://ieeexplore.ieee.org/document/1506468/ "Web-based expert system for class schedule planning using JESS"

[servicios-web]:04-servicios-web.md "Marco Tecnológico: Servicios Web"
