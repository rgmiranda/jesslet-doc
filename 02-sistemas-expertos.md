# Sistemas Expertos

Las diversas aplicaciones de los programas de computación y los sistemas de
información ha hecho que estos tomen contacto con un sin número de problemas
presentes en la vida cotidiana humana. Inicialmente enfocadas en el
procesamiento de grandes volúmenes de datos, las aplicaciones del software
han ido mutando e introduciéndose en terrenos cada vez más blandos y complejos.
La asistencia en la toma de decisiones estratégicas y la resolución de problemas
en donde la experiencia humana es de vital importancia son ahora algunos de los
problemas que deben ser atacados. Los sistemas expertos aparecen como una
aproximación para enfrentar este tipo de situaciones.

## ¿Qué son los sistemas expertos?
Los **sistemas expertos** son una aproximación a la solución de problemas
mediante conceptos vinculados con la **inteligencia artificial**.
\[[FEIGENBAUM1982]\] los define de la siguiente manera:

> Un sistema experto es un programa de computación inteligente que usa el
conocimiento y los procedimientos de inferencia para resolver problemas que son
los suficientemente difíciles como para requerir significativa experiencia
humana en su solución.

En otras palabras, es un sistema que emula la habilidad para tomar decisiones de
un humano sobre un problema dado \[[GIARRATANO1998]\].

Al formar parte del campo de la inteligencia artificial, los sistemas expertos
enfrentan dos dificultades en su implementación \[[GARCIA2004]\]:

1. Los seres humanos no saben realmente cómo realizan la mayoría de sus
actividades intelectuales.
2. Las computadoras enfrentan las tareas de manera distinta que un ser humano.
Esto se debe a que son programadas con lenguajes en los que sólo es posible
expresar conceptos muy elementales.

### Tipos de Conocimiento
Los conocimientos que manipula un sistema experto pueden ser clasificados de la
siguiente manera \[[GARCIA2004]\]:

1. **Conocimiento público**. Es el conocimiento que ha sido publicado y se
encuentra al alcance de toda la comunidad, por ejemplo en libros, revistas,
documentación y especificaciones técnicas.
2. **Conocimiento privado**. Es el conocimiento propio del o los expertos. Es
adquirido mediante el ejercicio de sus actividades, y se lo utiliza de manera
implícita. Suele ser un tipo de conocimiento que el especialista puede
verbalizar, ya que es consciente de su accionar.
3. **Metaconocimiento**. Es un tipo de conocimiento que tienen interiorizado los
expertos, adquiridos mediante la realización de sus actividades, pero que no
puede ser verbalizado ya que el experto no es consciente de poseerlo y
utilizarlo.

En este punto, cabe hacer notar una distinción entre los sistemas expertos y los
sistemas basados en conocimiento (SBC). Los primeros están basados en el
conocimiento y la experiencia humana en un determinado dominio, es decir, su
fuente de conocimientos es el experto. Mientras que los SBC utilizan
conocimientos públicos. Sin embargo, esta distinción ha perdido
fuerza desde que los sistemas expertos y SBC fueron desarrollados por primera
vez en los años setentas, volviéndose prácticamente sinónimos
\[[GIARRATANO1998]\].

### Ventajas de los Sistemas Expertos
Los sistemas expertos proporcionan **disponibilidad** de experiencia humana en
cualquier lugar en donde sea posible su instalación. Asimismo, su implantación
puede ser realizada a un **costo bajo**, en comparación con la capacitación y
preparación de recursos humanos para las tareas. Además, algunas de esas tareas
pueden ser demasiado peligrosas para una persona, por lo que un sistema experto
permite **reducir el peligro**. También posibilita tener **experiencia
permanente** dentro de la organización, ya que, a diferencia de los humanos,
los sistemas expertos no pueden retirarse, renunciar o morir. Otra gran ventaja
es que los sistemas expertos posibilitan la implementación de **múltiples
experiencias** de manera simultánea sobre un mismo problema.

Al tratarse de programas de computación pueden brindar una **explicación clara y
concisa** sobre el razonamiento utilizado para llegar a una determinada
conclusión. Por otro lado, darán **rápidas respuestas**, totalmente **basadas en
cálculos y carente de emociones**, cualidad que puede ser determinante cuando un
experto debe trabajar bajo mucha presión y fatiga \[[GIARRATANO1998]\].

## Componentes de un Sistema Experto
Los componentes elementales que pueden encontrarse en un sistema experto son
\[[GARCIA2004]\]:

1. La Base de Conocimientos
2. La Base de Datos
3. La Memoria de Trabajo
4. El Motor de Inferencia
5. El Trazador de Consultas
6. El Trazador de Explicaciones
7. El Manejador de Comunicaciones

La **base de conocimientos** contiene el conocimiento que el sistema experto
maneja. Es una formulación simbólica manipulable del área de conocimiento sobre
el cual el sistema es experto. Su construcción es un punto vital para el
correcto funcionamiento del sistema resultante, ya que éste será tan bueno como
su base de conocimientos. La función principal de esta base es proveer al motor
de inferencia de toda lo necesario para realizar el procesamiento.

Una de las formas para representar el conocimiento en un sistema experto es
mediante **reglas de inferencias** o **reglas de producción**. Estas reglas son
atribuidas a Newell y Simon, y aparece en su trabajo _Human Problem Solving_ en
el año 1972. Uno de los aportes más importantes de estos autores fue la
conclusión de que gran parte de la solución humana de problemas puede expresarse
mediante estas reglas de producción de la forma `SI ... ENTONCES ...`. Por
ejemplo:

~~~text
SI batería baja ENTONCES conectar cable de alimentación
~~~

Una regla corresponde a una pequeña colección modular de conocimiento llamada
fragmento. Los fragmentos se organizan con nexos hacia otros fragmentos con los
cuales presenten alguna relación. Newell y Simon popularizaron la representación
del conocimiento humano mediante reglas y mostraron cómo es posible razonar con
ellas. La idea de esto es que los estímulos recibidos por el cerebro mediante
los sentidos disparan reglas en la **memoria de largo plazo**, produciéndose una
respuesta apropiada. Esta memoria se compone de cientos de miles de reglas
\[[GIARRATANO1998]\].

La **base de datos**, también conocida como **base de hechos**, está formada por
distintos datos sobre el problema particular que el sistema experto intenta
resolver. El propósito principal es proveer de información al motor de
inferencia.

La **memoria de trabajo** es una base de datos temporal en la cual el motor de
inferencia almacena información inferida a partir de la base de conocimiento, la
base de datos, y la misma memoria de trabajo.

En el modelo de Newell y Simon, la **memoria a corto plazo**, o **memoria
activa**, se utiliza para almacenar datos e información temporal durante la
solución de un problema. Esta memoria, contrario a la de largo plazo, tienen una
capacidad sorprendentemente pequeña: entre cuatro y siete fragmentos
\[[GIARRATANO1998]\]. En Jess, al igual que en el trabajo de estos
investigadores, la base de hechos y la memoria de trabajo son lo mismo.

El **motor de inferencia** es el encargado de activar las reglas en función de
la información contenida en la base de datos y la memoria de trabajo. También se
encarga de proporcionar al trazador de explicaciones las reglas que se activaron
para producir una salida determinada.

El motor de inferencia puede trabajar bajo dos principios: *universo cerrado* o
*universo abierto*. El principio de **universo cerrado** establece que toda la
información necesaria está definida en la base de datos, por lo que todo aquello
que no puede demostrar como verdadero, lo supone falso. Bajo este principio, la
base de hechos no puede ser vacía. El principio del **universo abierto**
establece que la información necesaria no está contenida en el sistema, por lo
que debe comunicarse con el usuario para obtenerla.

Para realizar las inferencias, el motor de inferencia puede utilizar dos
estrategias: orientada por el objetivo (*backward chaining*), o bien orientada
por los datos (*forwardchaining*). La **estrategia orientada por el objetivo**
usa como origen de la inferencia un resultado posible, e intenta construir un
árbol hacia los datos conocidos, estando las reglas asociadas a las ramas del
mismo. La **estrategia orientada por los datos** parte de los datos, y a partir
de éstos intenta construir un conjunto que contenga como elemento al objetivo,
usando las reglas de la base de conocimiento.

En el contexto de la investigación de Newell y Simon, otro elemento importante
es el **procesador cognitivo**, cuya función principal es determinar qué reglas
serán activadas con el estímulo apropiado. Sin embargo, si en un determinado
momento hay varias reglas que deben ser activadas de manera simultánea, el
procesador cognitivo llevará a cabo una solución de conflicto para decidir cuál
posee la prioridad más alta. El motor de inferencia de los sistemas expertos se
corresponde con este procesador. El modelo de Newell y Simon para la solución
humana de problemas desde la perspectiva de la memoria a largo plazo (base de
conocimiento), memoria a corto plazo (base de datos y memoria de trabajo), y un
procesador cognitivo (motor de inferencia) es la base de los sistemas expertos
modernos basados en reglas de inferencia \[[GIARRATANO1998]\].

El **trazador de consultas**, en un sistema experto, organiza y presenta en una
forma entendible por el usuario los requerimientos de información. El trazador
de explicaciones interpreta los requerimientos del usuario acerca del porqué de
ciertas preguntas por parte del sistema, justificando las mismas. Ambos
trazadores sólo se utilizan con un motor de inferencia que funcione bajo el
principio del universo abierto.

El **manejador de comunicaciones** tiene dos funciones principales: derivar la
información inicial que suministra el usuario hacia la base de datos, e
interpretar los mensajes del usuario. Estos mensajes pueden ser respuestas del
usuario a una pregunta formulada por el sistema, o bien una solicitud de una
explicación a partir de una consulta del sistema.

## Recursos para el Desarrollo de Sistemas Expertos
Una de las decisiones más importantes al momento de definir un problema es en
qué profundidad debe ser modelado y el paradigma utilizado para hacerlo. En
general, una guía para seleccionar un paradigma es considerar inicialmente el
más tradicional, es decir, la programación convencional. Esto se debe a que la
experiencia con esta aproximación es mayor, además de que se dispone de una
variedad de paquetes comerciales para desarrollos. Si mediante la programación
convencional no puede solucionar eficazmente un problema, entonces se debe
considerar paradigmas alternativos, como la inteligencia artificial
\[[GIARRATANO1998]\].

Un lenguaje para el desarrollo de sistemas expertos es un lenguaje de más alto
nivel que otros, que permite realizar cosas con mayor facilidad, pero sólo ante
un número reducido de problemas. Así, por su naturaleza especializada, un
lenguaje de este tipo resulta idóneo para la creación de sistemas expertos, pero
no para la programación general.

La principal diferencia entre un lenguaje especializado y uno convencional es el
enfoque de la representación. Los lenguajes convencionales están enfocados en
brindar técnicas flexibles para poder representar y obtener una adecuada
**abstracción de datos**. Mientras que los lenguajes para los sistemas expertos
proporcionan herramientas para, además de la representación y abstracción de
datos, lograr la  **abstracción de conocimiento** \[[GIARRATANO1998]\].

Con la explosión comercial de los sistemas expertos, se han multiplicado los
recursos disponibles para desarrollarlos. Éstos pueden ser clasificados en
lenguajes, herramientas, o shells. Los **lenguajes** son traductores de comandos
escritos en una sintaxis bien definida. Un lenguaje para sistemas expertos
también proporcionará un motor de inferencia para ejecutar las instrucciones del
lenguaje. Una **herramienta** es un lenguaje adicionado con programas
utilitarios para facilitar el desarrollo y la depuración. Estos programas
utilitarios pueden incluir editores de texto, depuradores e incluso generadores
de código. También pueden proporcionarse ensambladores multiplataforma para
obtener software portable. Los **shells** son herramientas con propósitos
especiales, diseñadas para cierto tipo de aplicación en las que el usuario sólo
debe rellenar la base de conocimiento \[[GIARRATANO1998]\].

## Metodología de Desarrollo IDEAL
El desarrollo del **sistema experto** con el que se validará este trabajo, y que
se describirá en detalle más adelante, se hizo siguiendo los lineamientos de la
metodología **IDEAL** propuestos por \[[GARCIA2004]\]. A continuación se
presentan cada una de las fases junto con las etapas que las componen:

1. Fase I. Identificación de la Tarea
    * Etapa I.1: Plan de Requisitos y adquisición de conocimientos
    * Etapa I.2: Evaluación y selección de la tarea
    * Etapa I.3: Definición de la característica de la tarea
2. Fase II. Desarrollo de Prototipos
    * Etapa II.1: Concepción de la solución (descomposición en subproblemas y
    determinación de analogías)
    * Etapa II.2: Adquisición y conceptualización de los conocimientos
    * Etapa II.3: Formalización de los conocimientos y definición de la
    arquitectura
    * Etapa II.4: Implementación
    * Etapa II.5: Validación y evaluación del prototipo
    * Etapa II.6: Definición de nuevos requisitos y diseño
3. Fase III. Ejecución de la construcción del sistema integrado
    * Etapa III.1: Requisitos y diseño de la integración
    * Etapa III.2: Implementación y evaluación del sistema integrado
    * Etapa III.3: Aceptación del sistema por parte del cliente
4. Fase IV. Actuación para conseguir el mantenimiento perfectivo
    * Etapa IV.1: Definir el mantenimiento del sistema global
    * Etapa IV.2: Definir el mantenimiento de las bases de conocimientos
    * Etapa IV.3: Adquisición de nuevos conocimientos y actualización del
    sistema
5. Fase V. Lograr una adecuada transferencia tecnológica
    * Etapa V.1: Organizar la transferencia tecnológica
    * Etapa V.2: Completar la documentación del sistema experto construido

### Identificación de la Tarea
El principal objetivo de esta fase es la **definición de los objetivos de la
aplicación**, para así determinar si la tarea en cuestión es susceptible de ser
abordada mediante la ingeniería de conocimientos. De ser afirmativo, se definen
las características del problema y se especifican los requisitos que enmarcarán
la solución del problema.

La primera etapa busca **identificar las necesidades del cliente** mediante la
descripción de los objetivos del sistema. Se debe, además, determinar qué
funcionalidades se le exigirán, la información que será suministrada, y que
información debería ser devuelta.

La etapa de **evaluación y selección de tarea** es una suerte de "estudio de
viabilidad", en el cual se evalúa la tarea desde la perspectiva de la ingeniería
del conocimiento y se la cuantifica para determinar el grado de dificultad.

En la última etapa de esta fase se **establecen y definen las características
más relevantes asociadas al desarrollo de la aplicación**. Para esto, se debe
realizar una definición (lo más formal posible) del sistema, en donde se
incluyen:

- Requisitos funcionales
- Requisitos operativos
- Interfaz de usuario y con otros sistemas
- Soporte de hardware y software
- Criterios de éxito
- Casos de prueba
- Recursos y herramientas para el desarrollo
- Análisis de costo/beneficio
- Plan y calendario

### Desarrollo de Prototipos
Esta fase implica el desarrollo de los diferentes prototipos que permiten ir
definiendo y refinando más rigurosamente las especificaciones del sistema,
gradualmente hasta conseguir las especificaciones exactas de lo que se puede
hacer y cómo realizarlo.

La etapa de **concepción de la solución** tiene por finalidad producir un diseño
general del prototipo, en base  a la documentación obtenida en la fase anterior.

La **adquisición de los conocimientos** en la segunda etapa puede hacerse en dos
facetas: extracción de los **conocimientos públicos** (libros, documentos,
manuales, etc.), y educción de los **conocimientos privados** de los expertos.
La **conceptualización** consiste, esencialmente, en el entendimiento del
dominio del problema, y la terminología y conceptos utilizados. Para tal fin,
puede utilizarse mapas conceptuales y diccionarios con la terminología del
dominio de la aplicación. La adquisición y la conceptualización puede alternarse
cíclicamente para modelizar el comportamiento experto.

La etapa de **formalización** tiene como finalidad expresar los conocimientos
relativos al problema y su resolución en la forma de estructuras que puedan ser
usadas por una computadora. Esta etapa se divide en dos actividades. Por un
lado, se deben seleccionar los formalismos para poder representar en una
computadora la conceptualización obtenida en la etapa anterior, y por el otro,
se debe realizar un diseño detallado del sistema experto.

La **implementación** depende del formalismo elegido, junto con la herramienta
de desarrollo. Si se dispone de herramienta potente, se puede ahorrar mucho del
trabajo de programación. En otro caso, este tarea se realiza como en cualquier
otro sistema.

La **validación y evaluación** es uno de los puntos más sensibles y críticos.
Estos sistemas están construidos para contextos en donde, generalmente, las
decisiones son discutibles. Es posible comparar las respuestas de los expertos
con las del sistema para buscar discrepancias. Si las hay, se deberá refinar el
sistema.

La última etapa aparece por el desarrollo incremental de cada prototipo, y es
aquí donde se elicitan los requisitos del prototipo del ciclo siguiente.

### Ejecución de la Construcción del Sistema Integrado
La tercera fase abarca el estudio y diseño de interfaces con otros sistemas de
hardware y software, y el desarrollo e implementación de estas interacciones.
Finalmente se lleva a cabo la prueba final de aceptación por parte de los
expertos y usuarios finales, la cual debe satisfacer todas sus expectativas y
exigencias, tanto en lo relativo a su fiabilidad como eficiencia.

### Actuación para Conseguir el Mantenimiento Perfectivo
Esta fase trata sobre el mantenimiento del sistema. Debido a las características
de un sistema experto, el mantenimiento perfectivo es esencial, ya que no solo
puede incrementar las funcionalidades, sino que incorpora nuevos conocimientos.
Para este caso, se deben establecer los protocolos para captarlos, registrarlos,
e incorporarlos al sistema.

### Lograr una Adecuada Transferencia Tecnológica
Esta fase aborda el problema de la transferencia de manejo para una correcta
implementación y uso rutinario. Esto puede hacerse mediante la planeación y
organización meticulosamente la transferencia mediante entrenamiento en sesiones
de tutoría entre diseñadores y usuarios finales. También debe completarse la
documentación y los manuales de usuario para su correcto uso.

## Resumen
A lo largo de este capítulo se ha realizado una presentación sucinta sobre los
sistemas expertos, sus objetivos y propósitos. También se han visto los tipos de
conocimiento que estos pueden representar, y las tareas que los expertos
comúnmente desempeñan. Estas tareas son de interés por el hecho de que deben
luego ser realizadas por el sistema de una manera análoga a la realizada por el
humano. Se describieron los componentes esenciales de cualquier sistema experto,
el rol que desempeña cada uno, y cómo interactúan. Finalmente, se realizó una
presentación de la metodología IDEAL para el desarrollo de sistemas expertos,
sus fases y qué actividades se realizan en ellas.

## Bibliografía

1. \[[FEIGENBAUM1982]\] Edward A. Feigenbaum. Knowledge Engineering for the
1980s. 1982. Stanford University.
2. \[[GARCIA2004]\] Ramón García Martínez, Paola Britos. Ingeniería de Sistemas
Expertos. 2004. Nueva Librería. ISBN:9789871104154.
3. \[[GIARRATANO1998]\] Joseph Giarratano, Gary Riley. Sistemas Expertos:
Principios y Programación. 1998. Thomson. ISBN-13: 978-0878353354.

[GIARRATANO1998]: http://books.google.com.ar/books/about/Sistemas_expertos.html?id=ScS_QgAACAAJ "Joseph Giarratano, Gary Riley. Sistemas Expertos: Principios y Programación."
[GARCIA2004]: http://www.cuspide.com/9789871104154/Ingenieria+De+Sistemas+Expertos/ "Ramón García Martínez, Paola Britos. Ingeniería de Sistemas Expertos."
[FEIGENBAUM1982]: https://stacks.stanford.edu/file/druid:bf210yv9971/bf210yv9971.pdf "Knowledge Engineering for the 1980s"
