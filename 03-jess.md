# JESS

Jess es un lenguaje y motor de reglas de inferencias desarrollado sobre el
lenguaje **Java**, y basado, a su vez, en otro motor llamado **CLIPS** (escrito
en lenguaje *C*) \[[FRIEDMAN2003], [FRIEDMAN2008]\]. Aunque está inspirado y es
similar a este, Jess se diferencia esencialmente en su implementación. Su
tecnología subyacente lo convierte en una buena opción cuando se requiere
introducir o implementar las prestaciones de los sistemas expertos basados en
reglas de inferencia en aplicaciones sobre Java. Esto se debe a que Jess es en
sí un componente sobre esta plataforma, con lo cual puede acceder a un potente
conjunto de bibliotecas que permiten la conexión a bases de datos, redes e
interfaz gráfica de usuario. A lo largo de este capítulo se realizará una
presentación del mundo de Jess y algunas opciones que se pueden tomar para
implementarlo dentro de otros sistemas y aplicativos.

## Ejecución de Jess
Para poner a funcionar Jess es necesario contar con el archivo *JAR* (por sus
siglas en inglés, **J**ava **AR**chive) que lo contiene, y con la máquina
virtual de Java instalada. El archivo de Jess puede ser descargado desde la
dirección <http://www.jessrules.com/jess/download.shtml>.

En el fragmento que sigue se muestra un ejemplo de la ejecución de Jess mediante
una consola.

~~~bash
:~$ java -cp /directorio/de/jess.jar jess.Main 
Jess, the Rule Engine for the Java Platform
Copyright (C) 2008 Sandia Corporation
Jess Version 7.1p2 11/5/2008 
Jess> 
~~~

Para este ejemplo, se deben ingresar una a una las instrucciones en lenguaje
Jess, para luego ser interpretadas y ejecutadas por el motor.

~~~bash
Jess> (** 2 8) 
256.0 
Jess> (printout t "Ground Control to Major Tom!" crlf) 
Ground Control to Major Tom!
~~~

Sin embargo, el ingreso manual de todos los comandos y sentencias resulta
poco práctico ante programas que deben realizar diferentes acciones y contienen
varias reglas de inferencia. Por ello, Jess admite como parámetro opcional la
ubicación un archivo con código Jess, cuyas instrucciones serán ejecutadas luego
de la carga. Como muestra por ejemplo, el fragmento siguiente que se almacena en
un archivo llamado *main.clp*.

~~~lisp
; Contenido del archivo 'main.clp'
(bind ?var (* 2 3))
(printout t "This is Major Tom to Ground Control." crlf)
(printout t "I'm stepping through the door, ")
(printout t "and I'm floating in a most peculiar way." crlf)
(printout t "2x3 es " ?var crlf)
~~~

Para ejecutarlo, su ubicación es enviada como parámetro al invocar el shell.

~~~bash
:~$ java -cp /directorio/de/jess.jar jess.Main main.clp

Jess, the Rule Engine for the Java Platform
Copyright (C) 2008 Sandia Corporation
Jess Version 7.1p2 11/5/2008

This is Major Tom to Ground Control.
I'm stepping through the door, and I'm floating in a most peculiar way.
2x3 es 6
~~~

## El Lenguaje Jess
Jess, además de tratarse de un shell para la creación de sistemas expertos,
define un lenguaje para este fin. Al igual que *CLIPS*, la sintaxis del lenguaje
de Jess es similar a **LISP**, en donde las *listas* son la base para de
programación.

### Funciones
Como todo elemento en el lenguaje Jess, las llamadas a funciones también son
listas, en donde el primer elemento (**cabecera**) es el nombre de la función a
ser invocada, y los restantes elementos son sus parámetros, tal como muestra el
párrafo que continúa.

~~~lisp
(** 2 16)
(+ 232 898 45 45 12 45 2 -89 -5623 654 854)
(str-cat "¡HOLA " "MUNDO!")
~~~

Jess viene con un conjunto de *funciones predefinidas* que son de utilidad para
la creación de los programas (una lista completa de estas funciones puede
encontrarse en <http://www.jessrules.com/jess/docs/71/functions.html>). Sin
embargo, en muchos casos es necesaria la creación de funciones para propósitos
específicos del sistema que se pretende desarrollar. La construcción
`deffunction` permite justamente esto. Una vez que la función ha sido definida,
es posible utilizarla en el resto del software como a cualquier otra función. El
siguiente párrafo ejemplifica la creación de la función factorial utilizando
`deffunction`.

~~~lisp
(deffunction factorial (?n)                                    
    "Calcula el factorial de un número"
    (if (> ?n 0) then
        (return (* ?n (factorial (- ?n 1))))
    else
        (return 1))
)
(factorial 5) ; 5! = 120
(factorial 10) ; 10! = 3628800
~~~

### Hechos
Jess mantiene una colección de piezas de información denominadas *hechos*. Esta
colección se conoce como **memoria de trabajo**, y los datos que esta contiene
son utilizados por las reglas de inferencia durante la ejecución.

El shell ofrece un conjunto de funciones que permiten realizar las operaciones
básicas de creación, eliminación, y actualización de hechos sobre la memoria de
trabajo:

- `assert`: Agrega un hecho a la memoria de trabajo
- `clear`: Borra todos los hechos y reglas de inferencia
- `deffacts`: Establece los contenidos iniciales de la memoria de trabajo
- `facts`: Muestra los hechos actuales
- `reset`: Inicializa la memoria de trabajo
- `retract`: Borra un hecho puntual
- `watch facts`: Imprime mensajes de diagnóstico ante ciertos eventos

Los hechos definidos en Jess poseen atributos que se conocen como **ranuras**, y
cada tipo de hecho tiene un número fijo de estas. Estos tipos de hechos, junto
con las ranuras que admiten, se corresponden con  **plantillas**, las cuales se
definen mediante la función `deftemplate`. Si bien pueden ser de diferentes
tipos, como la mayoría de las estructuras de Jess, los hechos son almacenados
como *listas*.

### Reglas de Inferencia
La **base de conocimientos** está formada por la colección de reglas de
inferencia, las cuales ejecutan acciones dependiendo de los hechos que se
hallen en la base de hechos o memoria de trabajo. Una regla de inferencia se
asemeja a una sentencia `SI ... ENTONCES ..` (`if ... then`) en un lenguaje de
programación, pero en un sentido no procedimental. Mientras que, en contexto de
programación, estas construcciones se ejecutan en el orden determinado por el
programador, en Jess se disparan en el momento en el que son activadas.

Así como existen funciones y construcciones que permiten administrar los hechos,
el shell posee otras que lo hacen sobre las reglas:

- `defrule`: Define o actualiza una regla
- `undefrule`: Elimina una regla de inferencia
- `ppdefrule`: Imprime una regla en una forma amigable para el usuario
- `run`: Ejecuta el sistema
- `watch rules`: Muestra mensajes de diagnóstico cuando se dispara una regla
- `watch activations`: Muestra mensajes de diagnóstico cuando se activa una regla

En el [anexo A][anexo-a] puede encontrarse una especificación más detallada del
*lenguaje Jess*, su sintaxis, estructuras de control y las construcciones para
la administración de hechos y reglas de inferencia.

## Funcionamiento de Jess
El propósito de Jess, en breve, es la aplicación continua de instrucciones del
tipo `SI ... ENTONCES ...`, sobre el conjunto de datos que forman la memoria de
trabajo. Supóngase una regla como se muestra en el párrafo que sigue.

~~~txt
SI
    usuario admin ESTA ACTIVO
ENTONCES
    IMPRIMIR "El usuario está activo"
    ENVIAR CORREO ELECTRÓNICO admin
~~~

Esta regla se *activa* si existe en la memoria de trabajo un usuario *admin* en
un estado *activo*. En lenguaje Jess, esta se escribiría como aparece en el
siguiente párrafo.

~~~lisp
(
    defrule usuario-activo
    "Regla que imprime un mensaje cuando se halla un usuario activo"
    (usuario (nombre-usuario admin) (estado ACTIVO))
    =>
    (printout t "El usuario está activo" crlf)
    (enviar-email admin)
)
~~~

Las acciones que una regla ejecuta (el consecuente) pueden ser:
- **Funciones predeterminadas** de Jess
- **Funciones definidas por el usuario** mediante `deffunction`
- **Funciones definidas en Java**

De esta manera, el principal problema que Jess debe resolver es emparejar
algunos de los hechos presentes en la memoria de trabajo con los antecedentes de
las reglas de inferencias especificadas. Con esta finalidad, se debe ejecutar
perpetuamente un ciclo compuesto por las siguientes instrucciones:

1. Hallar todas las reglas satisfechas por el conjunto de hechos presentes en la
memoria de trabajo.
2. Formar *registros de activación* para estas asociaciones regla/hecho.
3. Optar por uno de estos registros para su ejecución.

Para que la búsqueda de estas activaciones de las reglas de inferencia sea
eficiente, Jess implementa el algoritmo **Rete**.

### El Algoritmo Rete
Este algoritmo es implementado mediante la construcción de una red
interconectada de **nodos**, en donde cada uno de ellos representa una o más
pruebas encontradas en los antecedentes de las reglas. Cada nodo posee una o dos
entradas, y un número variable de salidas. Los hechos que se van agregando o
quitando de la memoria de trabajo son procesados por esta red. Los nodos de
entrada se encuentran en la parte superior de la red, mientras que los de salida
se ubican en la inferior. En conjunto, forman la **red Rete**, la cual es la
implementación en sí de la memoria de trabajo en Jess.

En la parte superior de la red, los nodos de entrada separan los hechos en
categorías dependiendo de su cabecera (`usuario` en el ejemplo anterior), y a
medida que se desciende, se aplican discriminaciones más finas hasta llegar a
los nodos inferiores, los cuales representan las reglas en sí. Cuando un
conjunto de hechos atraviesa la red, significa que pasa todos los filtros
definidos en el antecedente de una regla particular, y debe convertirse en un
*registro de activación*.

## Jess como un Componente
A continuación se presentan dos ejemplos de interacción de Jess con otros
sistemas. En el primer ejemplo será utilizado desde un programa escrito en
lenguaje **Java**, mientras que en segundo ejemplo será utilizado como un
componente desde un programa en **Python**.

### Jess en Java
Para utilizar Jess **como un componente dentro de una aplicación Java** se debe
agregar el archivo *JAR* en el directorio de clases (*classpath*). Los módulos y
clases necesarios se encuentran dentro del *paquete*[1] `jess`. El fragmento
siguiente es un programa sencillo en Java que muestra cómo hacer esto.

~~~java
public class Main {
    public static void main(String[] args) {
        try {
            jess.Rete motor = new jess.Rete();
            motor.eval("(printout t \"HOLA MUNDO\" crlf)");
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex.getMessage());
        }
    }
}
~~~

En el ejemplo anterior, se crea un objeto del tipo `Rete` (el motor de
inferencias en sí) sobre el cual se va a ejecutar la función `printout` de Jess 
para mostrar `"HOLA MUNDO"` en la consola. Para ello se hace uso de la función
`eval` de la clase `Rete`, la cual interpreta y ejecuta instrucciones en
lenguaje Jess.

Otra forma de conseguir el mismo resultado de una manera más precisa, es emplear
la clase `Funcall` como aparece en el párrafo a continuación.

~~~java
public class Main {
    public static void main(String[] args) {
        try {
            jess.Rete motor = new jess.Rete();
            jess.Context context = motor.getGlobalContext();
            jess.Funcall f = new jess.Funcall("printout", motor);
            f.arg("t");
            f.arg("HOLA MUNDO");
            f.arg("crlf");
            f.execute(context);
         } catch (Exception ex) {
            System.out.println("ERROR: " + ex.getMessage());
        }
    }
}
~~~

Para ambos casos, esto se compila y ejecuta con las siguientes instrucciones:

~~~bash
$~ javac -cp /directorio/de/jess.jar Main.java -d .
$~ java -cp .:/directorio/de/jess.jar Main
HOLA MUNDO
~~~

### Jess en Otras Plataformas
Para poder utilizar Jess sobre otra plataforma que no se trate de Java se puede
optar entre dos aproximaciones:

1. Recurrir a una **interfaz de consola de comandos** (__CLI__, por sus siglas
en inglés) desde el entorno o plataforma de trabajo
2. Emplear bibliotecas o componentes que permitan acceder a las clases y módulos
presentes en el archivo *JAR*

La primera solución encuentra como desventaja el hecho de que el *JAR* que
contiene la biblioteca de Jess puede ejecutar código solamente **desde un
archivo**; luego, todo código *no Java* que requiera realizar una o más acciones
sobre el motor de inferencias, debe primero crear y escribir un archivo con las
líneas en *lenguaje Jess* correspondientes.

Un ejemplo del `HOLA MUNDO` anterior en lenguaje *Python* se vería como se
muestra a continuación:

~~~python
from subprocess import call
from tempfile import NamedTemporaryFile
import os
with NamedTemporaryFile() as archivo:
    archivo.write('(printout t "HOLA MUNDO" crlf)')
    archivo.flush()
    call(["java", "-cp", "/opt/jess/lib/jess.jar", "jess.Main", archivo.name])
~~~

Para esta situación, se utiliza un *archivo temporal* del sistema para
almacenar las instrucciones correspondientes. La ejecución de estas devuelve un
mensaje como el siguiente:

~~~bash
Jess, the Rule Engine for the Java Platform
Copyright (C) 2008 Sandia Corporation
Jess Version 7.1p2 11/5/2008

This copy of Jess will expire in 1336 day(s).
HOLA MUNDO
~~~

Sin embargo, la generación de un archivo cada vez que se necesite realizar algo
sobre Jess, indefectiblemente, repercutirá en el desempeño general de la
aplicación debido a la carga de operación de E/S.

Una solución quizás un tanto más eficiente (y elegante) es la segunda
alternativa, la cual implica hacer uso de herramientas (propias de la plataforma
en donde se quiere implementar Jess) que permitan acceder a un archivo *JAR*, y
consecuentemente, a todas las clases y paquetes de Jess. En Python existe (entre
otras) la biblioteca **JPype** (<https://pypi.python.org/pypi/JPype1>) que
posibilita esto. Usando esta biblioteca el ejemplo mostrado antes quedaría de la
siguiente forma:

~~~python
from jpype import *
startJVM(getDefaultJVMPath(), '-Djava.class.path=/directorio/de/jess.jar')
motor = JPackage('jess').Rete()
motor.eval('(printout t "HOLA MUNDO" crlf)')
shutdownJVM()
~~~

El resultado de este código es un tanto más escueto que el anterior:

~~~bash
HOLA MUNDO
~~~

Cabe agregar que, utilizando *JPype*, es posible tener un acceso más profundo a
la funcionalidad de la biblioteca de Jess que mediante la _CLI_.

###  Dificultades
El trabajo con Jess **directo desde la consola** encuentra como desafío inicial
el conocer con cierta profundidad el lenguaje de Jess. Y el tener que entender
un nuevo lenguaje cuando los planes y tiempos de desarrollo apremian puede
resultar un gran escollo. A su vez, otro gran reto puede encontrarse al momento
de la separación de las salidas y resultados. Debido a que todo se envía al
buffer de salida estándar, se deben establecer los mecanismos necesarios para
determinar si un resultado recibido es una recomendación, un mensaje de error,
o un mensaje de diagnóstico. Este inconveniente resulta particularmente evidente
al correr Jess en una *interfaz de consola* desde otras plataformas, en donde
además, al tratarse de un proceso separado, el control que se tiene sobre este
es prácticamente inexistente. Por estos motivos, esta forma de trabajo con Jess
no suele ser una alternativa viable o plausible para un sistema en producción.
El trabajo con la consola de comandos puede, no obstante, resultar aceptable en
un ambiente de pruebas y experimentación.

Al acceder a Jess como un componente se obtiene una gran flexibilidad en cuanto
a las acciones que se pueden realizar sobre el shell. El manejo de las clases
internas posibilita la creación de configuraciones más precisas y efectivas
de reglas de inferencia, condiciones y acciones, y además, obtener más detalles
en lo relativo a la ejecución del programa. No obstante, esta flexibilidad trae
consigo otro tipo de complejidad en el código resultante. Si bien por un lado se
pierde la carga del trabajo sobre el lenguaje Jess, por el otro aparece la
necesidad de entender el funcionamiento de la API de Jess y el uso correcto de
sus componentes. Esto, indudablemente, incrementa la cantidad de componentes y
módulos que se ven involucrados, lo cual repercute en el acoplamiento del código

La situación puede agravarse aún más si se trabaja desde una plataforma *no
Java*, debido a la implementación de herramientas y paquetes externos (*JPype*
en el ejemplo anterior), añadiendo así, más dependencias al sistema.

## Resumen
En este capítulo se ha realizado una introducción al shell Jess y sus
componentes. Asimismo, se ha referido a las formas en las que este puede
ejecutarse, como así también un breve análisis de las ventajas, desventajas y
desafíos que pueden encontrarse en ellas.

## Bibliografía
1. \[[FRIEDMAN2003]\] Ernest Friedman-Hill. Jess in Action: Ruled-Based Systems
in Java. Manning 2003. ISBN 1-930110-89-8.
2. \[[FRIEDMAN2008]\] Ernest Friedman-Hill. Jess The Rule Engine for the Java
Platform. Sandia National Laboratories.


[FRIEDMAN2008]: http://www.jessrules.com/jess/docs/Jess71p2.pdf "Ernest Friedman-Hill. Jess The Rule Engine for the Java Platform."
[FRIEDMAN2003]: http://www.manning.com/friedman-hill/ "Ernest Friedman-Hill. Jess in Action: Ruled-Based Systems in Java."
[anexo-a]: ./11-anexo-a-jess.md "Anexo A - Lenguaje Jess"
[1]:https://docs.oracle.com/javase/specs/jls/se7/html/jls-7.html "Un paquete Java es un contenedor que organiza las clases de Java en espacios de nombres, proveyendo uno (único) para cada tipo que contiene Permiten discriminar y agrupar las clases de acuerdo a su funcionalidad o cualquier otra clasificación arbitraria."
