# Servicios Web

Desde hace ya algún tiempo, las organizaciones advirtieron los beneficios que
devenían con la migración de sus negocios hacia la red. Tener un sitio web fue,
tal vez, el sello de una empresa tecnológicamente pujante en los 90. La nube es
otro paso en la evolución de la computación en las empresas y organizaciones, y
los **servicios web** son una de las tantas herramientas que la componen. Este
capítulo introducirá los conceptos básicos para el entendimiento de esta
tecnología y los principios sobre los cuáles se fundamenta.

## ¿Qué son los servicios web?
Un **servicio web** es un componente de aplicación que puede ser accedido por
otras aplicaciones a través de una red mediante una combinación de protocolos de
comunicación abiertos (como HTTP, XML, SMTP o Jabber) \[[W3SCHOOLSWS]\]. Se
trata de una unidad lógica autónoma, la cual puede evolucionar aisladamente,
manteniendo, a su vez, cierto de grado de estandarización para la comunicación
\[[EARL2005]\].

Las Figuras 4.1 y 4.2 presentan, en diagramas, la forma en la que trabaja un
servicio web. En su esencia, un servicio web funciona como una interfaz,
construida utilizando tecnologías estandarizadas de Internet y accesible a
través de una red, ubicada entre cierta **funcionalidad** y los **clientes** de
esta. Su rol principal es el de actuar como una capa de abstracción respecto a
la plataforma sobre la cual se ejecuta dicha funcionalidad. Los servicios web en
sí no representan nada revolucionario, sino que son más bien el resultado de la
evolución de los conceptos y principios que han gobernado Internet desde sus
comienzos \[[SNELL2001]\].

![Figura 4.1. Acceso a funcionalidad mediante un servicio web.](imgs/4_1_acceso_por_ws.png "Figura 4.1. Acceso a funcionalidad mediante un servicio web.")

![Figura 4.2. Los servicios web como una capa para abstraer la comunicación entre la aplicación y los clientes.](imgs/4_2_ws_como_capa_de_abstraccion.png "Figura 2. Los servicios web como una capa para abstraer la comunicación entre la aplicación y los clientes.")

La Figura 4.3 presenta los componentes generales de los **servicios web**. La
**aplicación** es quien contiene toda la lógica de negocios. El **demonio de
servicio** entiende los protocolos de transporte de Internet (HTTP, SOAP, XML,
etc.), y se encarga de escuchar y recibir los pedidos entrantes. El **proxy de
servicio** se encarga de codificar y decodificar los pedidos y las respuestas de
la aplicación para el demonio. Los **clientes** son los **usuarios** que
necesitan acceder a la funcionalidad de la aplicación. Para este caso puntual,
el concepto de *usuario* es utilizado en un sentido amplio, abarcando no solo a
una persona, sino también a otras aplicaciones y sistemas \[[SNELL2001]\].

![Figura 4.3. Componentes clave en un servicio web.](imgs/4_3_componentes_de_ws.png "Figura 4.3. Componentes clave en un servicio web.")

La relación entre un servicio web y el mundo exterior se realiza en base a una
**descripción** de este, la cual contiene, mínimamente, el nombre del servicio,
los datos que se esperan recibir y los que se deberían devolver. Como resultado
del uso de estas descripciones, el acceso a los servicios web resulta en una
relación denominada **débilmente acoplada** \[[EARL2005]\]. La herramienta
utilizada para especificarla es **WSDL** (*Web Services Description Language*)
\[[W3CWSDL]\], el cual establece cómo y dónde acceder al servicio.

El intercambio de información, en una relación de estas características, se
lleva a cabo mediante una plataforma de **mensajería**, la cual gestiona los
mensajes que emite y recibe un servicio web independientemente de este y de
otros mensajes \[[EARL2005]\]. El estándar **SOAP** (*Simple Object Access
Protocol*) \[[W3CSOAP]\] cumple con estos requisitos, y además posee una amplia
difusión en diferentes plataformas como protocolo de mensajes (para más
información sobre WSDL y SOAP, ver el [anexo B][anexo-b]).

### Características de los Servicios Web
Los servicios web presentan ciertas particularidades, algunas de las cuales han
resultado vitales para poder conseguir la abstracción de los pormenores del
*shell Jess*. En ese sentido, se pueden mencionar las siguientes como las más
sobresalientes:

- El **acoplamiento débil** hace que los servicios web se relacionen minimizando
las dependencias.
- Un **contrato de servicio** se desprende de las descripciones de los
servicios, el cual establece cómo se comunicará.
- Son **autónomos** en el sentido que controlan la lógica que abstraen.
- **Abstraen** dicha lógica y otros pormenores del mundo exterior.
- La abstracción de la funcionalidad permite la **reutilización**.
- Diferentes servicios pueden ser coordinados y ensamblados para crear otros
servicios **compuestos**.
- **Carecen de estado** al minimizar la información específica de una actividad.
- Son **susceptibles de ser descubiertos** mediante mecanismos específicos para
estos fines.

## Beneficios de los Servicios Web
El mayor beneficio que los servicios web aportan es el de la
**interoperabilidad**. La abstracción provista por las interfaces basadas en
estos hace que sea indistinto si la aplicación que brinda el servicio está
escrita en Python y su cliente en Javascript, o si una o la otra se ejecuta
sobre Solaris, Linux, Windows Phone o Android. Esta tecnología posibilita una
forma de comunicación en donde la plataforma se vuelve irrelevante. Por ejemplo,
una implementación de mucha utilidad puede encontrarse en los sistemas
administradores de bases de datos, sobre los cuales se trabaja con servicios web
para que funcionen como fuentes de datos con un alto grado de accesibilidad.

Esta interoperabilidad promueve un diseño netamente **reutilizable**. El hecho
de que los servicios sean accesibles hace que estos sean utilizados como partes
componentes de otros sistemas y servicios.

La aceptación y dispersión de las tecnologías de los servicios web ha generado
un beneficio para los **sistemas heredados**, dando la posibilidad de que estos
puedan ser incorporados a nuevas arquitecturas y sistemas de información. Un
servicio web, siendo una capa de comunicación, le permite a sistemas en
ambientes o plataformas aisladas, concretar el intercambio de información e
interoperación \[[EARL2005]\].

## Desafíos en la Implementación de los Servicios Web
La mayoría de los servicios web, al estar basados en estándares abiertos de
Internet, realizan el envío y la recepción de mensajes utilizando solo texto.
Esto puede generar sobrecargas de procesamiento, y, eventualmente, perjudicar el
desempeño general. En numerosos casos se deben transmitir datos complejos, como
por ejemplo estructuras y arreglos, y su codificación y decodificación puede
resultar muy costosa \[[KALIN2013]\].

En sus inicios, generalmente los servicios responden adecuadamente. Sin embargo,
a medida que se agrega más funcionalidad, también se ve incrementada la
comunicación basada en mensajes. En esta instancia suelen presentarse los
problemas de latencia y demoras debido a la sobrecarga de procesamiento. Para
prever este tipo de problemas se recomienda realizar pruebas de estrés sobre
los ambientes donde se han de ejecutar los servicios. Por otro lado, también
debería probarse la capacidad de la plataforma de mensajería, y explorar
alternativas para la optimización de su desempeño \[[EARL2005]\].

## Servicios Web en Java
**JAX-WS** es una API presente en Java, que tiene el propósito de simplificar la
creación y el consumo de servicios web. Para tal fin, se define un conjunto de
**anotaciones** [1] que permiten establecer qué es un servicio web, cuáles son
sus métodos, y generar el código de máquina correspondiente.

- `@WebService` marca una clase como un servicio web.
- `@WebMethod` marca un método público como un método de servicio dentro de una
clase marcada con la anotación anterior.

Cabe notar que la anotación `@WebMethod` es opcional, pero está recomendada. Por
defecto, en toda clase anotada con `@WebService`, sus métodos públicos son
considerados de servicio, aún cuando no se encuentran anotados como tales
\[[KALIN2013]\].

La publicación un servicio web puede hacerse de dos maneras:

1. Mediante un servidor web Java estándar (por ejemplo, Apache Tomcat).
2. Utilizado la clase `Endpoint` para crear publicaciones *ad-hoc*.

A continuación se presenta un ejemplo de una clase definida como un servicio, la
cual posee un único método para recuperar el *tiempo UNIX* en base a la fecha y
hora actuales.

~~~java
package testws;

import javax.jws.WebService;
import javax.jws.WebMethod;

@WebService
public class DateTimeService {
    @WebMethod
    public long Timestamp() {
        return System.currentTimeMillis() / 1000;
    }
}
~~~

La clase `Main`, que publicará el servicio, se define como:

~~~java
package testws;

import javax.xml.ws.Endpoint;
public class Main {
    public static void main(String[] args) {
        try {
            String url = "http://localhost:8888/tsws";
            System.out.println("Iniciando el servicio...");
            Endpoint end = Endpoint.publish(url, new DateTimeService());
            System.out.println("Servicio Iniciado...");
            System.out.println("Presione ENTER para cerrar...");
            System.in.read();
            System.out.println("Cerrando el servicio...");
            end.stop();
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex.getMessage());
        }
    }
}
~~~

Esto se compila y ejecuta como se muestra en el extracto que sigue.

~~~bash
:~$ javac testws/Main.java -d .
:~$ java testws.Main
Iniciando el servicio...
Servicio Iniciado...
Presione ENTER para cerrar...
~~~

En este punto, el servicio se encuentra en ejecución, con lo que si se realiza
un pedido a la URL `http://localhost:8888/tsws?wsdl` mediante **cURL**[2] o un
navegador web, se obtiene el documento **WSDL** del servicio generado:

~~~xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- Published by JAX-WS RI at http://jax-ws.dev.java.net. RI's version is JAX-WS RI 2.2.4-b01. -->
<!-- Generated by JAX-WS RI at http://jax-ws.dev.java.net. RI's version is JAX-WS RI 2.2.4-b01. -->
<definitions xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:wsp="http://www.w3.org/ns/ws-policy" xmlns:wsp1_2="http://schemas.xmlsoap.org/ws/2004/09/policy" xmlns:wsam="http://www.w3.org/2007/05/addressing/metadata" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://testws/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://schemas.xmlsoap.org/wsdl/" targetNamespace="http://testws/" name="DateTimeServiceService">
  <types>
    <xsd:schema>
      <xsd:import namespace="http://testws/" schemaLocation="http://localhost:8888/tsws?xsd=1"/>
    </xsd:schema>
  </types>
  <message name="Timestamp">
    <part name="parameters" element="tns:Timestamp"/>
  </message>
  <message name="TimestampResponse">
    <part name="parameters" element="tns:TimestampResponse"/>
  </message>
  <portType name="DateTimeService">
    <operation name="Timestamp">
      <input wsam:Action="http://testws/DateTimeService/TimestampRequest" message="tns:Timestamp"/>
      <output wsam:Action="http://testws/DateTimeService/TimestampResponse" message="tns:TimestampResponse"/>
    </operation>
  </portType>
  <binding name="DateTimeServicePortBinding" type="tns:DateTimeService">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" style="document"/>
    <operation name="Timestamp">
      <soap:operation soapAction=""/>
      <input>
        <soap:body use="literal"/>
      </input>
      <output>
        <soap:body use="literal"/>
      </output>
    </operation>
  </binding>
  <service name="DateTimeServiceService">
    <port name="DateTimeServicePort" binding="tns:DateTimeServicePortBinding">
      <soap:address location="http://localhost:8888/tsws"/>
    </port>
  </service>
</definitions>
~~~

Para una prueba sencilla, se genera el siguiente mensaje *SOAP* a los fines de
ser enviado a la ubicación donde fue publicado el servicio.

~~~xml
<?xml version="1.0"?>
<soap:Envelope
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    soap:encodingStyle="http://www.w3.org/2003/05/soap-encoding"
    xmlns:ns1="http://testws/">
    <soap:Body>
        <ns1:Timestamp></ns1:Timestamp>
    </soap:Body>
</soap:Envelope>
~~~

Al efectuar un pedido utilizando el verbo *POST* del protocolo *HTTP*, pasando
como datos el contenido del mensaje previamente generado, se obtiene la
siguiente respuesta desde el servicio.

~~~xml
<?xml version="1.0"?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
  <S:Body>
    <ns2:TimestampResponse xmlns:ns2="http://testws/">
      <return>1488722277</return>
    </ns2:TimestampResponse>
  </S:Body>
</S:Envelope>
~~~

Cabe notar que, al tratarse de un estándar abierto y ampliamente utilizado, la
mayoría de las plataformas y entornos de desarrollo manejan herramientas y
bibliotecas que permiten generar automáticamente estos mensajes SOAP. En
*Python* por ejemplo, se encuentra la biblioteca [*PySimpleSOAP*][pysimplesoap],
la cual permite utilizar el servicio desarrollado en Java desde este
lenguaje. La simplicidad de su uso queda evidenciada en el siguiente ejemplo.

~~~python
from pysimplesoap.client import SoapClient
c = pysimplesoap.client.SoapClient(wsdl = 'http://localhost:8888/tsws?wsdl')
print(c.Timestamp())
~~~

## Resumen
En esta etapa se ha realizado una introducción a los servicios web y sus
tecnologías fundamentales. A su vez, se ha presentado brevemente la forma en la
que estos pueden ser implementados y publicados en la plataforma Java, y
consumidos desde otras sistemas y plataformas.

## Bibliografía
1. \[[CERAMI2002]\] Ethan Cerami. Web Services Essentials. O'Reilly 2002.
ISBN 0-596-00224-6.
2. \[[KALIN2013]\] Martin Kalin. Java Web Services: Up and Running.
O'Reilly 2013. ISBN 978-1-449-36511-0.
3. \[[SNELL2001]\] James Snell, Doug Tidwell , Pavel Kulchenko . Programming
Web Services with SOAP . O'Reilly 2001. ISBN 0-596-00095-2.
4. \[[W3CSOAP]\] SOAP Specifications. W3C.
5. \[[W3CWSDL]\] Web Services Description Language (WSDL) 1.1 Specifications.
W3C.
6. \[[W3SCHOOLSWS]\] Web Services Tutorial. W3Schools.


[KALIN2013]: http://shop.oreilly.com/product/0636920029571.do "Martin Kalin. Java Web Services: Up and Running."
[EARL2005]:http://servicetechbooks.com/ctd "Thomas Earl. Service-Oriented Architecture: Concepts, Technology, and Design."
[CERAMI2002]: http://shop.oreilly.com/product/9780596002244.do "Ethan Cerami. Web Services Essentials."
[SNELL2001]: http://shop.oreilly.com/product/9780596000950.do "James Snell, Doug Tidwell , Pavel Kulchenko. Programming Web Services with SOAP."
[W3SCHOOLSWS]: http://www.w3schools.com/webservices/default.asp "W3Schools Web Services Tutorial"
[W3CWSDL]: http://www.w3.org/TR/wsdl "Web Services Description Language (WSDL) 1.1"
[W3CSOAP]: http://www.w3.org/TR/soap/ "SOAP Specifications"
[anexo-b]: ./12-anexo-b-soap.md "Anexo B: SOAP"
[pysimplesoap]: https://pypi.python.org/pypi/PySimpleSOAP/1.16 "PySimpleSOAP"

[1]: http://docs.oracle.com/javase/1.5.0/docs/guide/language/annotations.html "Una anotación de Java es una forma de añadir metadatos, al código fuente, que están disponibles para la aplicación en tiempo de ejecución. Es una forma de programación declarativa dentro de Java."
[2]: https://curl.haxx.se/ "cURL es una herramienta y biblioteca basada en línea de comandos para la transferencia de datos con URLs."
