# Métricas para la Cohesión y el Acoplamiento

Las dos hipótesis que marcan el rumbo de este proyecto son las siguientes:

1. *Utilizar el Jesslet reduce el **acoplamiento** entre Jess y otros sistemas
al ser implementado como un componente*
2. *Utilizar el Jesslet aumenta la **cohesión** de Jess al integrarse dentro de
otro sistema*

Para poder corroborar o refutar estas hipótesis se hizo uso de las métricas de
**cohesión** y **acoplamiento**, las cuales se presentan en el presente
capítulo, así también como la forma en que se realizarán las mediciones.

## La Cohesión y el Acoplamiento

A lo largo de la historia de la Ingeniería de Software, ha ido evolucionando un
conjunto de conceptos fundamentales acerca del diseño tales como la abstracción,
la arquitectura, los patrones, la modularidad, la independencia funcional, etc.,
los cuales deben tenerse en consideración si se pretende conseguir software de
alta calidad; sin importar el modelo de proceso que se elija, los métodos de
diseño que se apliquen o el lenguaje de programación que se utilice.

El término **acoplamiento** fue utilizado por primera vez dentro de la
Ingeniería de Software cuando la programación estructurada era la norma, y fue
definida como "*la medida de fuerza de la asociación establecida por una forma
de conexión de un módulo con otro*" \[[STEVENS1974], [ISO24765]\]. En el
contexto del diseño orientado a objetos, el acoplamiento se ve cómo el grado de
interconexión de una clase con otra, es decir que el acoplamiento indica la
dependencia de una respecto de otra. Una alta dependencia puede decrementar la
reutilización de la clase e incrementar el esfuerzo de mantenimiento, ya que los
cambios en una clase pueden afectar a las clases dependientes y, en
consecuencia, producirse un efecto en cascada. Así, cuanto más fuerte es el
acoplamiento entre los componentes de un sistema, más difícil resultará su
comprensión, modificación y corrección y, por lo tanto, más costoso su
mantenimiento.

Para reducir el acoplamiento, resulta necesario definir módulos con una alta
**cohesión**, la cual se la define "*como un indicador cualitativo del grado en
el que un módulo se centra en hacer una sola cosa*" \[[PRESSMAN2010],
[ISO24765]\]. De idéntica manera, el acoplamiento entre clases se puede reducir
promoviendo la cohesividad de las mismas. Una clase es cohesiva si la asociación
de los elementos declarados en la clase se centran en llevar a cabo una única
tarea.

## Métricas
Las métricas de software se han convertido en un elemento esencial al medir la
calidad de los proyectos de software. En especial, las métricas de acoplamiento
y de cohesión juegan un papel importante en la evaluación cuantitativa y la
mejora de los atributos de calidad internos de los productos de software.

A lo largo de la literatura se han introducido diferente conjuntos de métricas
tanto para la cohesión y el acoplamiento, siendo **LCOM4** y **CBO** las
métricas seleccionadas para medirlos en el presente trabajo. La importancia de
la selección de estas métricas queda evidenciada en \[[BRIAND2000]\], en donde
se analiza la relación entre estas y la **propensión al fallo** en las clases de
un programa. Puntualmente, se encuentra evidencia de que ante el **aumento del
acoplamiento**, o la **disminución de la cohesión** en una clase, el número de
fallos o errores encontrados en esta tiende a aumentar. De esta forma, resulta
patente que cualquier mejora que pudiera hacerse en este sentido, repercutirá
positivamente en la calidad de los sistemas resultantes.

### Métrica para la Cohesión de Clases
Para la medición de la cohesión de los componentes y clases su utilizará la
métrica **[LCOM4]** (*Lack of Cohesion of Methods*, *Carencia de Métodos
Cohesivos*), la cual es definida por \[[HITZ1995]\], tomando como base la
métrica LCOM previamente propuesta por \[[CHIDAMBER1991]\].

Esta métrica calcula el número de **componentes conectados** en una clase,
entendiéndose un componente conectado como un *conjunto de métodos y variables
de clase relacionados*. Como regla general, debería haber uno y sólo un
componente por clase. De haber dos o más, entonces la clase debería ser dividida
en tantas subclases como componentes hayan sido detectados.

Dos métodos, `A` y `B`, están relacionados si se cumple algunas de las
siguientes condiciones:

1. Ambos acceden o utilizan la misma variable de clase
2. `A` invoca a `B`, o `B` invoca a `A`

De esta manera, el valor `LCOM4` es igual a la cantidad de grupos de métodos
conectados.

- Si `LCOM4 = 1`, entonces la clase es cohesiva.
- Si `LCOM4 >= 2`, entonces indica un problema de que la clase tiene más de una
responsabilidapd y debería ser dividida.
- Si `LCOM4 = 0`, indica que la clase no contiene método alguno (no tiene
responsabilidad) y se trata de un contenedor de datos.

En la Figura 6.1 que aparece a continuación se presenta un ejemplo de un caso
con `LCOM4 = 1`, y otro con `LCOM4 = 2`. En esta Figura, los **métodos** se
representa con **rectángulos verdes**, y las **variables de la clase** mediante
**círculos azules**.

![Figura 6.1. Cálculo de LCOM4.](imgs/6_1_lcom4.png "Figura 6.1. Cálculo de LCOM4.")

Cabe notar que la presencia de un valor `LCOM4 >= 2` no necesariamente requiere
de una refactorización de la clase. Este indicador alerta a los programadores de
un posible problema en la clase. Queda a su criterio determinar si se trata o no
de error de diseño, y si merece o no ser resuelto.

### Métrica para el Acoplamiento entre Clases
La evaluación del acoplamiento entre clases de realizará mediante la métrica
**CBO** (*Coupling Between Object Classes*, *Acoplamiento Entre Objetos de
Clases*) \[[CHIDAMBER1991]\], la cual se obtiene mediante el conteo de las
clases que una clase utiliza o accede, sumado a la cantidad de clases que la
referencian a esta. Si las clases se referencian mutuamente, solo se cuenta una
sola vez. Como ejemplo, se presenta el diagrama de la Figura 6.2.

![Figura 6.2. Ejemplo para el Cálculo de CBO.](imgs/6_2_cbo.png "Figura 6.2. Ejemplo para el Cálculo de CBO.")

La Tabla 6.1 que aparece a continuación lista los valores de *CBO* obtenidos con
dicho cálculo.

Clase       |CBO
------------|---
`Foo`       |2
`Bar`       |2
`Baz`       |2
`Qux`       |3
`Quux`      |1

Un número bajo es una buena señal, mientras que un número alto un indicador de
peligro. El acoplamiento entre clases ha probado ser efectivo al momento de
predecir los fallos en los sistemas, y algunos estudios han demostrado que un
valor menor o igual a **9** representa el límite más eficiente para el *CBO*.

## Cálculo de las Métricas
Las métricas arriba presentadas están pensadas para ser aplicadas sobre una
clase puntual, por lo cual, en este proyecto, serán calculadas sobre las clases
utilizadas como *interfaces* para **Jess** y el **Jesslet**
([ver capítulo VII][jesslet]). La Figura 6.3 ilustra la situación descrita.

![Figura 6.3. Cálculo de las Métricas en el Proyecto.](imgs/6_3_calculo_metricas.png "Figura 6.3. Cálculo de las Métricas en el Proyecto.")

Una vez programadas las clases y superadas satisfactoriamente todas las pruebas
unitarias automatizadas, estos cálculos serán realizados de manera **manual**.
El motivo principal por el cual estos valores serán obtenidos de esta manera es
que no se ha podido encontrar una herramienta automática que arroje resultados
concluyentes o acordes a lo esperado.

En el caso de *LCOM4*, con estas herramientas, se obtiene un valor cero (`0`)
debido a que los métodos implementados mediante una anotación `@Override` son
simplemente ignorados. Con esto, la aplicación de un cálculo automatizado de
*LCOM4*, para este caso, resulta inviable.

Por otro lado, de acuerdo a la investigación realizada, la métrica *CBO* ha
sido "*interpretada*" de varias maneras desde su propuesta original por
\[[CHIDAMBER1991]\]. Por esta razón, muchas herramientas automáticas, habiendo
aplicado una interpretación arbitraria, arrojan valores distintos para la misma
clase.

## Resumen
En esta parte se han presentado las métricas utilizadas para evaluar el
resultado de la incorporación de las capacidades de un *sistema experto* dentro
de otro sistema mediante la implementación del *Jesslet*. Tanto **LCOM4**
(métrica para la *cohesión*) como **CBO** (métrica para el *acoplamiento*)
fueron presentadas mediante ejemplos sencillos, y luego se introdujo la forma en
la que estas serían luego utilizadas para analizar el impacto del *Jesslet*. La
importancia de estas mediciones está en que permiten obtener un resultado firme
y cuantitativo del impacto real del uso del servicio web en el diseño y
construcción de otro sistema.

## Bibliografía
1. \[[BRIAND2000]\] Lionel C.Briand, Jürgen Wüst, John W.Daly y D.Victor Porter.
Exploring the relationships between design measures and software quality in
object-oriented systems. 2000. Journal of Systems and Software, Vol: 51, No: 3,
PP: 245-273.
2. \[[CHIDAMBER1991]\] Shyam R. Chidamber y Chris F. Kemerer. Towards a Matrics
Suite for Object Oriented Design. 1991. Sloan School of Management, MIT.
3. \[[HITZ1995]\] Measuring coupling and cohesion in object oriented systems,
Proceedings of the International Symposium on Applied Corporate Computing, 1995,
pp. 25-27.
4. \[[ISO24765]\]: ISO/IEC/IEEE. ISO/IEC/IEEE 24765:2010. Systems and software
engineering -- Vocabulary. 2010.
5. \[[PRESSMAN2010]\]: Roger Pressman. Ingeniería del Software: Un Enfoque
Práctico. 2010. McGraw Hill. ISBN: 978-607-15-0314-5. Cap: 8, PP: 193.
6. \[[SHATNAWI2010]\] Raed Shatnawi. A Quantitative Investigation of the
Acceptable Risk Levels of Object-Oriented Metrics in Open-Source Systems. IEEE
Transactions on Software Engineering, Vol: 36, No: 2, PP: 216-225, March/April
2010, DOI:10.1109/TSE.2010.9
7. \[[STEVENS1974]\]: W. P. Stevens, G. J. Myers y L. L. Constantine. Structured
Design. 1974. IBM Systems Journal, Vol: 13, No: 2, PP: 115-139, 1974, DOI:
10.1147/sj.132.0115


[BRIAND2000]: http://www.sciencedirect.com/science/article/pii/S0164121299001028 "Exploring the relationships between design measures and software quality in object-oriented systems"
[CHIDAMBER1991]: http://www.pitt.edu/~ckemerer/CK%20research%20papers/TowardsMetricsSuiteForOODesign_ChidamberKemerer91.pdf "Towads a Metrics Suite for Object Oriented Design"
[HITZ1995]: http://www.isys.uni-klu.ac.at/PDF/1995-0043-MHBM.pdf "Measuring Coupling and Cohesion In Object-Oriented Systems"
[ISO24765]: https://www.iso.org/standard/50518.html "ISO/IEC/IEEE 24765:2010. Systems and software engineering -- Vocabulary"
[LCOM4]: http://www.aivosto.com/project/help/pm-oo-cohesion.html#LCOM4 "Lack of Cohesion Methods 4"
[PRESSMAN2010]: https://www.amazon.com/Software-Engineering-Practitioners-Approach-International/dp/0071267824 "Ingeniería del Software: Un Enfoque Práctico"
[SHATNAWI2010]: http://www.computer.org/csdl/trans/ts/2010/02/tts2010020216-abs.html "A Quantitative Investigation of the Acceptable Risk Levels of Object-Oriented Metrics in Open-Source Systems"
[STEVENS1974]: http://ieeexplore.ieee.org/document/5388187/ "Structured Design"


[jesslet]:07-jesslet.md "Desarrollo del Jesslet"