# Jesslet

En este capítulo se documenta el proceso de desarrollo del servicio web
**Jesslet**, durante el cual se especifica y programa un servicio web sobre Jess
con métodos bien definidos, que permiten gestionar las reglas de inferencia y
los hechos de un sistema experto, y ejecutarlo para recuperar sus inferencias.
Para dicho desarrollo se utiliza el enfoque propuesto por las prácticas de la
**Programación Extrema**, adaptado y ajustado al contexto de **un único
programador** ([ver capítulo V][xp]).

## Fase de Exploración
En esta primera fase, se procedió a establecer el alcance general del proyecto,
las herramientas, pautas, restricciones, etc. que se utilizaron a lo largo del
proceso, así también como las primeras historias de usuario necesarias para la
siguiente fase.

### Alcance
Para este trabajo el alcance del prototipo del Jesslet fue definido como sigue:

- Podrá ser incorporado o utilizado dentro de otro sistema de manera sencilla y
sin la necesidad de contar con software o componentes especiales.
- Posibilitará el trabajo con Jess y los sistemas expertos creados con éste
desde diferentes plataformas, sistemas y dispositivos de manera concurrente.
- Será un **desarrollo abierto** para su estudio y posible mejora posterior por
parte de estudiantes e investigadores.

Más específicamente, los métodos que componen el contrato del Jesslet permitirán
crear y ejecutar un sistema experto. A su vez, proveerá de funciones que darán
la posibilidad de agregar, actualizar, eliminar y recuperar las reglas de
inferencia y los datos de estos sistemas. Este servicio web será programado
utilizando el lenguaje Java, e incorporará como un componente a la librería de
*Jess*.

Debido al funcionamiento inherente de los servicios web, se ha limitado el
conjunto de sistemas que pueden ser representados con el Jesslet a **sistemas
expertos de universo cerrado** ([ver capítulo II][sistemas-expertos]).

### Herramientas
El Jesslet fue desarrollado utilizando el **lenguaje de Programación Java**, el
cual presenta un gran soporte para el desarrollo de servicios web.

Se utilizó como base el *shell Jess*, debido la capacidad que presenta de poder
ser utilizado como una librería dentro de otra aplicación Java, lo cual lo hace
adecuado para ser tomado como shell base para el servicio web. Además, y no
menos importante, su API está adecuadamente documentada y es posible obtener una
licencia para uso académico con sólo solicitarla.

Cada historia de usuario fue desarrollada mediante el enfoque del **desarrollo
guiado por las pruebas** ([ver capítulo V][xp]), utilizando la herramienta
**JUnit**[1][1] para realizar pruebas de unidad de manera automatizada sobre
Java. Algunas historias de usuario fueron trabajadas mediante mapas mentales,
gráficos y diagramas UML. Si bien la utilización de estas herramientas parece
contradictoria con el principio ágil de la poca o nula documentación, se debe
considerar que la XP no hace hincapié en ningún tipo de modelo o representación
formal de los sistemas o parte de ellos. Entre sus lineamientos sólo enuncia que
los programadores y actores del proyecto deben utilizar aquello que les resulte
conveniente o consideren mejor para facilitar la comunicación entre sus
miembros. En el contexto del desarrollo del *Jesslet*, la representación de
ciertos patrones de diseño orientados a objetos mediante estos diagramas fueron
de gran utilidad para poder analizar el funcionamiento del sistema previo a su
programación. Puntualmente, través de los diagramas de secuencia se pudieron
trazar de manera precoz algunos de los métodos e interfaces de las clases, cuyas
interconexiones fueron determinantes en el diseño del sistema final.

A lo largo del desarrollo del Jesslet (y de este documento) se utilizó
**Mercurial** para el control de versiones del código fuente y del contenido,
utilizando como repositorio central el servicio gratuito provisto por
**BitBucket**. Este repositorio puede encontrarse en
<https://bitbucket.org/rgmiranda/jesslet>, en el cual pueden apreciarse todos
los cambios por los que ha ido pasando el sistema.

Por último, para la escritura del código se utilizó como recomendación el
**estándar de codificación** propuesto por **Sun Microsystems** para el lenguaje
Java \[[SUN1997]\].

## Fase de Planificación y Plan de Lanzamiento
Los requisitos fueron planteados mediante la definición de **historias de
usuarios**. Cada historia de usuario fue escrita en tarjetas o fichas haciendo
uso de la aplicación gratuita Trello[2][2]. Una vez completada la lista de
historias, se procedió a la creación de un plan de trabajo mediante su
priorización, la cual se lista a continuación:

1. Servicio web contenedor de los métodos
2. Método para agregar una regla de inferencias
3. Método para recuperar una regla de inferencias
4. Método para recuperar todas las reglas de inferencias
5. Método para eliminar una regla de inferencia
6. Método para agregar un hecho (**Fact**)
7. Método para eliminar un hecho
8. Método para recuperar un hecho
9. Método para recuperar todos los hechos
10. Método para restablecer los hechos (**reset**)
11. Método para ejecutar el sistema (**run**)
12. Soportar múltiples sistemas (proyectos)
13. Manejo de un archivo de configuración central

## Fase de Iteraciones
A continuación se presenta la especificación de las historias de usuarios, los
ciclos de desarrollo y las historias abarcadas en cada uno de ellos. Cabe notar
que algunas historias impactaron o modificaron el funcionamiento de otras. Lo
que se describe a continuación son las historias terminadas en su totalidad; no
se muestran los cambios que se les pudieran haber realizado a medida que eran
desarrolladas las restantes historias. La ejecución casi constante de las
pruebas automatizadas ayudó a mantener la consistencia en las interfaces de las
clases y sus valores esperados.

### Primer Ciclo
El primer ciclo abarcó la **creación del servicio web** inicial sobre el cual se
ejecutan todos los métodos y procedimientos que se invoquen sobre el *Jesslet*.
La estimación del tiempo que insumiría la creación de este componente provocó
que nacieran tres historias de usuario a partir de la original:

1. Definición del contrato del servicio.
2. Implementación del contrato
3. Publicación del servicio.

#### Historia de Usuario *Definición del Contrato del Servicio*
El primer paso en la creación del servicio web es la definición de la
**interfaz** (`Interface`), la cual establece la firma de cada uno de los
métodos que integran el contrato del servicio web. Esta interfaz es utilizada
por las bibliotecas de la plataforma Java para generar el documento `WSDL`
([ver capítulo IV][servicios-web]) necesario para ser consumido
por los clientes.

La siguiente Figura presenta la interfaz y su relación con el servicio web.

![Figura 7.1. La interfaz del servicio](imgs/7_1_interfaz.png "Figura 7.1. La interfaz del servicio")

Esta historia de usuario está centrada exclusivamente en la parte de la interfaz
`<<Interface>> IJesslet`. En este punto, aún no se tenían firmemente
establecidos los tipos de parámetros que cada uno de ellos recibirían, ni los
valores que deberían devolver. A medida que se avanzó sobre el desarrollo de las
restantes historias de usuario, y se obtuvo un mayor conocimiento de las mismas,
las firmas de los métodos fueron actualizadas oportunamente. En el fragmento de
código que se muestra a continuación se encuentra una aproximación inicial de
los métodos disponibles.

~~~java
public interface IJesslet {
    void AddRule();
    void RemoveRule();
    void GetRule();
    void GetAllRules();
    void AssertFact();
    void RemoveFact();
    void GetFact();
    void GetAllFacts();
    void Reset();
    void Run();
}
~~~

#### Historia de Usuario *Implementación del Contrato*
La implementación del contrato o interfaz implicó la creación de la clase
`Jesslet` como una implementación de la interfaz `IJesslet`. Cabe mencionar que
ninguno de los métodos definidos en la interfaz, en este punto, poseía
implementación alguna. Inicialmente, todas estas funciones sólo eran marcadores
de posición para las implementaciones que se realizarían al trabajar con las
otras historias de usuario en las iteraciones subsecuentes.

Este fragmento muestra parte de la clase `Jesslet` en su estado incipiente.

~~~java
public class Jesslet implements IJesslet {
    @Override
    public void AddRule() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void RemoveRule() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void GetRule() {
        throw new UnsupportedOperationException();
    }

    // ...
}
~~~

#### Historia de Usuario *Publicación del Servicio*
La publicación del servicio implicó la creación de un **demonio** para escuchar
las invocaciones mediante SOAP que se realicen sobre los métodos de la interfaz.
Los cambios introducidos por esta historia, como se ve en el diagrama de la
Figura 7.1, implicó la utilización del paquete `JAX-WS`, y la creación del
servicio mediante este. Esta biblioteca utiliza los métodos públicos definidos
en la interfaz para generar automáticamente el documento WSDL con los métodos
SOAP disponibles, sus parámetros, y los valores devueltos. Para conseguir esto
se establecen una serie de **anotaciones** ([ver capítulo IV][servicios-web]) en
la interfaz `IJesslet` como aparece a continuación.

~~~java
@WebService(name = "JessletService", targetNamespace = JessletConstants.WS_TARGETNAMESPACE, portName = "JessletServicePort")
@SOAPBinding(style = Style.RPC, use = Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface IJesslet {

    @WebMethod
    void AddRule();

    @WebMethod
    void RemoveRule();

    @WebMethod
    void GetRule();

    // ...
}
~~~

Existen dos maneras de publicar un servicio web en Java:

1. Utilizar un servidor de aplicaciones (por ejemplo *Tomcat* o *Glassfish*)
para que escuchar las solicitudes HTTP y SOAP.
2. Crear un servidor *ad-hoc* mediante *JAX-WS*.

Por razones de simplicidad y facilidad de puesta a punto, se optó por el segundo
camino, obteniéndose una clase principal `Main` en donde se crea la instancia
del servicio y se define la URL sobre la cual escuchará los pedidos SOAP
(`http://127.0.0.1/jesslet/`).

~~~java
public class Main {

    public static void main(String[] args) {
        try {
            System.out.println("Iniciando el servicio...");
            Endpoint end = Endpoint.publish("http://127.0.0.1/jesslet", new Jesslet());
            System.out.println("Servicio Iniciado...");
            System.out.println("Presione ENTER para cerrar...");
            System.in.read();
            System.out.println("Cerrando el servicio...");
            end.stop();
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
        } catch (JessletFault ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
~~~

### Segundo Ciclo
Este ciclo incluyó la implementación del **método para la adición de una nueva
regla de inferencia** en la base de conocimientos, lo cual requirió de la
creación de una clase `Rule` a modo de contenedor de todos los datos relativos a
una regla de inferencia. El método `AddRule` recibe como parámetro una instancia
de dicha clase, la cual representa la regla de inferencia a ser creada, y
devuelve un valor `boolean` indicando si la operación ha resultado exitosa o no.
En la nueva firma del método se establece que, eventualmente, este podría lanzar
una excepción del tipo `ServerFault`. La cual sirve para retornar errores (`SOAP
Faults`) hacia el cliente del servicio.

El fragmento siguiente presenta la **firma** de este método de la interfaz
`IJesslet`, en la cual se especifica que este método tiene la posibilidad de
lanzar una excepción del tipo `ServerFault`.

~~~java
boolean AddRule(Rule rule) throws ServerFault;
~~~

Debido a la complejidad del desarrollo que se debía afrontar en esta historia,
tuvo que se fraccionada en dos:

1. Representación de las Reglas de Inferencia
2. Creación de una Regla de Inferencia en el Shell

#### Historia de Usuario *Representación de las Reglas de Inferencia*
La clase `Rule` representa una regla de inferencia en el *Jess*, y, al igual que
su par en el shell, está compuesta por un **nombre**, una **descripción**, el
**antecedente**, (condiciones o restricciones de activación), y el
**consecuente** (acciones a realizarse cuando esta sea disparada)
([ver capítulo II][sistemas-expertos]).

~~~commonlisp
(nombre-regla "Descripción de la regla"
	; Condiciones antecedentes...
	=>
	; Acciones consecuentes...
)
~~~

De esta forma, `Rule` fue creada con cuatro atributos: `name`, `description`,
`conditions` y `actions`. El campo `name` se corresponde con `nombre-regla` y
`description` con la documentación de la regla que aparece entre comillas. Ambos
campos son del tipo *cadena de caracteres* (`String`), mientras que los campos
`conditions` y `actions` son arreglos que representan los conjuntos de
antecedentes y consecuentes respectivamente.

#### Condiciones de una Regla
Cada **condición** en una regla es una **restricción** basada en la existencia
(o no) de un hecho con determinadas características. Para el desarrollo del
Jesslet, se tuvo que restringir la sintaxis de estas condiciones a un formato
que pudiese, por un lado, ser lo suficientemente **flexible** para permitir el
trabajo adecuado con las reglas de inferencia, y por otro, lo suficientemente
**abstracto** para ser representado mediante una clase serializable a *XML* para
su envío a través de un mensaje *SOAP*.

Debido a estas limitaciones, se optó por soportar sólo condiciones con **hechos
ordenados con un único valor**, por ejemplo:

~~~commonlisp
(encendido TRUE)
(semaforo "VERDE")
(tension 220)
~~~

Los hechos no ordenados y ordenados con múltiples valores habrían complejizado
demasiado la interfaz del servicio, y se consideró que no era lo ideal para una
primera aproximación.

El atributo `conditions` en la clase `Rule` es un arreglo de objetos del tipo
`FactCondition`. Cada elemento de este arreglo se corresponde con una condición
para la regla, y posee cuatro atributos:

1. `factName`: nombre del hecho.
2. `factAlias`: nombre con el que será llamada la variable asociada al valor del
hecho, y al hecho en sí mismo siempre que no esté negado (`negated = false`).
3. `negated`: valor booleano que indica si el hecho está negado (`not`) o no.
4. `filter`: objeto del tipo `Expression` que establece una condición sobre el
valor del hecho.

La clase `Expression` ha sido diseñada para representar una expresión en Jess
mediante una **estructura de árbol**. Por ejemplo `(* (+ ?x 34) (- ?y -40))` se
traduciría en el árbol que aparece en el Figura 7.2.

![Figura 7.2. Árbol de una expresión.](imgs/7_2_expresion.png "Figura 7.2. Árbol de una expresión.")

`Expression` es una **clase abstracta** dos clases:

- `LiteralExpression`, la cual se usa para expresiones literales, por ejemplo
`"una cadena"`, `256`, `3.1459`.
- `OperationExpression`, la cual representa expresiones basadas en una función
u operación, por ejemplo `(= 90 90.0)`, `(and (<= 0 ?var) (<= ?var 35535))`.

Una **expresión literal** se utiliza para presentar un valor literal final, como
una cadena de caracteres o un número. `LiteralExpression` tiene dos campos para
tal fin: `value` y `valueType`. Este último determina el tipo de valor con el
cual será cargado `value`. Por ejemplo, si se tiene una expresión literal con un
valor `-128`, y el tipo de valor fuese `STRING`, el valor asignado sería
`"-128"`. Por otro lado, si fuese `INTEGER`, entonces se obtendría sencillamente
`-128`. Para poder hacer referencia al valor del hecho en los antecedentes de la
regla de inferencia, hay que utilizar el tipo de valor `REFERENCE`, y el valor
debe iniciar con el caracter `@`. Cuando la condición se haga efectiva en el
motor de inferencias, este valor será modificado cambiando el `@` por `?`, y
agregando el sufijo `-fact`. Por ejemplo, `@alias` será convertido en
`?alias-fact`.

Una `OperationExpression`, o **expresión de operación**, se encuentra compuesta,
a si mismo, por un conjunto de objetos del tipo `Expression` (los *operandos*),
utilizando de esta manera, un **patrón Composite**.

#### Acciones en una Regla
Se entiende por **acciones** al conjunto de sentencias que aparecen en el
consecuente de una regla de inferencia, y en `Rule`, están representadas por el
arreglo `actions`. Cada elemento de este arreglo es una instancia de una clase
que extiende a la clase *abstracta* `Action`.

Un inconveniente encontrado a la hora de representar las acciones fue que en
esta parte de la regla es posible escribir prácticamente cualquier función o
código fuente en lenguaje Jess. A los fines de evitar el envío de código fuente
a través del servicio web, se tuvo que reducir las acciones que se pueden
realizar en el consecuente de una regla de inferencia a tres tipos: **creación
de un hecho**, **eliminación de un hecho**, **recomendación al operador**.

A partir de `Action`, heredan tres clases que se corresponden con estas tres
acciones:

- `AssertAction`: para la **creación de un hecho** en la memoria de trabajo.
- `RemoveAction`: para la **eliminación de un hecho** de la memoria de trabajo.
- `RecommendAction`: para la presentación de un **mensaje para el operador**.

`AssertAction` posee un único atributo `fact` del tipo `SimpleFact`, el cual
simboliza el hecho a ser agregado.

`SimpleFact` está compuesta por el identificador del hecho en la memoria de
trabajo (`id`), el nombre del hecho (`name`), una breve descripción
(`description`), y el valor del hecho a agregarse (`value`). Este valor es una
instancia de una clase que hereda de `Expression`.

La acción `RemoveAction` sólo se compone del atributo `factId`, el cual guarda
el identificador del hecho a ser eliminado de la memoria de trabajo. Este
identificador es una cadena de caracteres, y debe ser un identificador válido
para un alias de un hecho en la regla. Este identificador debe cumplir con las
reglas antes detalladas para un alias en una expresión literal, es decir, debe
iniciar con el caracter `@`, el cual será reemplazado por `?` y se la agregará
el sufijo `-fact`.

Una acción `RecommendAction` genera el código que imprime un mensaje al operador
del sistema. La única propiedad que tiene esta clase es `message`, y se trata de
cadena de caracteres a ser presentada. Por ejemplo:

~~~commonlisp
(printout t "Esta es la cadena de caracteres en 'message'." crlf)
~~~

El parámetro `t`, que aparece primero en la llamada a la función `printout`, es
el buffer de salida sobre el cual se va a imprimir el mensaje, y `t` es el
buffer por defecto de *Jess*. En el contexto del *Jesslet*, este buffer cambia
por otro desde el cual se pueden recuperar los mensajes para ser devueltos como
resultado de una ejecución a través de SOAP.

#### Historia de Usuario *Creación de una Regla de Inferencia en el Shell*
La creación de la regla de inferencia se hace ejecutando un código en lenguaje
Jess, el cual hace uso de la función `defrule` para conseguir esto. La clase
`Rule` ha sido dispuesta con un método `getSource`, el cual genera el código
fuente en lenguaje Jess correspondiente a la definición de la regla de
inferencia requerido por `defrule` para crearla en el motor. La Figura 7.3
presenta un diagrama UML con las clases junto con sus atributos y métodos que
intervienen en la generación del código fuente para la definición la regla.

![Figura 7.3. Clases del método para agregar una regla de inferencia](imgs/7_3_agregar_regla.png "Figura 7.3. Clases del método para agregar una regla de inferencia")

Cuando el método `getSource` de `Rule` es invocado, este se encarga de generar
el código haciendo uso de los `getSource` de las clases que la componen.

En la Figura 7.4 aparece un diagrama de secuencias de UML en donde se presenta
conjuntamente el trabajo de la clase `Rule` para la obtención del código fuente
con `getSource`, desde que se invoca al método `AddRule` en el Jesslet, hasta su
implementación en el motor (`Rete`) mediante la función `eval`.

![Figura 7.4. Diagrama de Secuencias para Agregar una Regla de Inferencia.](imgs/7_4_secuencia_agregar_regla.png "Figura 7.4. Diagrama de Secuencias para Agregar una Regla de Inferencia.")

Para las condiciones de la regla, se recorre el arreglo de objetos
`FactCondition`, generando una a una las condiciones del antecedente. El código
mediante el método `getSource` de esta clase posee el siguiente formato:

~~~commonlisp
?<factAlias>-fact <- (<factName> ?<factAlias>-value&:<filter.getSource()>)
~~~
Por ejemplo, si se tiene un objeto con los atributos:

~~~js
{
	factName: "hecho",
	factAlias: "h",
	negated: false,
	filter: null
}
~~~

entonces se generaría una condición de la forma:

~~~commonlisp
?h-fact <- (hecho ?h-value)
~~~

Si `negated`, por el contrario, tuviese un valor positivo (`true`), entonces el
hecho no se dejaría en variable alguna, y el resultado de la función `getSource`
cambiaría de esta forma:

~~~commonlisp
(not (hecho ?h-value))
~~~

Análogamente, las acciones en el consecuente también son generadas una a una con
el recorrido de su arreglo contenedor. Cada clase hija de `Action` implementa el
método abstracto `getSource` utilizado por `Rule` para generar el código
completo de la regla.
Para las acciones de **creación de hechos** (`AssertAction`), el código generado
es estructurado de esta manera:

~~~commonlisp
(assert <fact.getSource()>)
~~~

Por ejemplo:

~~~commonlisp
(assert (presion 101325))
(assert (mensaje "..."))
(assert (ensamblado TRUE))
(assert (total (* 2 (+ ?x-value ?y-value))))
~~~

Para una acción de **eliminación de un hecho**, se hace uso de `factId` para
obtener el código Jess. Por ejemplo, un valor `@alias-del-hecho` resultaría de
esta forma:

~~~commonlisp
(retract ?alias-del-hecho-fact)
~~~

La principal ventaja de estructurar las acciones de esta manera es que si en un
futuro se requiere una nueva acción, su adición no representará una gran
dificultad. Solamente debe crearse una nueva clase que herede de `Action` e
implemente oportunamente el método `getSource`.

### Tercer Ciclo
Durante esta iteración se trabajó sobre los **métodos para recuperar las reglas
de inferencia** del shell Jess, y con el **método para eliminar una regla** de
la base de conocimientos. El reto principal en este ciclo fue el de traducir los
objetos internos de Jess que representan el antecedente y el consecuente hacia
objetos del tipo `Rule`.

#### Historia de Usuario *Método para Recuperar una Regla de Inferencia*
El método `GetRule` tiene el propósito de recuperar una regla de inferencia, y
para tal fin, su firma fue actualizada para recibir como único parámetro el
nombre de la regla de inferencia a ser buscada, y devolver un objeto del tipo
`Rule` en caso de que la regla haya sido encontrada. En caso contrario, se
lanza una excepción `ServerFault`.

~~~java
Rule GetRule(String ruleName) throws ServerFault;
~~~

Para este método, también se tuvo que modificar la clase `Rule` agregándole un
constructor que recibe como parámetro objeto `Defrule`, el cual representa una
regla de inferencia en el contexto de Jess. Este objeto es extraído desde la
instancia de `Rete`, que representa el motor de Jess en sí. Cuando este
constructor es ejecutado, se recuperan todos los objetos `CondicionalElement`
(las **condiciones**) y `Funcall` (las **acciones**). Utilizando estos objetos,
se crean las instancias de `FactCondition` para las condiciones, y las
instancias de `Action` para las acciones en la clase `Rule`.

Para la creación de cada uno de los objetos `FactCondition` mediante un
`ConditionalElement`, se programó un constructor que recibe como parámetro una
instancia de esta clase. Este se encarga de determinar qué tipo de condición es,
y de la creación del filtro (`Expression`) correspondiente.

Cada `ConditionalElement` está asociado a un conjunto de pruebas (`Test1`), las
cuales son los filtros asociados a la condición. Cada objeto `Test1` tiene un
valor asignado (`Value`) con el contenido de la prueba. La clase abstracta
`Expression` también fue modificada para dar soporte a esta construcción con un
método estático `loadExpression`. Esta función construye una instancia de una de
las dos clases que heredan de `Expression` (`LiteralExpression` y
`OperationExpression`) usando el parámetro del tipo `Value` asociado a la
instancia de `Test1`.

De manera semejante, para poder crear las acciones `Action` para la regla, esta
clase fue adicionada con un método estático `loadAction`, el cual utiliza un
objeto `Funcall` (cada una las acciones en la regla `Defrule` en Jess) para
determinar qué tipo de acción es (agregar, eliminar o recomendar), y construir
la instancia correspondiente (`AssertAction`, `RemoveAction` o
`RecommendAction`). Para el caso de la creación de un hecho, también se requiere
recuperar desde Jess el hecho a ser creado. Este se corresponde con el atributo
`fact` de la clase `AssertAction`. Para dar soporte a esta situación, se
programó un constructor sobre `SimpleFact` que recibe como parámetro un objeto
del tipo `Fact`.

El diagrama en la Figura 7.5 muestra las clases, junto con sus relaciones,
intervinientes en el proceso descrito.

![Figura 7.5. Diagrama de Clases para Recuperar una Regla de Inferencia.](imgs/7_5_clases_recuperar_regla.png "Figura 7.5 Diagrama de Clases para Recuperar una Regla de Inferencia.")

Las Figuras 7.6, 7.7 y 7.8 de abajo presentan el diagrama de secuencias de la
creación de una clase `Rule`. La primera se enfoca en el constructor de esta
clase, mientras que las otras dos ahondan en la creación de las condiciones
(`FactCondition`) y las acciones (`Action`).

![Figura 7.6. Diagrama de Secuencias para Recuperar una Regla de Inferencia.](imgs/7_6_secuencia_recuperar_regla.png "Figura 7.6. Diagrama de Secuencias para Recuperar una Regla de Inferencia.")

![Figura 7.7. Diagrama de Secuencias para Construir una Condición.](imgs/7_7_secuencia_constructor_condicion.png "Figura 7.7. Diagrama de Secuencias para Construir una Condición.")

![Figura 7.8. Diagrama de Secuencias para Construir una Acción.](imgs/7_8_secuencia_constructor_accion.png "Figura 7.8. Diagrama de Secuencias para Construir una Acción.")


#### Historia de Usuario *Método para Recuperar Todas las Reglas del Sistema*
Este método funciona de manera similar al desarrollado en la historia anterior,
con la diferencia de que en vez de retornar una, devuelve todas las reglas
encontradas en el sistema como un arreglo de objetos `Rule`.

~~~java
public Rule[] GetAllRules() throws ServerFault;
~~~

Internamente, este procedimiento itera sobre todas las reglas presentes
(`Defrule`), y generar para cada una su correspondiente representación (`Rule`)
para ser enviada al cliente. En este caso, se reutiliza el constructor de `Rule`
desarrollado en la función anterior. El diagrama de secuencia en la Figura 7.9
presenta brevemente este funcionamiento.

![Figura 7.9. Diagrama de Secuencias para Recuperar Todas las Reglas.](imgs/7_9_secuencia_recuperar_todas_reglas.png "Figura 7.9. Diagrama de Secuencias para Recuperar Todas las Reglas.")

#### Historia de Usuario *Método para Eliminar Una Regla de Inferencia*
Este método recibe el nombre de regla que se quiere borrar como único parámetro,
y devuelve un valor booleano: `true` si todo ha resultado correctamente, y
`false` en cualquier otro caso.

~~~java
public boolean RemoveRule(String ruleName) throws ServerFault;
~~~

El funcionamiento de este procedimiento resulta bastante sencillo. Haciendo uso
del nombre de la regla, se genera un fragmento de código en lenguaje Jess de la
siguiente forma `(undefrule <ruleName>)`, por ejemplo

~~~commonlisp
(undefrule regla-1)
(undefrule betelgeuse)
(undefrule aracalacana)
~~~

Nuevamente, este fragmento se ejecuta sobre el `Rete` con la función `eval`.

### Cuarto Ciclo
Las historias de usuario que fueron abarcadas por este ciclo son las que se
corresponden con los métodos para **agregar** y **eliminar** hechos de la
memoria de trabajo de Jess.

#### Historia de Usuario *Método para Agregar un Hecho (`Fact`)*
El fin de esta función es agregar un hecho a la memoria de trabajo del sistema.
La definición en la interfaz `IJesslet` fue alterada para recibir como parámetro
un objeto del tipo `SimpleFact`, y, en base a este, crear una instancia de la
clase `Fact` de Jess. Ya se trabajó anteriormente con esta clase `SimpleFact` en
la historia de usuario "*Representación de las Reglas de Inferencia*" en la
primera iteración.

~~~java
public int AssertFact(SimpleFact simpleFact) throws ServerFault;
~~~

El diagrama de secuencias que aparece a continuación (Figura 7.10) resume el
funcionamiento del método.

![Figura 7.10. Diagrama de Secuencia para Agregar un Hecho.](imgs/7_10_secuencia_agregar_hecho.png "Figura 7.10. Diagrama de Secuencia para Agregar un Hecho.")

El abordaje de este requisito se hizo, en un principio, de una forma semejante
a la utilizada para agregar una regla de inferencia. El método invocaría a la
función `getSource` del parámetro `SimpleFact` recibido, para luego llamar a
`eval` en el motor `Rete`. La ventaja de este enfoque es que es posible una
abstracción con respecto a los detalles internos del manejo de los hechos en el
shell. No obstante, el problema que se descubrió con esta aproximación es que
resulta imposible determinar con que *identificador* fue agregado el hecho a la
memoria de trabajo. Podría hacerse la presunción de que el nuevo identificador
podría ser el mayor de todos, pero debido a que Jess estaría ejecutándose en
calidad de un servicio, no hay forma de garantizar esta premisa.

La solución finalmente propuesta es la creación de los hechos nativos (`Fact`)
en base a los atributos del parámetro `SimpleFact`. Estos hechos se agregan a la
memoria de trabajo asociada al motor usando la función `assert` en la clase
`jess.Rete`.

#### Historia de Usuario *Método para Eliminar un Hecho*
Este método borra un hecho de la memoria de trabajo de Jess, y devuelve `true`
si ha sido satisfactorio, y `false` en otro caso. La firma resultante del método
es la siguiente:

~~~java
public boolean RemoveFact(int factId) throws ServerFault;
~~~

El procedimiento hace uso del método `findFactByID` del motor `jess.Rete` para
recuperar el método, y luego invoca a `retract` en caso de que el hecho haya
sido encontrado.

### Quinto Ciclo
En esta iteración fueron desarrolladas las historias de usuario correspondientes
a los métodos para **recuperar hechos** de la memoria de trabajo, y para
**restablecerla** (`reset`).

#### Historia de Usuario *Método para Recuperar un Hecho*
Este método recibe un identificador de un hecho, y devuelve un objeto
`SimpleFact` correspondiente al hecho. Si el hecho no es encontrado, se lanza
una **SoapFault** del tipo `ServerFault`.

~~~java
public SimpleFact GetFact(int factId) throws ServerFault;
~~~

Este procedimiento reutiliza el constructor programado con anterioridad para la
clase `SimpleFact`, en el cual el objeto se construye utilizando una instancia
de un hecho `Fact` de Jess.

![Figura 7.11. Diagrama de Secuencia para Recuperar un Hecho.](imgs/7_11_secuencia_recuperar_hecho.png "Figura 7.11. Diagrama de Secuencia para Recuperar un Hecho.")

#### Historia de Usuario *Método para Recuperar Todos los Hechos*
Esta función trabaja de una forma análoga a la anterior, con la diferencia de
que retorna un arreglo de objetos `SimpleFact` en lugar de uno solo.

~~~java
public SimpleFact[] GetAllFacts() throws ServerFault;
~~~

Recupera todos los hechos desde el motor `Rete`, y construye el arreglo usando
estos objetos `Fact` encontrados.

#### Historia de Usuario *Método para Restablecer los Hechos*
Este método es una suerte de *alias* para el método `reset` de Jess, y tiene el
objetivo de eliminar de la memoria de trabajo todos los hechos y crear el hecho
inicial `initial-fact` ([ver capítulo III][jess]).
Recordemos que este hecho resulta ser muy importante, debido a que permite la
activación de reglas de inferencia cuyas primeras condiciones utilizar la
cláusula de no existencia `not`.

En el Jesslet, `Reset` invoca al método `reset` de la clase `Rete`, y devuelve
un valor booleano que indica si la operación ha sido exitosa o no.

~~~java
public boolean Reset() throws ServerFault;
~~~

### Sexto Ciclo
En esta etapa se programó el método del servicio que permite ejecutar el sistema
experto con sus reglas de inferencias y hechos en la memoria de trabajo. Cabe
recalcar que la ejecución debe hacerse luego de *restablecer* el sistema y
cargar los hechos necesarios.

#### Historia de Usuario *Método para Ejecutar el Sistema*
El método `Run` permite ejecutar el sistema y devuelve una instancia de la clase
`Response`, la cual resume el resultado de la operación.

~~~java
public Response Run() throws ServerFault;
~~~

Este objeto `Response` se compone de los siguientes atributos:

- `httpCode`: código de resultado de la operación
- `text`: texto con mensajes de resultado
- `recommendations`: listado de recomendaciones emitidas por el sistema
- `ruleStackTrace`: listado de las activaciones de las reglas de inferencia

El `httpCode` es el código de resultado, e indica el resultado de la ejecución
del sistema. El valor de esta propiedad se corresponde a uno de los **códigos de
estado de HTTP** \[[W3CHTTP11][W3CHTTP11]\]. `text` mantiene un mensaje de
resultado de la operación, a través del cual se puede obtener más información
sobre el resultado final de la operación. Todo mensaje de error puede ser
recuperado con esta propiedad. El campo `recommendations` es un arreglo de
cadenas de caracteres en el cual cada una de estas es un mensaje de
recomendación emitido por el motor de inferencias a medida que es ejecutado.
Estas recomendaciones se generan mediante las acciones `RecommendAction` en una
regla, las cuales se traducen a una llamada a la función `printout` en Jess.
La propiedad `ruleStackTrace` permite recuperar el listado de activaciones por
de las reglas de inferencias por las cuales ha ido pasando el sistema a lo largo
de su ejecución.

Para poder obtener las recomendaciones y la traza de las activaciones y hechos
generados, se deben configurar streams de salida en el motor `Rete` de Jess, e
indicarle que los utilice para dicho propósito. Para crear estos streams de
salida se programó una clase especial llamada `StringArrayWriter`, la cual se
encarga de escribir todo buffer de entrada como un nuevo elemento en un arreglo
de cadenas de caracteres. Esta clase hereda de la clase abstracta nativa de Java
`Writer`. Con el método `addOutputRouter` de `Rete` se agrega un objeto de este
tipo para recuperar todas las salidas. Cuando la ejecución finaliza, se usa la
función `getStrings`, el cual devuelve el arreglo de cadenas necesario.

La Figura 7.12 presenta un diagrama de secuencias en donde puede apreciarse con
mayor detalle la interacción de las diferentes clases involucradas en este
método.

![Figura 7.12. Diagrama de Secuencias para la Ejecución de un Sistema.](imgs/7_12_secuencia_ejecutar.png "Figura 7.12. Diagrama de Secuencias para la Ejecución de un Sistema.")

Cabe aclarar que en este diagrama aparece una clase, aún no mencionada, llamada
`PropertiesManager`. Esta clase será explicada más adelante al tratar el
requisito del **manejo de un archivo de configuración central**.

Debido al funcionamiento inherente de los servicios web, no es posible que el
sistema experto que se ejecuta sobre el Jesslet solicite al usuario información
para sumarla a su base de hechos. Una vez que la ejecución es iniciada, esta no
puede ser detenida. Es por ello que, para este prototipo inicial, se ha limitado
el conjunto de sistemas que pueden ser representados con el Jesslet a los
**sistemas expertos de universo cerrado**
([ver capítulo II][sistemas-expertos]).

## Séptimo Ciclo
El séptimo y último ciclo abarcó dos características que permitieron mejorar el
funcionamiento del Jesslet: el soporte a **múltiples proyectos**, y el manejo de
un **archivo de configuración** central. La primera historia de usuario apareció
con el objetivo de permitir que el Jesslet pueda manejar más de un sistema
experto simultáneamente. Esta característica le agrega otra porción de
versatilidad en el contexto de los *servicios web*. Por otro lado, el tener un
archivo de configuración central y externo al software, lo vuelve más flexible
ante variaciones en su entorno de ejecución, permitiendo ajustar parámetros sin
la necesidad de recompilar todo el sistema.

#### Historia de Usuario *Soportar Múltiples Proyectos (Sistemas)*
La idea principal detrás de esta historia de usuario es poder administrar y
ejecutar diferentes sistemas dentro del servicio del Jesslet. Todo los métodos y
requerimientos descritos hasta este punto permiten agregar y actualizar reglas
de inferencias y hechos sobre un único sistema experto contenido en el Jesslet.
Si se necesitase crear un nuevo sistema experto, se debería configurar una nueva
instancia del servicio (con todo lo que ello implica).

De esta manera, se ideó una forma de permitirles a las aplicaciones clientes
especificar sobre qué sistema desean aplicar los métodos que invocan sobre el
servicio. El servicio debería **cargar el estado** de ese sistema o proyecto,
ejecutar el método invocado, y **guardar el estado** nuevamente para su uso
futuro.

![Figura 7.13. Carga y Almacenamiento de Sistemas.](imgs/7_13_carga_almacenamiento_sistemas.png "Figura 7.13. Carga y Almacenamiento de Sistemas.")

Para poder almacenar el estado de un sistema en Jess, en primera instancia, tuvo
que encontrarse una manera de representarlo. Afortunadamente, la clase `Rete`
cuenta con dos funciones para guardar y recuperar los contenidos de un sistema:
`bsave` y `bload`. Este último recibe un parámetro del tipo `InputStream`, y
sirve para cargar el contexto (estado) mediante este stream. Por otro lado, el
método `bsave` recibe un parámetro `OutputStream`, y tiene el objetivo de
guardar dicho contexto sobre este.

La especificación del proyecto de trabajo por parte del cliente se hace mediante
un parámetro **PID** (identificador de proyecto), el cual debe ser enviado en
cada mensaje SOAP en el elemento `<Header>`
([ver capítulo IV][servicios-web]).

~~~xml
<?xml version="1.0"?>
<soap:Envelope
  xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
  soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding"
  xmlns:ns="http://fce.unse.edu.ar/jesslet">

  <soap:Header>
    <ns:PID>FOO</ns:PID>
  </soap:Header>
  <!-- ... -->
</soap:Envelope> 
~~~

Para la recepción de la cabecera `<PID>` se hace mediante un parámetro especial
agregado en la firma de cada uno de los métodos de la interfaz `IJesslet`. Este
parámetro se recibirá desde el `<Header>` del mensaje SOAP. En el fragmento que
aparece a continuación , puede verse la definición de este parámetro, pero de
modo que representa lo mismo para cada método, ha sido obviado en las
definiciones de los métodos antes presentados.

~~~java
@WebMethod
    boolean AddRule(
        @WebParam(mode = WebParam.Mode.IN,
            name = "PID",
            partName = "PID",
            targetNamespace = JessletConstants.WS_TARGETNAMESPACE,
            header = true) String pid,
        @WebParam(name = "rule") Rule rule) throws JessletFault;
~~~

El parámetro en `<PID>` puede ser cualquier cadena de caracteres. Si el proyecto
se encuentra en la base de datos de proyectos, entonces es cargado. Sino, es
creado usando el nuevo identificador, y luego se lo carga. Para esta
funcionalidad, la clase `Jesslet` fue dotada de tres funciones: `getPID`,
`load`, y `save`. `getPID` recupera el identificador desde la cabecera del
mensaje SOAP. Se debe notar que si la ejecución no se encuentra en un contexto
de un servicio web, por ejemplo en las pruebas, el identificador será recuperado
como `null`. El método `load` utiliza el identificador para recuperar desde la
base de datos los contenidos del proyecto, mientras que `save` los almacena.
Estos dos últimos métodos utilizan un arreglo de *bytes* para representar estos
contenidos, y son los encargados de interactuar con los métodos `bload` y
`bsave` de `Rete`. Para enviar y recibir los datos desde esta última clase,
utilizan las clases nativas de Java `ByteArrayInputStream` y
`ByteArrayOutputStream`.

La **base de datos de proyectos** se abstrae mediante la *clase abstracta*
`Storage`, la cual establece *tres métodos* necesarios para tales fines:
`LoadProject`, `SaveProject`, y `ProjectExists`. El primero se encarga de
recuperar un proyecto, y recibe como parámetro el identificador del proyecto
(`pid`). Los contenidos del proyecto se devuelven como un arreglo de bytes
(*stream*), formato que utiliza la clase `Rete` en Jess para representar el
contexto (estado) de un sistema.

La Figura 7.14 que aparece a continuación resume en un diagrama de clases las
relaciones entre las clases, sus métodos y atributos.

![Figura 7.14. Diagrama de Clases del Almacenamiento de Proyectos.](imgs/7_14_diagrama_almacenamiento.png "Figura 7.14. Diagrama de Clases del Almacenamiento de Proyectos.")

Como puede verse, existen dos clases que heredan de `Storage`: `FileStorage` y
`SqliteStorage`.

La primera implementación utiliza el sistema de archivos de manera directa para
la persistencia de los datos de los proyectos. Cada proyecto es almacenado en un
archivo con el mismo nombre que su identificador, en el directorio donde se
ejecuta la aplicación.

La segunda clase, `SqliteStorage`, como su nombre lo indica, utiliza la
biblioteca **SQLite** \[[SQLITE][SQLITE]\] para gestionar el almacenamiento y
recupero de los proyectos. Dentro del proyecto, se tiene un archivo denominado
`jess.db`, el cual representa el esqueleto inicial de la base de datos. Cuando
el servicio intenta guardar o recuperar datos mediante `SqliteStorage` por
primera vez, el sistema crea un archivo homónimo en el directorio de ejecución
para realizar las consultas.

#### Historia de Usuario *Archivo de Configuración Centralizado*
Un archivo de configuración central tiene el propósito de parametrizar el
sistema y hacerlo más flexible ante ciertos cambios y variaciones del entorno en
el cual se ejecuta. Un ejemplo clásico de esta situación son los parámetros de
conexión a una base de datos. Estos datos, muchas veces, necesitan ser
actualizados, por lo que es necesario que esto se haga de manera única y sin
necesidad de recompilar parte del software.

Un archivo de configuración (o *archivo de propiedades*) `config.properties` ha
sido creado dentro del Jesslet, el cual contiene los datos de configuración
necesarios. Para su acceso, se programó una clase especial llamada
`PropertiesManager`, la cual tiene el solo objetivo de acceder al archivo de
propiedades y devolver el valor de una de ellas. Para ello, cuenta con un único
método llamado `getProperty`, el cual recibe el nombre de la propiedad que se
quiere recuperar, y devuelve una cadena con su valor

~~~java
public String getProperty(String propertyName);
~~~

Este método se encuentra *sobrecargado* con un segundo argumento que representa
un valor por defecto, en el caso de que la propiedad no haya sido encontrada.

~~~java
public String getProperty(String propertyName, String defaultValue);
~~~

Debido a que en toda la ejecución del sistema sólo es necesario contar con una
única instancia de `PropertiesManager`, esta clase se ha creado siguiendo el
patrón de diseño **Singleton**. Posee un **constructor privado**, y un **método
estático** `getInstance` para recuperar esta instancia única.

~~~java
private PropertiesManager ();

public static PropertiesManager getInstance();
~~~

Durante la inicialización del servicio, el Jesslet buscará el archivo
`config.properties` en el mismo directorio de ejecución. De no encontrarlo,
procederá a utilizar el archivo definido (y compilado) dentro de sí mismo. El
código que aparece a continuación es el archivo de configuración original que se
encuentra compilado en el código del servicio.

~~~cfg
# Nombre del stream de salida de las recomendaciones
jess.outstreams.recommendations=wsout

# Nombre del stream del watch de las reglas
jess.outstreams.watches=watchout

log.format=plain

# Nombre de la clase Storage a ser utilzada.
jesslet.storage.class=ar.edu.unse.fce.jesslet.service.FileStorage
jesslet.storage.config=/home/ricardo/srv/jesslet/projects

jesslet.service.location=http://127.0.0.1:8888/jesslet
~~~

## Funciones para Fecha/Hora
Para poder implementar el **Sistema de Sugerencia de Vacunas**
([ver capítulo VIII][sevac]) utilizando el *Jesslet* fue necesario contar con
las funciones de cálculo de fechas programadas en el archivo `fechahora.clp`.
Para ello se desarrolló un conjunto de funciones análogas en un archivo
`datetime.clp`, el cual puede ser encontrado en el el directorio
`/src/ar/edu/unse/fce/jesslet/jess/`. Cuando se debe crear un nuevo proyecto,
este archivo es cargado inmediatamente usando el método `batch` de la instancia
de la clase `Rete`. Así, todo proyecto que sea administrado a través de este
servicio web tendrá a su disponibilidad las funciones `datetime-*`.

Estas funciones trabajan con valores del tipo **fecha/hora**, que no son otra
cosa que **cadenas de caracteres** en el **formato de fecha y hora local del
estándar ISO** \[[ISO8601][ISO8601]\] (`yyyy-mm-ddThh:mm:ss`), por ejemplo
`1985-07-04T14:00:00`. La lista que aparece a continuación, presenta cada una de
las funciones de fecha/hora disponibles.

- `datetime-now`: Devuelve la fecha y hora actual. Esta función no recibe
parámetros.
- `datetime-format`: Aplica un formato sobre un valor fecha y hora, y lo
devuelve como resultado. Recibe como parámetros la **fecha** a la que se aplica
el formato, y el **formato** a aplicar. Para la definición de este formato se
usan los patrones definidos para la clase `SimpleDateFormat` \[[clase
SimpleDateFormat][sdf]\] de Java.
- `datetime-timestamp`: Recupera el **tiempo UNIX** de una fecha y hora, y tiene
como parámetro único este valor.
- `datetime-fromformat`: Realiza la acción inversa de `datetime-format`. Intenta
crear una fecha y hora a partir de un valor de texto con cierto formato. Esta
función recibe dos parámetros: el texto a ser convertido, y el formato desde el
cual se trabajará. Nuevamente, este formato debe respetar los patrones
utilizados para la clase `SimpleDateFormat`.
- `datetime-diff`: Calcula la *diferencia* entre dos valores de fecha y hora en
una *unidad* dada. Este procedimiento recibe tres parámetros: la fecha y hora
inicial, la fecha y hora final, y la unidad sobre la cual será calculada. Esta
unidad puede tomar los valores `s` para *segundos*, `m` para *minutos*, `h` para
horas, `d` para días, `w` para semanas, `M` para meses, y `y` para años.
- `datetime-add`: Adiciona una cantidad de unidades de tiempo a una fecha dada.
Los parámetros que esta función espera son la fecha a ser modificada, la
cantidad de unidades de tiempo a ser agregada, y la unidad de tiempo. Esta
última unidad puede tomar alguno de los valores para las unidades de tiempo
presentada en `datetime-diff`.

El fragmento que aparece a continuación muestra un ejemplo del funcionamiento
de algunos de los procedimientos `datetime-*`.

~~~bash
Jess> (bind ?hoy (datetime-now))
"2015-12-03T10:54:03"
Jess> (bind ?fecha-nac 1985-07-04T14:00:00)
1985-07-04T14:00:00
Jess> (printout t "Hoy es " (datetime-format ?hoy "EEEE d 'de' MMMM 'del' y") crlf)
Hoy es jueves 3 de diciembre del 2015
Jess> (printout t "Tiene " (datetime-diff ?fecha-nac ?hoy w) " semanas de vida." crlf)
Tiene 1586 semanas de vida.
~~~

## Pruebas desde Otros Entornos
Una de las ventajas de los servicios web radica en el hecho de que pueden ser
accedidos desde un sinnúmero de aplicaciones y plataformas. A continuación se
describe el desarrollo de uno de los aplicativos que utilizan el *Jesslet* para
sus operaciones en este trabajo. La finalidad de este desarrollo es el de
demostrar la implementación y el funcionamiento de un sistema experto en *Jess*
una plataforma distinta a Java. En este caso se trata de una **aplicación de
escritorio** que permite administrar y probar uno o más proyectos de sistemas
expertos.

Asimismo, el segundo, es una **extensión para el navegador Google Chrome**, y
tiene por función realizar consultas sobre la implementación del *sistema de
sugerencia de vacunas SEVAC* en el *Jesslet* ([ver capítulo VIII][sevac]).

### Aplicación de Administración *PyJesslet*
**PyJesslet** es un programa de escritorio escrito en **Python**, el cual tiene
como propósito administrar los hechos, las reglas de inferencias, ejecutar y
recuperar los resultados de un sistema experto a través del *Jesslet*.

La Figura 7.15 muestra la arquitectura general de *PyJesslet*, en la cual se
observa como la aplicación utiliza el servicio web programado. Más en detalle,
ilustra cómo trabaja cada componente y cómo es la relación entre ellos. En
particular, *PyJesslet* utiliza un módulo especial de Python para manejar las
particularidades de la mensajería *SOAP*; mientras que en *Jesslet*, los
mensajes XML que se reciben son gestionados por las clases del paquete *JAX-WS*
de Java. Estas últimas determinan qué método será invocado en la clase
principal, y, consecuentemente, en el motor de Jess (`jess.Rete`) en sí; tal
como se especificó anteriormente.

![Figura 7.15. Conexión entre PyJesslet y el Jesslet.](imgs/7_15_pyjesslet_jesslet.png "Figura 7.15. Conexión entre PyJesslet y el Jesslet.")

La Figura 7.16 muestra la ventana principal del *PyJesslet*, junto con algunas
de las opciones del menú principal, las cuales se detallan a continuación:

![Figura 7.16. Ventana Principal de PyJesslet.](imgs/7_16_pyjesslet_main.png "Figura 7.16. Ventana Principal de PyJesslet.")

Esta aplicación, cuyo código fuente se encuentra versionado mediante *Mercurial*
y usando el servicio *BitBucket* como repositorio central en 
[https://bitbucket.org/rgmiranda/pyjesslet][pyjesslet], además fue utilizada
para la implementación del sistema experto **SEVAC**
([ver capítulo VIII][sevac]). 

#### Proyectos
Desde la opción *Proyecto* pueden administrarse los proyectos asociados. Un
proyecto es, en esencia, un sistema experto en Jess. El Jesslet requiere para la
invocación de los métodos, un parámetro `PID` en la cabecera el mensaje SOAP.
El valor de este puede ser cualquier cadena de uno o más caracteres, y es el
identificador del proyecto sobre el cual el Jesslet ha de realizar el trabajo.
La Figura 7.17 es una captura de pantalla de la ventana de apertura de
proyectos. Aquí puede verse el nombre del proyecto junto con su *PID* asociado.

![Figura 7.17. Ventana de Apertura de Proyectos.](imgs/7_17_pyjesslet_proyectos.png "Figura 7.17. Ventana de Apertura de Proyectos.")

#### Administración de las Reglas de Inferencia
Las reglas de inferencia pueden ser administradas accediendo a la opción *Ver
Todas* en el submenú *Reglas de Inferencia*. *Nueva* abre directamente el
formulario para agregar una nueva regla en el sistema.

La Figura 7.18 muestra el formulario de administración de reglas, en el cual se
listan reglas de inferencias relacionadas con la aplicación de la vacuna *BCG*,
durante la implementación del **SEVAC** usando la herramienta *PyJesslet*.
Desde esta ventana puede crearse una nueva regla, actualizar una existente, o
eliminarla con los botones en la parte inferior.

![Figura 7.18. Ventana de administración de reglas de inferencias.](imgs/7_18_pyjesslet_reglas.png "Figura 7.18. Ventana de administración de reglas de inferencias.")

El formulario para una nueva regla de inferencia permite cargar el *nombre* de
la regla, una *descripción* breve que será utilizada como documentación de la
misma, las *condiciones* (antecedente) de activación, y las *acciones* a ser
llevadas a cabo al ser disparada. Hay que notar que el nombre de una regla no
puede ser alterado una vez que esta ha sido creada. Esta situación puede verse
en el formulario presentado en la Figura 7.19: el formulario posee deshabilitado
el campo de texto correspondiente.

Los antecedentes aparecen en el listado dentro del primer panel (titulado
*Condiciones*) junto con las opciones para su gestión, y los consecuentes se
presentan en el segundo (titulado *Acciones*).

![Figura 7.19. Formulario de Edición de una Regla.](imgs/7_19_pyjesslet_regla.png "Figura 7.19. Formulario de Edición de una Regla.")

Un detalle que podría prestar a confusión y conviene aclarar es la notación con
la cual se expresan las reglas de inferencia, sus condiciones y acciones en los
formularios. Las reglas de inferencias en la ventana de la Figura 7.18, y las
condiciones y acciones de la Figura 7.19 se expresan usando el **lenguaje Jess**
([ver capítulo III][jess]). Esto no necesariamente debe ser así. Internamente,
las reglas, acciones, y condiciones son **objetos** con sus atributos y métodos,
y estos pueden ser presentados de innumerables formas. Se optó por expresarlos
de esta manera a los fines de poder llevar un mejor control sobre los datos que
se están comunicando desde y hacia el Jesslet. Recordemos que se trata de un
prototipo, y muchas de sus funcionalidades pueden ser propensas a fallos o
provocar situaciones inesperadas.

#### Antecedentes de la Reglas
Los antecedentes de una regla de inferencia son todos los hechos (junto con sus
restricciones) que determinan su activación. Cuando se agrega o modifica una
condición, se presenta el formulario que aparece en la Figura 7.20.

![Figura 7.20. Formulario de Edición de una Condición.](imgs/7_20_pyjesslet_condicion.png "Figura 7.20. Formulario de Edición de una Condición.")

El **nombre del hecho** es el nombre *cabecera* que posee el hecho en la memoria
de trabajo. El **alias** es el *identificador de la variable* usada para
almacenar el hecho y su valor. Si la condición es marcada con **negar**, se le
indicará al motor de inferencias que este hecho *no debe existir* para cumplir
con la condición. El panel de **filtros** permite establecer restricciones con
respecto al valor de este u otros hechos que aparezcan entre los antecedentes de
la regla.

En el formulario puede verse como el filtro se define y expresa como un *árbol*.
Esto se realizó de esta forma debido a que existe una traducción casi directa
entre un árbol de operaciones y una expresión en lenguaje *Jess*, y visualizar
las expresiones de esta manera simplificaría la depuración de errores lógicos y
de programación. La expresión del ejemplo se convierte en la siguiente:

~~~commonlisp
(< (datetime-diff ?f-value (datetime-now) d) 7)
~~~

Esta expresión (*booleana*) será utilizada como **restricción de función
predicado** dentro de la condición. En este caso, devuelve `TRUE` si la
diferencia en días entre valor `?f-value` y la fecha y hora actual es menor a 7,
es decir, si la persona tiene menos de 7 días de vida.

Desde los controles que aparecen en la parte de abajo del panel de filtros puede
definirse si estos son una *expresión literal* (`"Una cadena"`, `256`, `TRUE`),
o una *expresión compuesta* (`(= 8 (* 2 2 2))`). Si se trata de esta última,
entonces se le permite agregar otras expresiones anidadas para dar lugar a los
parámetros.

#### Consecuentes de la Reglas
Los consecuentes de las reglas son todas las acciones que se llevan a cabo al
ser disparada la regla, y, como ya fue mencionado, para evitar una alta
complejidad, el conjunto de acciones disponibles en el Jesslet fue limitado a
tres: creación de hechos
(`assert`), eliminación de hechos (`remove`), y de presentación de mensajes para
el operador del sistema (`recommend`). En el panel de *Acciones* puede verse una
serie de botones que se corresponden con estos tipos de acciones, y abren el
formulario correspondiente.

En la Figura 7.21 se presenta el formulario que permite establecer una acción
para agregar un hecho a la memoria de trabajo. Debido a que el valor que puede
contener un hecho puede ser una expresión, aparece un control de vista de árbol
similar al presentado para la definición de los filtros de un hecho en el
antecedente.

![Figura 7.21. Formulario de Acción de Creación de Hecho.](imgs/7_21_pyjesslet_assert.png "Figura 7.21. Formulario de Acción de Creación de Hecho.")

La eliminación de un hecho sólo demanda el ingreso del **identificador** de
este. Este identificador debe ser el **valor de un alias** de un hecho definido
en alguna de las condiciones del antecedente, **antepuesto con el carácter**
`@`. La ventana en la Figura 7.22 contiene el formulario para esta acción.

![Figura 7.22. Formulario de Acción de Eliminación de un Hecho.](imgs/7_22_pyjesslet_remove.png "Figura 7.22. Formulario de Acción de Eliminación de un Hecho.")

Para disponer una acción de recomendación para el operador también se debe
establecer como único parámetro el mensaje a ser presentado (Figura 7.23).

![Figura 7.23. Formulario de Acción de Recomendación.](imgs/7_23_pyjesslet_recommend.png "Figura 7.23. Formulario de Acción de Recomendación.")

#### Hechos
Los hechos de la memoria de trabajo del proyecto se administran desde la opción
*Hechos* del menú principal. Si se accede a *Ver Hechos*, se abre una ventana
con el listado de hechos que actualmente posee el proyecto. Cabe aclarar que el
hecho inicial (`initial-fact`), creado cuando se invoca al método `reset` de
Jess, no aparece en este listado. La Figura 7.24, que muestra esta ventana,
contiene un único hecho (además del inicial) `fecha-nacimiento`.

![Figura 7.24. Administración de Hechos.](imgs/7_24_pyjesslet_hechos.png "Figura 7.24. Administración de Hechos.")

También puede accederse directamente al formulario de creación de un nuevo hecho
desde *Nuevo Hecho* en el menú principal, o bien desde el botón *Nuevo* en la
ventana del listado. Cualesquiera de estas opciones, abre el formulario para
definir un hecho en la memoria de trabajo del proyecto. La Figura 7.25 muestra
este formulario en donde se solicita el nombre del hecho, una descripción breve,
y su valor.

![Figura 7.25. Formulario de Edición de un Hecho.](imgs/7_25_pyjesslet_hecho.png "Figura 7.25. Formulario de Edición de un Hecho.")

#### Restablecimiento y Ejecución
Para restablecer un proyecto se utiliza la opción *Restablecer* de la parte de
*Proyecto* en el menú principal, con lo cual se invoca al método `Reset` en el
Jesslet. Si la operación ha sido exitosa, en la caja de presentación de mensajes
en el formulario principal aparece algo como lo siguiente:

~~~text
Restableciendo el proyecto <pid>..

El proyecto a sido restablecido.
~~~

Vale recordar que este método elimina todos los hechos de la memoria de trabajo
y crear el hecho inicial cero, por lo que su invocación debe hacerse antes de
cargar lo hechos para una ejecución.

La ejecución de un proyecto se hace mediante la opción *Ejecutar*, con lo que se
invoca al método `Run` en el Jesslet. Durante la ejecución pueden activarse y
desactivarse reglas, crearse y borrarse hechos, y generarse mensajes para el
operador. Estos mensajes (recomendaciones), de existir, aparecerán en la caja de
mensajes. En el ejemplo que aparece a continuación, después de la ejecución se
recomienda aplicar la vacuna *BCG* al beneficiario.

~~~text
Ejecutando el proyecto <pid>...

Se ha terminado con la ejecución del programa.
Aplicar BCG (Esquema Normal)
~~~

Si la ejecución no ha devuelto recomendación alguna, entonces el mensaje de
resultado se verá como el siguiente:

~~~text
Ejecutando el proyecto <pid>...

Se ha terminado con la ejecución del programa.
No se ha producido recomendación alguna.
~~~

Por último, resulta importante destacar que para este prototipo inicial, aún no
se ha trabajado sobre la presentación de la traza de activación y ejecución de
reglas de inferencia, y la generación y eliminación de hechos.

## Resumen
En esta parte se ha expuesto el proceso seguido para la creación del prototipo
inicial del *Jesslet*. En primer lugar, se han establecido herramientas básicas
para el desarrollo del software, como los estándares de codificación y el
sistema de control de versiones a utilizar. Posteriormente, se presentaron las
historias de usuario definidas para poder llegar al producto final, y cómo
fueron programadas a lo largo de una serie de ciclos de desarrollo, tal como lo
propone la XP. En cada ciclo se ha mostrado cómo ha sido creada cada historia,
junto con los detalles y cuestiones más sobresalientes que emergieron en el
proceso. Finalmente, se ha presentado el programa *PyJesslet*, el cual tiene la
finalidad de administrar proyectos en el *Jesslet*, sus reglas de inferencia y
hechos, y ejecutarlos para recuperar sus recomendaciones. Este último fue
desarrollado con el fin de poner de manifiesto la capacidad y versatilidad del
*Jesslet* al momento de trabajar con el *shell Jess* desde diferentes lenguajes.

## Referencias
1. \[[ISO8601][ISO8601]\] ISO. Data elements and interchange formats -
Information interchange - Representation of dates and times. 2004.
2. \[[SQLITE][SQLITE]\] SQLite. https://www.sqlite.org/
3. \[[SUN1997][SUN1997]\] Java Code Conventions. Sun Microsystems 1997.
4. \[[W3CHTTP11][W3CHTTP11]\] W3C. Hypertext Transfer Protocol -
HTTP/1.1. 10 Status Code Definitions.

[1]: http://junit.org/ "JUnit es un conjunto de bibliotecas creadas por Erich Gamma y Kent Beck que son utilizadas en programación para hacer pruebas unitarias de aplicaciones Java."
[2]: https://trello.com/b/sTFkSF16/jesslet "Trello es una aplicación para la administración de proyectos. El proyecto del Jesslet puede encontrarse en la dirección https://trello.com/b/sTFkSF16"
[3]: http://docs.oracle.com/javase/1.5.0/docs/guide/language/annotations.html "Una anotación de Java es una forma de añadir metadatos, al código fuente, que están disponibles para la aplicación en tiempo de ejecución. Es una forma de programación declarativa dentro de Java."

[servicios-web]:04-servicios-web.md "Servicios Web"
[jess]:03-jess.md "Introducción a Jess"
[xp]:05-xp.md "Programación Extrema"
[sistemas-expertos]:02-sistemas-expertos.md "Sistemas Expertos"
[sevac]:08-vacunas-jess.md "Sistema de Recomendación de Vacunas"
[jesslet]:https://bitbucket.org/rgmiranda/jesslet "Proyecto Jesslet en Bitbucket"
[sdf]: http://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html "Class SimpleDateFormat - Java Platform SE 7"

[ISO8601]:http://www.iso.org/iso/home/store/catalogue_tc/catalogue_detail.htm?csnumber=40874 "ISO 8601: Representation of dates and times"
[SQLITE]:https://www.sqlite.org/ "SQLite"
[SUN1997]:http://www.oracle.com/technetwork/java/codeconventions-150003.pdf "Java Code Conventions"
[W3CHTTP11]:http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html "W3C. Hypertext Transfer Protocol -- HTTP/1.1. 10 Status Code Definitions"
