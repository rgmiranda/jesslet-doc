# *SEVAC*, Sistema de Sugerencia de Vacunas en Jess

El shell [Jess][JESS] es un potente motor para la creación de sistemas expertos
basados en reglas de inferencia. A los fines de demostrar sus capacidades, en
este capítulo se desarrolló el **sistema experto SEVAC** para realizar
sugerencias sobre la aplicación de vacunas a una persona, basándose en su
edad e historial de inmunizaciones aplicadas.

Estas sugerencias estarán basadas en el **[calendario nacional de
vacunación][CALENDARIO]** y en la información de sobre inmunizaciones provista
por la Dra. Florencia Coronel, asesora del presente trabajo.

## Dominio de Aplicación
En el contexto de este sistema experto de sugerencias, existen ciertos conceptos
que deben ser desarrollados a los fines de evitar malentendidos y definiciones
ambiguas.

En primer lugar, en relación al sistema, una **inmunización** se entiende como
una combinación de una vacuna, y su número de dosis. Por ejemplo:

- *Hepatitis B 2da Dosis*
- *SABIN Refuerzo*
- *Quíntuple 3ra Dosis*

Por otro lado, un **esquema** es una clasificación de las inmunizaciones
aplicadas que se realiza en base a la *edad de la persona* y los *factores de
riesgo* que afectaban a la persona al momento ser inmunizado. Los esquemas que
pueden encontrarse son:

- **Esquema Normal:** La aplicación de la dosis se ha realizado de acuerdo al
calendario y en situaciones ideales contempladas para la vacuna.
- **Esquema Alternativo:** La aplicación de la dosis se ha realizado de acuerdo
a parámetros que no podrían ser considerados normales, pero que igualmente
resultan aceptables.
- **Fuera de Esquema:** La aplicación se ha realizado en parámetros fuera de lo
normal.

El esquema en el cual habrá de clasificarse cada dosis dependerá exclusivamente
de la vacuna correspondiente.

## Fuentes de Conocimiento
El conocimiento para la creación del *SEVAC* se obtuvo desde dos fuentes:

- Información pública respecto al *Calendario Nacional de Vacunación* brindada
por el *Ministerio de Salud de la Nación*
<http://www.msal.gob.ar/index.php/programas-y-planes/184-calendario-nacional-de-vacunacion-2016>.
- Entrevistas realizadas a la Dra. Coronel.

Cabe aclarar que lo que finalmente resultó ser la educción de conocimientos para
SEVAC originalmente no se hizo para desarrollar un sistema experto, sino en el
marco del desarrollo de otro sistema solicitado por la Dra. Coronel. El
propósito de esta aplicación es el *registro de aplicaciones de vacunas* del
Calendario Nacional en los centros de salud habilitados.

## Proceso de Desarrollo
El desarrollo del _**SEVAC**_ se realizó siguiendo la metodología **IDEAL**
propuesta por \[[GARCIA2004]\]. Debido al contexto en el cual fue desarrollado
este software, muchas etapas de la metodología fueron obviadas. Puntualmente,
se realizó solamente la fase II, la cual abarca la creación de los prototipos
del sistema experto, haciendo hincapié en las etapas de **conceptualización** y
de **formalización**.

En este punto cabe notar que el sistema no tiene casi interacción con el
operador. Existen dos motivos para esta decisión:

1. Dada la gran cantidad de vacunas y dosis involucradas, evidentemente el
preguntar al operador sobre las inmunizaciones recibidas por una persona puede
resultar molesto y poco usable.
2. Debido a que se busca luego convertir este mismo sistema (o una versión
análoga) en un componente a modo de servicio web, la interacción con el usuario
debe ser reducida. Una de las ideas fundamentales, en este sentido, es que su
principal interacción ha de ser con otros sistemas y no tanto con personas.

A futuro, podría pensarse en mejorar esta primera aproximación, y darle la
posibilidad al sistema de reportar un calendario completo con rangos de fechas
dentro de los cuales deberían aplicarse las inmunizaciones a la persona.

### Conceptualización
Durante esta etapa se crearon los modelos para representar los **conocimientos
fácticos** y los **conocimientos tácticos**.

Para los primeros se trabajó el **diccionario de conceptos** y las **tablas de
concepto-atributo-valor**. El diccionario de conceptos permite identificar la
terminología clave y de más alto nivel. Para cada concepto se establece su
utilidad o función, sinónimos, y los atributos que lo definen. Una tabla de
concepto-atributo-valor se utiliza para ordenar los conceptos identificados, y
en esta se definen los atributos de los conceptos en un grado más de detalle,
incluyendo los valores que pueden tomar los mismos.

Los conocimientos tácticos fueron abordados mediante las **tablas de decisión**
y los **grafos de causalidad**. Las primeras sirven para describir procesos y
procedimientos de información. En estas tablas se presentan las condiciones
(conjunto de circunstancias) que dan lugar a un conjunto de acciones. Una tabla
típica se divide en cuatro cuadrantes. El primer cuadrante lista las
condiciones, y en el segundo aparecen las acciones. El tercer cuadrante contiene
las combinaciones de valores de las condiciones que darán lugar a la toma de
decisión, y el cuarto indica qué acciones se deben realizar en cada caso. Por
otro lado, los grafos de causalidad aparecen como una aproximación a las
**reglas de producción** con las que se llenará la base de conocimientos del
*SEVAC*. Un grafo causal es una representación automáticamente manipulable del
conocimiento asociado a los procesos deductivos del experto de campo, la cual
permite comparar el procedimiento que realiza el experto de campo con el que
realizará el sistema. Se construyen en base a las tablas de decisión, y a las
entrevistas y observaciones realizadas con la persona experta.

El diccionario de conceptos, que aparece en la Tabla 8.1, lista y define los
conceptos y términos que se manejan dentro del dominio del sistema.

Concepto           |Función                                                    |Sinónimos   |Atributos
-------------------|-----------------------------------------------------------|------------|----------------------------------------------------------------------------------------------
Beneficiario       |Persona que recibió una aplicación de una vacuna.          |Persona     |Posee nombre, apellido, un número de documento (DNI), y fecha de nacimiento.
Vacuna             |Una de las vacunas especificadas en el Calendario Nacional.|            |Se compone por el nombre y un código alfanumérico identificatorio.
Dosis              |Una de las dosis asociadas a una vacuna del Calendario.    |            |Una dosis asociada a una vacuna que contiene un nombre, y un código numérico.
Aplicación         |Una aplicación de una dosis de una vacuna a una persona.   |Inmunización|Se compone esencialmente de una dosis de una vacuna, una persona, y una fecha de aplicación.

La tabla concepto-atributo-valor (Tabla 8.2) ahonda en detalles para cada
concepto, presentando sus atributos, y los valores que éstos pueden tomar.

Concepto           |Atributos              |Valor                 |Observaciones
-------------------|-----------------------|----------------------|-------------
Beneficiario       |Nombre                 |Cadena de caracteres  |
Beneficiario       |Apellido               |Cadena de caracteres  |
Beneficiario       |Número de DNI          |Número entero positivo|
Beneficiario       |Fecha de Nacimiento    |Fecha                 |
Vacuna             |Nombre                 |Cadena de caracteres  |
Vacuna             |Código                 |Cadena de caracteres  |Código único interno de una vacuna
Dosis              |Nombre                 |Cadena de caracteres  |
Dosis              |Orden                  |Número entero positivo|Indica el orden de la dosis dentro del esquema de la vacuna
Dosis              |Código                 |Número entero positivo|Código de la dosis para la vacuna
Dosis              |Vacuna                 |Cadena de caracteres  |Código único de la vacuna asociada a la dosis
Aplicación         |Fecha de Aplicación    |Fecha                 |
Aplicación         |Vacuna                 |Cadena de caracteres  |Código único de la vacuna aplicada
Aplicación         |Dosis                  |Número entero positivo|Código de la dosis aplicada
Aplicación         |Número de DNI          |Número entero positivo|

Como se mencionó previamente, el objetivo del *SEVAC* es presentar alertas de
aplicación de vacunas basándose en la edad de la persona y su historial de
inmunizaciones. Debido al gran número de vacunas que aparecen en el calendario,
y todas los rangos etarios a ser analizados, tanto las tablas de decisión como
los grafos de causalidad del *SEVAC* se presentan discriminadas por cada vacuna
tratada.

## Las Vacunas
Las vacunas a ser incluidas en el desarrollo de este sistema de sugerencia
son las siguientes:

1. **BCG (Tuberculosis)**
2. **Hepatitis B**
3. **Neumococo Conjugada (Meningitis y Neumonía)**
4. **Quíntuple (Difteria, Tos Convulsa, Tétanos, Hepatitis B, Influenza B)**
5. **Cuádruple (Difteria, Tos Convulsa, Tétanos, Influenza B)**
6. **SABIN (Poliomielitis)**
7. **Triple Viral (Sarampión, Rubeola, Paperas)**
8. **Triple Bacteriana Celular (Difteria, Tos Convulsa, Tétanos)**
9. **Triple Bacteriana Acelular (Difteria, Tos Convulsa, Tétanos)**
10. **Hepatitis A**
11. **HPV**

Como puede apreciarse, no todas las vacunas del calendario han sido
consideradas. La razón de esto es que no todos los casos requieren decisiones
que pueden ser consideradas lo suficientemente complejas. Los casos más
sencillos han sido dejados de lado de momento.

### BCG
La vacuna BCG es una vacuna contra la Meningitis y la forma miliar de la
Tuberculosis, y consta de una única dosis. Dependiendo de la edad de la persona,
la aplicación de esta dosis única puede ubicarse en los siguientes esquemas:

- **Antes de los 7 días** de vida del niño se reporta en el **esquema normal**.
- **Entre los 7 días y el año** de edad se reporta en el **esquema
alternativo**.
- **Entre uno y cinco años** se reporta como **fuera de esquema**.
- A niños **mayores de cinco años**, no se les puede aplicar esta vacuna.

La Tabla 8.3 contiene la tabla de decisiones para esta vacuna.

BCG                  |                  |                       |                  |
---------------------|------------------|-----------------------|------------------|--------------
Edad del Beneficiario|`menor que 7 días`|`entre 7 días y un año`|`entre 1 y 5 años`|`5 años o más`
Aplicada Dosis BCG   |`No`              |`No`                   |`No`              |`No`
**Acciones**         |                  |                       |                  |
Aplicar BCG          |`Sí`              |`Sí`                   |`Sí`              |`No`
Reportar Esquema     |`Normal`          |`Alternativo`          |`Fuera de Esquema`|`NO APLICA`

A continuación se presenta el grafo de causalidad para esta vacuna, junto con la
tabla de símbolos (Tabla 8.4) y sus significados.

![Figura 8.1. Grafo de causalidad BCG.](imgs/8_1_bcg.png "Figura 8.1. Grafo de Causalidad BCG.")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`bcg-aplicada = TRUE`
`B`    |`edad < 7 días`
`C`    |`edad < 5 años`
`D`    |`edad < 1 año`
`1`    |`reportar-esquema-normal = TRUE`
`2`    |`aplicar-bcg = TRUE`
`3`    |`reportar-bcg-esquema-alternativo = TRUE`
`4`    |`reportar-bcg-fuera-esquema = TRUE`

### Hepatitis B
La vacuna contra la Hepatitis B se compone de *tres dosis*, y su esquema se
define de la siguiente manera:

- Si la **1ra dosis** se aplica **antes de la 12 horas de vida**, entonces se
inicia como un **esquema normal**.
- Si la **1ra dosis** es aplicada **entre las 12 horas y el mes de vida**,
entonces se inicia **fuera de esquema**.
- Si el niño tiene la **1ra dosis aplicada antes del mes**, el esquema puede ser
completado (2da y 3ra dosis) mediante la aplicación de la **Quíntuple
(Pentavalente)**.
- A partir de los **11 años de edad**, es posible iniciar o completar el esquema
en caso de ser necesario. La **2da dosis** debe ser aplicada con **un mes** de
diferencia de la 1ra, y la **3ra dosis**, con **6 meses** de diferencia de la
2da.
- Las dosis aplicadas a los **11 años** se reportan dentro de un **esquema
normal**.
- Toda dosis aplicada **entre los 12 y 17 años** debe ser reportada como **fuera
de esquema**.
- Las dosis que se apliquen a **mayores de 18 años** se reportan en un **esquema
alternativo**.

La Tablas 8.5, 8.6 y 8.7 presentan las tablas de decisión discriminadas por cada
dosis de la vacuna.

Hepatitis B - Primera Dosis   |                   |                         |                  |                    |
------------------------------|-------------------|-------------------------|------------------|--------------------|---------------------
Edad del Beneficiario         |`menor de 12 horas`|`entre 12 horas y un mes`|`11 años`         |`entre 12 y 17 años`|`18 años o más`
Aplicada 1ra Dosis Hepatitis B|`No`               |`No`                     |`No`              |`No`                |`No`
Aplicada 2da Dosis Hepatitis B|`No`               |`No`                     |`No`              |`No`                |`No`
Aplicada 3ra Dosis Hepatitis B|`No`               |`No`                     |`No`              |`No`                |`No`
Aplicada 2da Dosis Quíntuple  |`No`               |`No`                     |`No`              |`No`                |`No`
Aplicada 3ra Dosis Quíntuple  |`No`               |`No`                     |`No`              |`No`                |`No`
**Acciones**                  |                   |                         |                  |                    |
Aplicar 1ra Dosis Hepatitis B |`Sí`               |`Sí`                     |`Sí`              |`Sí`                |`Sí`
Reportar Esquema              |`Normal`           |`Fuera de Esquema`       |`Esquema Normal`  |`Fuera de Esquema`  |`Esquema Alternativo`

Hepatitis B - Segunda Dosis   |                  |                    |
------------------------------|------------------|--------------------|---------------------
Edad del Beneficiario         |`11 años`         |`entre 12 y 17 años`|`18 años o más`
Aplicada 1ra Dosis Hepatitis B|`Sí`              |`Sí`                |`Sí`
Aplicada 2da Dosis Hepatitis B|`No`              |`No`                |`No`
Aplicada 3ra Dosis Hepatitis B|`No`              |`No`                |`No`
Aplicada 2da Dosis Quíntuple  |`No`              |`No`                |`No`
Aplicada 3ra Dosis Quíntuple  |`No`              |`No`                |`No`
**Acciones**                  |                  |                    |
Aplicar 2da Dosis Hepatitis B |`Sí`              |`Sí`                |`Sí`
Reportar Esquema              |`Esquema Normal`  |`Fuera de Esquema`  |`Esquema Alternativo`

Hepatitis B - Tercera Dosis   |                  |                    |                     |                  |                    |
------------------------------|------------------|--------------------|---------------------|------------------|--------------------|---------------------
Edad del Beneficiario         |`11 años`         |`entre 12 y 17 años`|`18 años o más`      |`11 años`         |`entre 12 y 17 años`|`18 años o más`
Aplicada 1ra Dosis Hepatitis B|`Sí`              |`Sí`                |`Sí`                 |`Sí`              |`Sí`                |`Sí`
Aplicada 2da Dosis Hepatitis B|`Sí`              |`Sí`                |`Sí`                 |`No`              |`No`                |`No`
Aplicada 3ra Dosis Hepatitis B|`No`              |`No`                |`No`                 |`No`              |`No`                |`No`
Aplicada 2da Dosis Quíntuple  |`No`              |`No`                |`No`                 |`Sí`              |`Sí`                |`Sí`
Aplicada 3ra Dosis Quíntuple  |`No`              |`No`                |`No`                 |`No`              |`No`                |`No`
**Acciones**                  |                  |                    |                     |                  |                    |
Aplicar 3ra Dosis Hepatitis B |`Sí`              |`Sí`                |`Sí`                 |`Sí`              |`Sí`                |`Sí`
Reportar Esquema              |`Esquema Normal`  |`Fuera de Esquema`  |`Esquema Alternativo`|`Esquema Normal`  |`Fuera de Esquema`  |`Esquema Alternativo`

La Figura 8.2 y la Tabla 8.8 presentan el grafo de causalidad resultante, y sus
símbolos respectivamente.

![Figura 8.2. Grafo de causalidad Hepatitis B.](imgs/8_2_hepatitis_b.png "Figura 8.2. Grafo de Causalidad Hepatitis B.")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad < 1 mes`
`B`    |`edad < 12 horas`
`C`    |`edad >= 11 años`
`D`    |`edad >= 12 años`
`E`    |`edad >= 18 años`
`F`    |`hb-1-aplicada = TRUE`
`G`    |`hb-2-aplicada = TRUE`
`H`    |`hb-2c-aplicada = TRUE`
`I`    |`dpthb-2-aplicada = TRUE`
`J`    |`hb-3-aplicada = TRUE`
`K`    |`hb-3c-aplicada = TRUE`
`L`    |`dpthb-3-aplicada = TRUE`
`M`    |`DIFF( fecha-inmunizacion-hb-1, fecha-actual ) > 1 mes`
`N`    |`DIFF( fecha-inmunizacion-hb-2, fecha-actual ) > 1 mes`
`O`    |`DIFF( fecha-nacimiento, fecha-inmunizacion-hb-1 ) < 1 mes`
`P`    |`DIFF( fecha-nacimiento, fecha-inmunizacion-hb-1 ) >= 11 años`
`1`    |`aplicar-hb-1-recien-nacido = TRUE`
`2`    |`aplicar-hb-1-mayores = TRUE`
`3`    |`esquema-hb-2-completo = FALSE`
`4`    |`chequear-dpthb-2 = TRUE`
`5`    |`esquema-dpthb-2-completo = FALSE`
`6`    |`continuar-esquema-dpthb-2 = TRUE`
`7`    |`chequear-diferencia-hb-1 = TRUE`
`8`    |`continuar-esquema-hb-2 = TRUE`
`9`    |`aplicar-hb-2 = TRUE`
`10`   |`reportar-hb-2-normal = TRUE`
`11`   |`reportar-hb-2-fuera-esquema = TRUE`
`12`   |`reportar-hb-2-alternativo = TRUE`
`13`   |`esquema-dpthb-2-completo = TRUE`
`14`   |`esquema-hb-2-completo = TRUE`
`15`   |`reportar-hb-1-normal = TRUE`
`16`   |`reportar-hb-1-fuera-esquema = TRUE`
`17`   |`esquema-dpthb-3-completo = FALSE`
`18`   |`completar-esquema-dpthb-3 = TRUE`
`19`   |`completar-esquema-hb-3 = TRUE`
`20`   |`aplicar-hb-3 = TRUE`
`21`   |`reportar-hb-3-normal = TRUE`
`22`   |`reportar-hb-3-fuera-esquema = TRUE`
`23`   |`reportar-hb-3-alternativo = TRUE`
`24`   |`reportar-hb-1-dosis-pediatrica = TRUE`
`25`   |`reportar-hb-1-fuera-esquema = TRUE`
`26`   |`reportar-hb-1-adultos = TRUE`

### SABIN
La vacuna SABIN es la vacuna contra la **poliomielitis** que se aplica de
manera oral. Consta de cuatro dosis y un refuerzo, y su aplicación de estas
posee las siguientes restricciones:

- Idealmente, la **1ra dosis** debe iniciar el esquema **entre los 2 y 4 meses**
de vida.
- La **2da dosis** debe ser aplicada **a partir de los 4 hasta los 6 meses** del
niño, y con una diferencia mínima de **un mes** con respecto a la aplicación de
la primera.
- Una **3ra dosis** se aplica **entre los 6 y los 18 meses** de edad, y también
debe tener una diferencia mínima de **un mes** con respecto a la anterior.
- La aplicación de la **4ta dosis** debe hacerse **entre los 18 y 24 meses**, de
edad, y con una diferencia mínima de **seis meses** con respecto a la tercera.
- El **refuerzo** debe ser aplicado a los **5 o 6 años de vida**; durante el
ingreso escolar.
- Todas las restricciones antes descriptas configuran el **esquema normal** de
aplicación de la vacuna SABIN.
- **Fuera de esquema**, puede aplicarse esta vacuna **entre los 2 y los 18
años**, pero se excluye, en este caso, el **refuerzo**. Sólo corresponderían las
las cuatro dosis, con los mismos períodos de separación mínimos descriptos
previamente.

Sabin - Primera Dosis   |                       |                   |
------------------------|-----------------------|-------------------|-----------------
Edad del Beneficiario   |`entre 2 y 4 meses`    |`entre 2 y 18 años`|`mayor a 18 años`
Aplicada 1ra Dosis Sabin|`No`                   |`No`               |`Cualesquiera`
**Acciones**            |                       |                   |
Aplicar 1ra Dosis Sabin |`Sí`                   |`Sí`               |`No`
Reportar Esquema        |`Esquema Normal`       |`Fuera de Esquema` |`NO APLICA`

Sabin - Segunda Dosis                   |                       |                       |
----------------------------------------|-----------------------|-----------------------|-----------------
Edad del Beneficiario                   |`entre 4 y 6 meses`    |`entre 2 y 18 años`    |`mayor a 18 años`
Aplicada 1ra Dosis Sabin                |`Sí`                   |`Sí`                   |`Cualesquiera`
Aplicada 2da Dosis Sabin                |`No`                   |`No`                   |`Cualesquiera`
Diferencia Fecha Aplicación 1ra Dosis   |`mayor o igual a 1 mes`|`mayor o igual a 1 mes`|`Cualesquiera`
**Acciones**                            |                       |                       |
Aplicar 2da Dosis Sabin                 |`Sí`                   |`Sí`                   |`No`
Reportar Esquema                        |`Esquema Normal`       |`Fuera de Esquema`     |`NO APLICA`

Sabin - Tercera Dosis                   |                       |                       |
----------------------------------------|-----------------------|-----------------------|-----------------
Edad del Beneficiario                   |`entre 6 y 18 meses`   |`entre 2 y 18 años`    |`mayor a 18 años`
Aplicada 1ra Dosis Sabin                |`Sí`                   |`Sí`                   |`Cualesquiera`
Aplicada 2da Dosis Sabin                |`Sí`                   |`Sí`                   |`Cualesquiera`
Aplicada 3ra Dosis Sabin                |`No`                   |`No`                   |`Cualesquiera`
Diferencia Fecha Aplicación 2da Dosis   |`mayor o igual a 1 mes`|`mayor o igual a 1 mes`|`Cualesquiera`
**Acciones**                            |                       |                       |
Aplicar 3ra Dosis Sabin                 |`Sí`                   |`Sí`                   |`No`
Reportar Esquema                        |`Esquema Normal`       |`Fuera de Esquema`     |`NO APLICA`

Sabin - Cuarta Dosis                    |                       |                       |
----------------------------------------|-----------------------|-----------------------|-----------------
Edad del Beneficiario                   |`entre 18 y 24 meses`  |`entre 2 y 18 años`    |`mayor a 18 años`
Aplicada 1ra Dosis Sabin                |`Sí`                   |`Sí`                   |`Cualesquiera`
Aplicada 2da Dosis Sabin                |`Sí`                   |`Sí`                   |`Cualesquiera`
Aplicada 3ra Dosis Sabin                |`Sí`                   |`Sí`                   |`Cualesquiera`
Aplicada 4ta Dosis Sabin                |`No`                   |`No`                   |`Cualesquiera`
Diferencia Fecha Aplicación 3ra Dosis   |`mayor o igual a 1 mes`|`mayor o igual a 1 mes`|`Cualesquiera`
**Acciones**                            |                       |                       |
Aplicar 4ta Dosis Sabin                 |`Sí`                   |`Sí`                   |`No`
Reportar Esquema                        |`Esquema Normal`       |`Fuera de Esquema`     |`NO APLICA`

Sabin - Refuerzo                |                       |
--------------------------------|-----------------------|--------------------------------
Edad del Beneficiario           |`entre 5 y 6 años`     |`menor que 5 o mayor que 6 años`
Aplicada 1ra Dosis Sabin        |`Sí`                   |`Cualesquiera`
Aplicada 2da Dosis Sabin        |`Sí`                   |`Cualesquiera`
Aplicada 3ra Dosis Sabin        |`Sí`                   |`Cualesquiera`
Aplicada 4ta Dosis Sabin        |`Sí`                   |`Cualesquiera`
Edad Aplicación 4ta Dosis Sabin |`menor que 24 meses`   |`Cualesquiera`
**Acciones**                    |                       |
Aplicar Refuerzo Sabin          |`Sí`                   |`No`
Reportar Esquema                |`Esquema Normal`       |`NO APLICA`

![Figura 8.3. Grafo de causalidad SABIN](imgs/8_3_sabin.png "Figura 8.3. Grafo de causalidad SABIN.")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad >= 2 meses`
`B`    |`edad >= 4 meses`
`C`    |`edad >= 6 meses`
`D`    |`edad >= 18 meses`
`E`    |`edad >= 5 años`
`F`    |`edad <= 6 años`
`G`    |`edad >= 2 años`
`H`    |`edad <= 18 años`
`I`    |`sabin-1-aplicada = TRUE`
`J`    |`DIFF( sabin-1-fecha-inmunizacion, fecha-actual ) >= 1 mes`
`K`    |`sabin-2-aplicada = TRUE`
`L`    |`DIFF( sabin-2-fecha-inmunizacion, fecha-actual ) >= 1 mes`
`M`    |`sabin-3-aplicada = TRUE`
`N`    |`DIFF( sabin-3-fecha-inmunizacion, fecha-actual ) >= 6 meses`
`O`    |`sabin-4-aplicada = TRUE`
`P`    |`sabin-99-aplicada = TRUE`
`Q`    |`edad-inmunizacion-sabin-4 < 2 años`
`1`    |`tiene-2-meses = TRUE`
`2`    |`tiene-12-18-anios = TRUE`
`3`    |`aplicar-sabin-1 = TRUE`
`4`    |`aplicar-sabin-1-fuera-esquema = TRUE`
`5`    |`tiene-4-meses = TRUE`
`6`    |`aplicar-sabin-2 = TRUE`
`7`    |`aplicar-sabin-2-fuera-esquema = TRUE`
`8`    |`tiene-6-meses = TRUE`
`9`    |`aplicar-sabin-3 = TRUE`
`10`   |`aplicar-sabin-3-fuera-esquema = TRUE`
`11`   |`tiene-18-meses = TRUE`
`12`   |`aplicar-sabin-4 = TRUE`
`13`   |`aplicar-sabin-4-fuera-esquema = TRUE`
`14`   |`aplicar-sabin-99 = TRUE`

### Quíntuple (Pentavalente)
Esta vacuna previene la *Difteria*, la *Tos Convulsa*, *Tétanos*, *Hepatitis B*,
y la *Influenza B*, y consta de **tres dosis**:

- La **1ra dosis** debe ser aplicada, idealmente, **entre los 2 y 4 meses** de
vida para iniciar el esquema.
- La **2da dosis** ha de ser aplicada **entre los 4 y los 6 meses** del niño, y
con una diferencia mínima de **un mes** con respecto a la aplicación de la
primera.
- Finalmente, la **3ra dosis** se aplica **entre los 6 y los 18 meses** de edad,
y esta también debe tener una diferencia mínima de **un mes** con respecto a la
anterior.
- Las condiciones antes descriptas configuran el **esquema normal** de la
inmunización. Las dosis recibidas en cualquier otra edad se consideran como un
**esquema alternativo**.
- El esquema de la pentavalente puede ser completado hasta los **5 años** del
niño. No debe aplicarse dosis alguna pasada esta edad.
- De no estar disponible una dosis de la **Quíntuple**, puede ser aplicada una
de la **Cuádruple** junto con una de **Hepatitis B**.

Quíntuple - Primera Dosis   |                       |                           |
----------------------------|-----------------------|---------------------------|--------------------------------------
Edad del Beneficiario       |`entre 2 y 4 meses`    |`entre 4 meses y 5 años`   |`menor que 2 meses o mayor que 5 años`
Aplicada 1ra Dosis Quíntuple|`No`                   |`No`                       |`Cualesquiera`
**Acciones**                |                       |                           |
Aplicar 1ra Dosis Quíntuple |`Sí`                   |`Sí`                       |`No`
Reportar Esquema            |`Esquema Normal`       |`Esquema Alternativo`      |`NO APLICA`

Quíntuple - Segunda Dosis               |                       |                           |
----------------------------------------|-----------------------|---------------------------|--------------------------------------
Edad del Beneficiario                   |`entre 4 y 6 meses`    |`entre 6 meses y 5 años`   |`menor que 4 meses o mayor que 5 años`
Aplicada 1ra Dosis Quíntuple            |`Sí`                   |`Sí`                       |`Cualesquiera`
Aplicada 2da Dosis Quíntuple            |`No`                   |`No`                       |`Cualesquiera`
Diferencia Fecha Aplicación 1ra Dosis   |`mayor o igual a 1 mes`|`mayor o igual a 1 mes`    |`Cualesquiera`
**Acciones**                            |                       |                           |
Aplicar 2da Dosis Quíntuple             |`Sí`                   |`Sí`                       |`No`
Reportar Esquema                        |`Esquema Normal`       |`Esquema Alternativo`      |`NO APLICA`

Quíntuple - Tercera Dosis               |                       |                           |
----------------------------------------|-----------------------|---------------------------|--------------------------------------
Edad del Beneficiario                   |`entre 6 y 18 meses`   |`entre 18 meses y 5 años`  |`menor que 6 meses o mayor que 5 años`
Aplicada 1ra Dosis Quíntuple            |`Sí`                   |`Sí`                       |`Cualesquiera`
Aplicada 2da Dosis Quíntuple            |`Sí`                   |`Sí`                       |`Cualesquiera`
Aplicada 3ra Dosis Quíntuple            |`No`                   |`No`                       |`Cualesquiera`
Diferencia Fecha Aplicación 2da Dosis   |`mayor o igual a 1 mes`|`mayor o igual a 1 mes`    |`Cualesquiera`
**Acciones**                            |                       |                           |
Aplicar 2da Dosis Quíntuple             |`Sí`                   |`Sí`                       |`No`
Reportar Esquema                        |`Esquema Normal`       |`Esquema Alternativo`      |`NO APLICA`

A continuación se presenta el grafo de causalidad de la vacuna. El código
asignado es `dpthb`.

![Figura 8.4. Grafo de causalidad Quíntuple.](imgs/8_4_quintuple.png "Figura 8.4. Grafo de Causalidad Quíntuple")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad >= 2 meses`
`B`    |`edad >= 4 meses`
`C`    |`edad >= 6 meses`
`D`    |`edad <= 18 meses`
`E`    |`edad < 5 años`
`F`    |`dpthb-1-aplicada = TRUE`
`G`    |`dpthb-2-aplicada = TRUE`
`H`    |`dpthb-3-aplicada = TRUE`
`I`    |`DIFF( fecha-inmunizacion-dpthb-1, fecha-actual ) >= 1 meses`
`J`    |`DIFF( fecha-inmunizacion-dpthb-2, fecha-actual ) >= 1 meses`
`1`    |`tiene-edad-dpthb = TRUE`
`2`    |`aplicar-dpthb-1 = TRUE`
`3`    |`reportar-dpthb-1-normal = TRUE`
`4`    |`reportar-dpthb-1-alternativo = TRUE`
`5`    |`reportar-dpthb-esquema-normal = "Aplicación Óptima"`
`6`    |`reportar-dpthb-esquema-alternativo = "Aplicación Esquema Alternativo`
`7`    |`aplicar-dpthb-2 = TRUE`
`8`    |`reportar-dpthb-2-normal = TRUE`
`9`    |`reportar-dpthb-2-alternativo = TRUE`
`10`   |`aplicar-dpthb-3 = TRUE`
`11`   |`reportar-dpthb-3-normal = TRUE`
`12`   |`reportar-dpthb-3-alternativo = TRUE`

### Cuádruple
La vacuna cuádruple tiene la finalidad de prevenir la *Difteria*, la *Tos
Convulsa*, *Tétanos*, e *Influenza B*, es decir, las mismas patologías que la
**Quíntuple**, excluida la *Hepatitis B*. Su **esquema normal** consta de tan
sólo una dosis que funciona como una *cuarta dosis* del esquema de esta, y debe
ser aplicada **entre los 15 y los 18 meses** de vida.

Sin embargo, en un **esquema alternativo**, puede ser aplicada junto con una
dosis de la **Hepatitis B** como una alternativa la **Quíntuple**. En este caso
en particular, ambas vacunas poseen exactamente las misma restricciones. En el
ámbito de este proyecto, este último esquema no será considerado.

Cuádruple - Cuarta Dosis    |                       |
----------------------------|-----------------------|-----------------------------------------
Edad del Beneficiario       |`entre 15 y 18 meses`  |`menor que 15 meses o mayor que 18 meses`
Aplicada 1ra Dosis Quíntuple|`Sí`                   |`Cualesquiera`
Aplicada 2da Dosis Quíntuple|`Sí`                   |`Cualesquiera`
Aplicada 3ra Dosis Quíntuple|`Sí`                   |`Cualesquiera`
**Acciones**                |                       |
Aplicar Dosis Cuádruple     |`Sí`                   |`No`
Reportar Esquema            |`Esquema Normal`       |`NO APLICA`

La Figura de a continuación presenta el grafo de casualidad para la vacuna
Cuádruple, usando como código para esta, el símbolo `dpth`.

![Figura 8.5. Grafo de causalidad Cuádruple.](imgs/8_5_cuadruple.png "Figura 8.5. Grafo de Causalidad Cuádruple")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad >= 15 meses`
`B`    |`edad <= 18 meses`
`C`    |`dpth-99-aplicada = TRUE`
`D`    |`dpthb-3-aplicada = TRUE`
`E`    |`DIFF( fecha-inmunizacion-dpthb-3, fecha-actual ) >= 6 meses`
`1`    |`aplicar-dpth-99 = TRUE`

### Neumococo Conjugada (13 Valente)
La vacuna Antineumococo Conjugada previene la *Meningitis*, *Neumonía*, y
*Sepsis por Neumococo*. Se compone de **tres dosis**, y su esquema de aplicación
tiene las siguientes condiciones:

- Para iniciar el esquema, la **1ra dosis** debe ser aplicada **a partir de los
2 meses** de vida del niño.
- La **2da dosis** debe ser aplicada **a partir de los 4 meses** de edad, y con
una **diferencia mínima de dos meses** con respecto a la primera.
- Finalmente, la **3ra dosis** debe aplicarse **desde los 12 meses**, y debe
tener una **diferencia mínima de dos meses** con respecto a la segunda
aplicación.
 - El esquema debe completarse **antes de los 18 meses** del niño. No se puede
realizar aplicación alguna a partir de esta edad.

Neumococo Conjugada - Primera Dosis |                           |
------------------------------------|---------------------------|-----------------------------------------
Edad del Beneficiario               |`mayor o igual que 2 meses`|`mayor que 18 meses`
Aplicada 1ra Dosis Neumococo Conj.  |`No`                       |`Cualesquiera`
**Acciones**                        |                           |
Aplicar 1ra Dosis Neumococo Conj.   |`Sí`                       |`No`
Reportar Esquema                    |`Esquema Normal`           |`NO APLICA`

Neumococo Conjugada - Segunda Dosis |                           |
------------------------------------|---------------------------|-----------------------------------------
Edad del Beneficiario               |`mayor o igual que 4 meses`|`mayor que 18 meses`
Aplicada 1ra Dosis Neumococo Conj.  |`Sí`                       |`Cualesquiera`
Aplicada 2da Dosis Neumococo Conj.  |`No`                       |`Cualesquiera`
Diferencia Fecha Aplic. 1ra Dosis   |`mayor o igual a 2 meses`    |`Cualesquiera`
**Acciones**                        |                           |
Aplicar 2da Dosis Neumococo Conj.   |`Sí`                       |`No`
Reportar Esquema                    |`Esquema Normal`           |`NO APLICA`

Neumococo Conjugada - Tercera Dosis |                               |
------------------------------------|-------------------------------|-----------------------------------------
Edad del Beneficiario               |`mayor o igual que 12 meses`   |`mayor que 18 meses`
Aplicada 1ra Dosis Neumococo Conj.  |`Sí`                           |`Cualesquiera`
Aplicada 2da Dosis Neumococo Conj.  |`Sí`                           |`Cualesquiera`
Aplicada 3ra Dosis Neumococo Conj.  |`No`                           |`Cualesquiera`
Diferencia Fecha Aplic. 2da Dosis   |`mayor o igual a 2 mes`        |`Cualesquiera`
**Acciones**                        |                               |
Aplicar 3ra Dosis Neumococo Conj.   |`Sí`                           |`No`
Reportar Esquema                    |`Esquema Normal`               |`NO APLICA`

En la Figura 8.6 que aparece abajo se define el grafo de causalidad para la
vacuna Neumococo Conjugada, utilizando `neumo` como símbolo dentro de las
reglas.

![Figura 8.6. Grafo de Causalidad Neumococo Conjugada](imgs/8_6_neumococo_conjugada.png "Figura 8.6. Grafo de Causalidad Neumococo Conjugada")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad >= 2 meses`
`B`    |`edad >= 4 meses`
`C`    |`edad >= 12 meses`
`D`    |`edad < 18 meses`
`E`    |`DIFF( fecha-inmunizacion-neumo-1, fecha-actual ) >= 2 meses`
`F`    |`DIFF( fecha-inmunizacion-neumo-2, fecha-actual ) >= 2 meses`
`G`    |`neumo-1-aplicada = TRUE`
`H`    |`neumo-2-aplicada = TRUE`
`I`    |`neumo-3-aplicada = TRUE`
`1`    |`tiene-edad-neumo = TRUE`
`2`    |`aplicar-neumo-1 = TRUE`
`3`    |`aplicar-neumo-2 = TRUE`
`4`    |`aplicar-neumo-3 = TRUE`

### Triple Viral
Esta vacuna previene el *Sarampión*, la *Rubeola* y *Paperas*, y su esquema se
divide en dos dosis.

- La **1ra dosis** debe ser aplicada a los **12 meses** del niño.
- La **2da dosis** se aplica a los **5 o 6 años** de edad, durante el ingreso
escolar. De no estar disponible esta vacuna, es posible aplicar una dosis de
**Doble Viral** en su lugar.
- Como **esquema alternativo**, pueden aplicarse la **2da dosis** a los **11
años** de edad, si no hubiera recibido dos dosis de **Triple Viral**, o una de
**Triple Viral** más una de **Doble Viral**.

Triple Viral - Primera Dosis    |                   |
--------------------------------|-------------------|----------------------------
Edad del Beneficiario           |`12 meses`         |`menor o mayor que 12 meses`
Aplicada 1ra Dosis Triple Viral |`No`               |`Cualesquiera`
**Acciones**                    |                   |
Aplicar 1ra Dosis Triple Viral  |`Sí`               |`No`
Reportar Esquema                |`Esquema Normal`   |`NO APLICA`

Triple Viral - Segunda Dosis    |                   |                       |
--------------------------------|-------------------|-----------------------|----------------------------
Edad del Beneficiario           |`5 o 6 años`       |`11 años`              |`distinto a 5, 6 u 11 años`
Aplicada 1ra Dosis Triple Viral |`Sí`               |`Sí`                   |`Cualesquiera`
Aplicada 2da Dosis Triple Viral |`No`               |`No`                   |`Cualesquiera`
**Acciones**                    |                   |                       |
Aplicar 2da Dosis Triple Viral  |`Sí`               |`Sí`                   |`No`
Reportar Esquema                |`Esquema Normal`   |`Esquema Alternativo`  |`NO APLICA`

La Figura 8.7 muestra el grafo de causalidad de la Triple Viral (`srp`) con las
restricciones de edad antes expuestas.

![Figura 8.7. Grafo de Causalidad Triple Viral](imgs/8_7_triple_viral.png "Figura 8.7. Grafo de Causalidad Triple Viral")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad = 12 meses`
`B`    |`edad >= 5 años`
`C`    |`edad <= 6 años`
`D`    |`edad = 11 años`
`E`    |`srp-1-aplicada = TRUE`
`F`    |`srp-2-aplicada = TRUE`
`G`    |`sr-0-aplicada = TRUE`
`1`    |`aplicar-srp-1 = TRUE`
`2`    |`aplicar-srp-2 = TRUE`
`3`    |`aplicar-srp-2-esquema-alternativo = TRUE`

### Doble Viral
La vacuna Doble Viral es una vacuna contra el *Sarampión* y la *Rubeola*. Consta
de una **dosis única**, y debe ser aplicada a **adultos**, **puérperas** y
**personal de salud** que no han completado el esquema de la **Triple Viral**.
Debido a que este proyecto sólo abarca *restricciones relativas a la edad*, y no
las *asociadas al grupo de riesgo* de la persona que recibe la inmunización,
estas dos últimas condiciones no serán tomadas en cuenta.

La dosis de la Doble Viral También puede ser aplicada como una **alternativa** a
la **segunda dosis** del esquema de la **Triple Viral**.

Doble Viral                     |                           |
--------------------------------|---------------------------|-------------------
Edad del Beneficiario           |`mayor o igual a 18 años`  |`menor que 18 años`
Aplicada 2da Dosis Doble Viral  |`No`                       |`Cualesquiera`
**Acciones**                    |                           |
Aplicar Dosis Doble Viral       |`Sí`                       |`No`
Reportar Esquema                |`Esquema Normal`           |`NO APLICA`

El grafo de esta vacuna (`sr`) resulta relativamente escueto, dadas las
condiciones de edad arriba presentadas.

![Figura 8.8. Grafo de Causalidad Doble Viral](imgs/8_8_doble_viral.png "Figura 8.8. Grafo de Causalidad Doble Viral")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad >= 18 años`
`B`    |`srp-2-aplicada = TRUE`
`C`    |`sr-0-aplicada = TRUE`
`1`    |`aplicar-sr-0 = TRUE`

### Triple Bacteriana Celular
Esta vacuna previene la *Difteria*, el *Tétanos* y la *Tos Convulsa*, y su
esquema posee una **dosis única** que debe ser aplicada como **2do refuerzo** a
la **Quíntuple** a los **5 o 6 años** de edad. No se puede vacunar a niños de
**7 años o más**.

Triple Bacteriana Celular       |                   |
--------------------------------|-------------------|--------------------------------
Edad del Beneficiario           |`5 o 6 años`       |`menor que 5 o mayor que 6 años`
Aplicada Dosis Cuádruple        |`Sí`               |`Cualesquiera`
Aplicada Dosis Triple Bacteriana|`No`               |`Cualesquiera`
**Acciones**                    |                   |
Aplicar Dosis Triple Bacteriana |`Sí`               |`No`
Reportar Esquema                |`Esquema Normal`   |`NO APLICA`

![Figura 8.9. Grafo de Causalidad Triple Bacteriana Celular](imgs/8_9_triple_bacteriana_celular.png "Figura 8.9. Grafo de Causalidad Triple Bacteriana Celular")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad >= 5 años`
`B`    |`edad <= 6 años`
`C`    |`dpt-99-aplicada = TRUE`
`D`    |`dpth-99-aplicada = TRUE`
`1`    |`aplicar-dpt-99 = TRUE`

### Triple Bacteriana Acelular
La vacuna Triple Bacteriana Acelular previene la *Difteria*, el *Tétanos* y la
*Tos Convulsa Acelular*, y posee un esquema de una **dosis única** a modo de
**refuerzo** para la **Quíntuple**. Esta dosis debe ser aplicada a niños de **11
años** de edad, **personal de salud** que atiene a **menores de 1 año**, o
**mujeres embarazadas** a partir de la **semana 20 de gestación**.

Nuevamente, en el alcance de este proyecto quedan excluidas las condiciones
relacionadas con los **grupos de riesgo** (personal de saluda y mujeres
embarazadas, en este caso), por lo que sólo se tomará en cuenta a los niños con
11 años de edad.

Triple Bacteriana Acelular          |                   |
------------------------------------|-------------------|---------------------------
Edad del Beneficiario               |`11 años`          |`menor o mayor que 11 años`
Aplicada Dosis Triple Bacteriana    |`Sí`               |`Cualesquiera`
Aplicada Dosis Triple Bacteriana Ac.|`No`               |`Cualesquiera`
**Acciones**                        |                   |
Aplicar Dosis Triple Bacteriana Ac. |`Sí`               |`No`
Reportar Esquema                    |`Esquema Normal`   |`NO APLICA`

![Figura 8.10. Grafo de Causalidad Triple Bacteriana Acelular](imgs/8_10_triple_bacteriana_acelular.png "Figura 8.10. Grafo de Causalidad Triple Bacteriana Acelular")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad = 11 años`
`B`    |`dpta-99-aplicada = TRUE`
`C`    |`dpt-99-aplicada = TRUE`
`1`    |`aplicar-dpta-99 = TRUE`

### Hepatitis A
La vacuna contra la Hepatitis A posee una **dosis única** que debe ser aplicada
a los **12 meses** de edad.

Hepatitis A             |                   |
------------------------|-------------------|----------------------------
Edad del Beneficiario   |`12 meses`         |`menor o mayor que 12 meses`
Aplicada Hepatitis A    |`No`               |`Cualesquiera`
**Acciones**            |                   |
Aplicar Hepatitis A     |`Sí`               |`No`
Reportar Esquema        |`Esquema Normal`   |`NO APLICA`

![Figura 8.11. Grafo de Causalidad Hepatitis A](imgs/8_11_hepatitis_a.png "Figura 8.11. Grafo de Causalidad Hepatitis A")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad = 12 meses`
`B`    |`ha-0-aplicada = TRUE`
`1`    |`aplicar-ha-0 = TRUE`

### VPH
Esta vacuna previene el **Virus del Papiloma Humano**, y se aplica solamente a
las **niñas de 11 años** de edad. Su esquema se compone de **tres dosis** con
las siguientes condiciones:

- La **2da dosis** debe ser aplicada con una **diferencia de un mes** de la
aplicación de la **1ra dosis**.
- La **3ra dosis** debe aplicarse **a partir de los 3 meses** de la aplicación
de la **2da dosis**.

VPH - Primera Dosis     |                   |
------------------------|-------------------|---------------------------
Edad del Beneficiario   |`11 años`          |`menor o mayor que 11 años`
Aplicada VPH 1ra Dosis  |`No`               |`Cualesquiera`
**Acciones**            |                   |
Aplicar VPH 1ra Dosis   |`Sí`               |`No`
Reportar Esquema        |`Esquema Normal`   |`NO APLICA`

VPH - Segunda Dosis                 |                       |
------------------------------------|-----------------------|---------------------------
Edad del Beneficiario               |`11 años`              |`menor o mayor que 11 años`
Aplicada VPH 1ra Dosis              |`Sí`                   |`Cualesquiera`
Aplicada VPH 2da Dosis              |`No`                   |`Cualesquiera`
Diferencia Fecha Aplic. 1ra Dosis   |`mayor o igual a 1 mes`|`Cualesquiera`
**Acciones**                        |                       |
Aplicar VPH 2da Dosis               |`Sí`                   |`No`
Reportar Esquema                    |`Esquema Normal`       |`NO APLICA`

VPH - Tercera Dosis                 |                           |
------------------------------------|---------------------------|---------------------------
Edad del Beneficiario               |`11 años`                  |`menor o mayor que 11 años`
Aplicada VPH 1ra Dosis              |`Sí`                       |`Cualesquiera`
Aplicada VPH 2da Dosis              |`Sí`                       |`Cualesquiera`
Aplicada VPH 3ra Dosis              |`No`                       |`Cualesquiera`
Diferencia Fecha Aplic. 2da Dosis   |`mayor o igual a 3 meses`  |`Cualesquiera`
**Acciones**                        |                           |
Aplicar VPH 3ra Dosis               |`Sí`                       |`No`
Reportar Esquema                    |`Esquema Normal`           |`NO APLICA`

![Figura 8.12. Grafo de causalidad VPH.](imgs/8_12_vph.png "Figura 8.12. Grafo de Causalidad VPH")

Símbolo|Significado
------:|:-----------------------------------------------------------------------
`A`    |`edad = 11 años`
`B`    |`vph-1-aplicada = TRUE`
`C`    |`vph-2-aplicada = TRUE`
`D`    |`vph-3-aplicada = TRUE`
`F`    |`DIFF( fecha-inmunizacion-vph-1, fecha-actual ) >= 1 meses`
`G`    |`DIFF( fecha-inmunizacion-vph-2, fecha-actual ) >= 3 meses`
`1`    |`aplicar-vph-1 = TRUE`
`2`    |`aplicar-vph-2 = TRUE`
`3`    |`aplicar-vph-3 = TRUE`

## Formalización
La formalización del *SEVAC* se realizó mediante la creación de **reglas de
producción** o **reglas de inferencia**, las cuales se desprenden de los grafos
de causalidad obtenidos en la conceptualización. A modo de ejemplo, se muestra
la regla correspondiente al grafo de causalidad de la vacuna BCG (Figura 8.1)

~~~text
IF NOT(bcg-aplicada = TRUE) AND edad < 5 años THEN
    SET aplicar-bcg = TRUE

IF aplicar-bcg = TRUE AND edad < 7 dias THEN
    SET reportar-esquema-normal = TRUE

IF aplicar-bcg = TRUE AND NOT(edad < 7 dias) AND edad < 1 año THEN
    SET reportar-bcg-esquema-alternativo = TRUE

IF aplicar-bcg = TRUE AND NOT(edad < 1 año) THEN
    SET reportar-bcg-fuera-esquema = TRUE
~~~

Todas las reglas de producción se muestran en el [Anexo C][anexo-c].

## Programación e Implementación sobre Jess
La programación del *SEVAC* se hizo mediante la traducción de las reglas de
inferencia definidas para cada vacuna ([ver Anexo C][anexo-c]), a código en
lenguaje de *Jess*. Para cada vacuna se creó un archivo `.clp`, que no es otra
cosa que un *script* que contiene las reglas de inferencia correspondientes. Por
ejemplo, para la vacuna **Triple Bacteriana Acelular**, se tiene un archivo
denominado `dpta.clp`, que contiene lo que aparece en el extracto siguiente.

~~~commonlisp
; +----------------------------------------------------------------------------+
; |    Triple Bacteriana Acelular <dpta>                                       |
; +----------------------------------------------------------------------------+
; | La vacuna Triple Bacteriana Acelular previene la Difteria, el Tétanos y la |
; | Tos Convulsa Acelular, y posee un esquema de una dosis única a modo de     |
; | refuerzo para la Quíntuple. Esta dosis debe ser aplicada a niños de 11     |
; | años de edad, personal de salud que atiene a menores de 1 año, o mujeres   |
; | embarazadas a partir de la semana 20 de gestación.                         |
; |                                                                            |
; | Nuevamente, en el alcance de este proyecto quedan excluídas las            |
; | condiciones relacionadas con los grupos de riesgo (personal de saluda y    |
; | mujeres embarazadas, en este caso), por lo que sólo se tomará en cuenta a  |
; | los niños con 11 años de edad.                                             |
; +----------------------------------------------------------------------------+

( defrule puede-aplicar-dpta
  ( edad-anios ?e &: ( = ?e 11 ) )
  ( dpt-99-aplicada TRUE )
  ( not ( dpta-99-aplicada TRUE ) )
  =>
  ( printout t "Aplicar Triple Bacteriana Acelular" crlf )
)

( provide dpta )
~~~

Dentro del proyecto existen tres archivos especiales:

- `fechahora.clp`: Define funciones para cálculos de fecha, hora e intervalos.
- `edades.clp`: Calcula, utilizando las funciones de `fechahora.clp`, la edad
del beneficiario con diferentes unidades (años, meses, semanas, días, horas).
- `vacunas.clp`: Es el *script* principal y el encargado de ejecutar el *SEVAC*.

Todo el código fuente y pruebas del sistema se encuentra en el repositorio
público **[https://bitbucket.org/rgmiranda/sevac][SEVAC]**, y ha sido versionado
mediante el sistema de control de versiones **Mercurial**.

## Implementación con *PyJesslet*
Con el fin de analizar el desempeño del *PyJesslet* sobre un sistema experto
completo, probar la carga de reglas de inferencias y hechos, y su ejecución, el
*SEVAC* fue implementado mediante esta aplicación de escritorio.
Durante este proceso, el ingreso de las reglas de inferencias especificadas
resultó ser relativamente directa. Vale recordar que los patrones en los hechos
en el antecedente y las acciones susceptibles de ser realizadas en el
consecuente, fueron restringidos para simplificar su representación en los
mensajes SOAP ([ver capítulo VII][jesslet]). Es por ello que la adaptación de
las reglas definidas en el sistema de sugerencia de vacunas se hizo realizando
ciertas modificaciones mínimas sobre las reglas.

En primer lugar, todos los hechos en el Jesslet deben representarse como
**hechos ordenados de valor único**, por lo que algunos patrones en los
antecedentes y ciertas acciones en el consecuente tuvieron que ser cambiados en
oportunamente. Por ejemplo, puede encontrarse acciones para la creación de
hechos de la forma `(assert (aplicar-bcg))`, como se presenta en el extracto
siguiente.

~~~commonlisp
(defrule
  ; ...
  =>
  ; ...
  (assert (aplicar-bcg))
  ; ...
  )
~~~

En el Jesslet, esta acción se transforma de manera tal que el valor asignado a
este hecho es el booleano `TRUE`.

~~~commonlisp
(defrule
  ; ...
  =>
  ; ...
  (assert (aplicar-bcg TRUE))
  ; ...
  )
~~~

Como resultado de esto, el antecedente en donde aparece este hecho:

~~~commonlisp
(defrule
  ; ...
  (aplicar-bcg)
  ; ...
  =>
  ; ...
  )
~~~

En el caso del Jesslet, este patrón se altera de la siguiente manera:

~~~commonlisp
(defrule
  ; ...
  ?a-fact <- (aplicar-bcg ?a-value&:(= ?a-value TRUE))
  ; ...
  =>
  ; ...
  )
~~~

Por otro lado, se ha llevado a cabo una *simplificación conceptual* en relación
a los antecedentes para la activación de ciertas reglas de inferencia. En el
sistema de sugerencia, por ejemplo, para indicar que la primera dosis de la
vacuna SABIN había sido aplicada se deben declarar los hechos `sabin-1-aplicada`
y `fecha-inmunizacion-sabin-1`, el primero con valor booleano `TRUE`, el segundo
con la fecha de la inmunización.

~~~commonlisp
(assert (sabin-1-aplicada TRUE))
(assert (fecha-inmunizacion-sabin-1 2015-09-13T10:24:00))
~~~

En la implementación sobre el Jesslet, las reglas que se activan con estos
patrones han sido modificadas para utilizar un único hecho de la forma
`<vacuna>-<dosis>`, que almacena la fecha de la inmunización. La existencia de
este hecho implicaría que la dosis a sido aplicada, y la no existencia se asume
como la no aplicación de la misma. El ejemplo anterior quedaría resumido:

~~~commonlisp
(assert (sabin-1 2015-09-13T10:24:00))
~~~

En el fragmento siguiente se muestra un resultado de una ejecución definiendo
como único hecho inicial la fecha de nacimiento de la persona.

~~~text
Restableciendo el proyecto FOO...

El proyecto a sido reestablecido.
Agregado: <1> (fecha-nacimiento 2015-10-01T00:00:00)
Ejecutando el proyecto FOO...

Se ha terminado con la ejecución del programa.
Aplicar Quíntuple, 1ra Dosis (Esquema Normal)
Aplicar BCG (Esquema de Hasta 1 Año)
Aplicar Neumococo Conjugada, 1ra Dosis
Aplicar SABIN 1ra Dosis (Esquema Normal)
~~~

Si además de la fecha de nacimiento, se crea un hecho correspondiente a la
primera dosis de la vacuna SABIN, la ejecución se presentaría sin la
recomendación correspondiente.

~~~text
Restableciendo el proyecto FOO...

El proyecto a sido reestablecido.
Agregado: <1> (fecha-nacimiento 2015-10-01T00:00:00)
Agregado: <2> (sabin-1 2015-12-05T00:00:00)
Ejecutando el proyecto FOO...

Se ha terminado con la ejecución del programa.
Aplicar Quíntuple, 1ra Dosis (Esquema Normal)
Aplicar BCG (Esquema de Hasta 1 Año)
Aplicar Neumococo Conjugada, 1ra Dosis
~~~

Estos resultados, devueltos por *PyJesslet*, con las sugerencias de vacunas
demuestran la versatilidad que se consigue al implementar como un servicio web
el shell *Jess*.

## Cliente de Inmunizaciones: _Chrolie_
El *Chrolie* es una extensión para el navegador **Google Chrome** escrita en
lenguaje **Javascript**, la cual fue creada con dos objetivos en mente. Por un
lado, pretende demostrar el funcionamiento del *Jesslet* desde una plataforma
liviana y sin las capacidades de otros ambientes de ejecución, y por el otro,
sirve como un caso de prueba para acceder al conocimiento del *SEVAC* en sí.
Para tal fin, la extensión permite buscar una persona en la base de datos de
inmunizaciones, recuperar sus registros de aplicaciones de vacunas, y,
utilizando estos datos, realizar una consulta sobre la instancia del *SEVAC* en
el _Jesslet_ (presentada en el apartado anterior). La fecha de nacimiento y las
de las aplicaciones de vacunas previamente registradas serán los hechos
utilizados para la ejecución. La Figura 8.13 presenta un diagrama conceptual en
donde aparecen las interacciones del _Chrolie_ con estos sistemas externos.

![Figura 8.13. Diagrama Conceptual del Funcionamiento del Chrolie.](imgs/8_13_chrolie.png "Figura 8.13. Diagrama Conceptual del Funcionamiento del Chrolie.")

El *sistema de inmunizaciones* se encarga de proveer al *Chrolie* de los datos
necesarios, tanto de las personas, como de las inmunizaciones recibidas por
ellas. Las búsquedas sobre este sistema se lleva a cabo mediante un *servicio
web REST* \[[1][REST]\], utilizando *JSON* \[[2][JSON]\] como formato para la
transferencia de los datos. En la Figura 8.14 puede verse la vista de una
búsqueda usando el número de documento `52000000`.

![Figura 8.14. Vista de la Búsqueda de Personas.](imgs/8_14_chrolie_2.png "Figura 8.14. Vista de la Búsqueda de Personas.")

Tras bambalinas, los datos de la búsqueda se devuelven en el formato JSON, el
cual se muestra en el siguiente fragmento.

~~~json
[
  {
    "id": 38646,
    "nombre": "Natalio",
    "apellido": "Natalia",
    "tipo_documento": "dni",
    "documento": "52000000",
    "fecha_nacimiento": "2015-10-01",
    "fecha_alta": "2015-12-17 17:22:12.731667"
  }
]
~~~

El usuario puede elegir la persona sobre la cual se realizará la consulta. La
razón de ser de este mecanismo de selección es el hecho de que pueden existir,
en la base de datos, números de documentos duplicados, con lo cual devolver la
primera persona encontrada puede devenir en resultados inesperados o equívocos.
Una vez escogida la persona de la consulta, *Chrolie* busca las inmunizaciones
que esta ha recibido anteriormente. La Figura 8.15 que aparece a continuación
muestra estos resultados.

![Figura 8.15. Resultados de Recupero de Aplicaciones.](imgs/8_15_chrolie_3.png "Figura 8.15. Resultados de Recupero de Aplicaciones.")

Nuevamente, para este caso, los datos aún se obtienen desde la base de datos de
inmunizaciones se transfieren en formato JSON.

~~~json
[
  {
    "id": 47678,
    "fecha_inmunizacion": "2015-10-01",
    "fecha_alta": "2015-12-17 17:24:13.861095-03",
    "id_beneficiario": 38646,
    "id_vacuna": 2,
    "id_dosis": 2,
    "lote": "xxxx",
    "nombre_beneficiario": "Natalio",
    "apellido_beneficiario": "Natalia",
    "tipo_documento": "dni",
    "documento": "52000000",
    "vacuna": "BCG",
    "codigo_vacuna": "bcg",
    "codigo_dosis": "0",
    "dosis": "Dosis Única",
  },
  {
    "id": 47680,
    "fecha_inmunizacion": "2015-10-01",
    "fecha_alta": "2015-12-17 17:26:14.802725-03",
    "id_beneficiario": 38646,
    "id_vacuna": 3,
    "id_dosis": 3,
    "lote": "xxxx",
    "nombre_beneficiario": "Natalio",
    "apellido_beneficiario": "Natalia",
    "tipo_documento": "dni",
    "documento": "52000000",
    "vacuna": "Hepatitis B",
    "codigo_vacuna": "hb",
    "codigo_dosis": "1",
    "dosis": "Primera Dosis",
  }
]
~~~

Con estos datos recuperados, se crearán los hechos en la memoria de trabajo del
Jesslet para la consulta. Puntualmente, para el caso de las inmunizaciones se
utilizarán `codigo_vacuna` y `codigo_dosis` para formar el nombre del hecho, y
su valor estará dado por `fecha_inmunizacion`. El primer paso antes de la carga
de los hechos es la reinicialización con el método `Reset`. Para poder conseguir
esto, el Chrolie debe generar el mensaje SOAP correspondiente y enviarlo al
*SEVAC* para su procesamiento. Para facilitar la creación de estos mensajes en
Javascript se optó por utilizar la herramienta *[Handlebars][HANDLEBARS]*, con
la cual es posible predefinir *plantillas*, y utilizarlas luego para la
generación de contenido parametrizado. Para el caso del método `Reset`, la
plantilla creada es la que aparece en el siguiente fragmento.

~~~xml
<SOAP-ENV:Envelope xmlns:ns0="{{namespace}}"
    xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header>
        <ns0:PID>{{pid}}</ns0:PID>
    </SOAP-ENV:Header>
    <ns1:Body>
        <ns0:Reset>
        </ns0:Reset>
    </ns1:Body>
</SOAP-ENV:Envelope>
~~~

Como puede verse, dentro de la plantilla hay una sección definida entre dos
llaves `{{pid}}`. Esto funciona a modo de *marcador de posición*, que será luego
reemplazado cuando se genere el contenido final. En este ejemplo, el `pid` no es
otra cosa que el ID del proyecto sobre el cual será ejecutado `Reset`. El
mensaje producido es el siguiente:

~~~xml
<SOAP-ENV:Envelope xmlns:ns0="http://fce.unse.edu.ar/jesslet/"
    xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header>
        <ns0:PID>FOO</ns0:PID>
    </SOAP-ENV:Header>
    <ns1:Body>
        <ns0:Reset>
        </ns0:Reset>
    </ns1:Body>
</SOAP-ENV:Envelope>
~~~

El mensaje generado para la creación del hecho `fecha-nacimiento` se muestra en
el fragmento a continuación:

~~~xml
<SOAP-ENV:Envelope xmlns:ns0="http://fce.unse.edu.ar/jesslet/"
    xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header>
        <ns0:PID>FOO</ns0:PID>
    </SOAP-ENV:Header>
    <ns1:Body>
        <ns0:AssertFact>
            <fact>
                <id></id>
                <name>fecha-nacimiento</name>
                <value xsi:type="ns0:literalExpression">
                    <type>DATETIME</type>
                    <value>2015-10-01T00:00:00</value>
                </value>
            </fact>
        </ns0:AssertFact>
    </ns1:Body>
</SOAP-ENV:Envelope>
~~~

La creación de los hechos correspondientes a las aplicaciones utiliza mensajes
análogos a este último. Una vez que todos los hechos han sido cargados, se
invoca al método `Run` para recuperar las recomendaciones (si es que hay
alguna). La Figura 8.16 muestra los resultados de las recomendaciones
recuperadas.

![Figura 8.16. Recomendaciones en el Chrolie.](imgs/8_16_chrolie_4.png "Figura 8.16. Recomendaciones en el Chrolie.")

De esta forma, se consigue integrar, sin demasiado esfuerzo, toda la
funcionalidad y potencia de un sistema experto completo dentro de una aplicación
relativamente liviana y de rápido desarrollo.

Todo el código de **Chrolie** se encuentra bajo el control de versiones
**Mercurial**, y está alojado también en el servicio *BitBucket*. Puede ser
descargado desde [https://bitbucket.org/rgmiranda/chrolie][chrolie], y su
ejecución se realizada en el contexto del navegador **Google Chrome**, en modo
de depuración

## Resumen
Las reglas definidas en este capítulo para el calendario de vacunación son la
base para el desarrollo del sistema experto experimental sobre *Jess*, y su
análogo sobre el *Jesslet*. Para cada tipo de vacuna implicada se ha realizado
una presentación sobre sus condiciones de aplicación, situaciones excepcionales
(si es que hubiere alguna), y se ha planteado un grafo de causalidad inicial con
las reglas de inferencias planteadas en base a esta información. Posteriormente
se ha presentado el programa resultante de su codificación en lenguaje de *Jess*
para su prueba. Con esto como base, se demostró la implementación del *SEVAC* en
el servicio *Jesslet* a través de la herramienta *PyJesslet*. Finalmente, se
presentó el *Chrolie* como un caso de prueba para la integración del *Jesslet*,
y para el *SEVAC* como sistema experto accesible a través del *Jesslet*.

## Bibliografía

1. \[[GARCIA2004]\] Ramón García Martínez, Paola Britos. Ingeniería de Sistemas
Expertos. 2004. Nueva Librería. ISBN:9789871104154.

[JESS]: http://www.jessrules.com/ "Jess The Rule Engine for the Java Platform."
[CALENDARIO]: http://www.msal.gov.ar/images/stories/calendario-vacunacion/2015-01_calendario-2015_ad_v1.jpg
[GARCIA2004]: http://www.cuspide.com/9789871104154/Ingenieria+De+Sistemas+Expertos/ "Ramón García Martínez, Paola Britos. Ingeniería de Sistemas Expertos."
[SEVAC]: https://bitbucket.org/rgmiranda/sevac "SEVAC en BitBucket"

[jesslet]: ./06-jesslet.md "Desarrollo del Jesslet"
[anexo-c]: ./13-anexo-c-reglas-sevac.md "Reglas de Producción del SEVAC"

[HANDLEBARS]:http://handlebarsjs.com/ "Handlebars"
[JSON]:http://www.json.org "JSON (JavaScript Object Notation) es un formato ligero de texto para el intercambio de datos, el cual se encuentra basado en la notación de objetos del lenguaje Javascript."
[REST]:http://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm "REST (Representational State Transfer) es un estilo de arquitectura para la creación de servicios web en donde los recursos son accesibles mediante representaciones textuales, y permite la ejecución de operaciones carentes de estado. Comúnmente, esto se hace mediante el protocolo HTTP, utilizando sus cuatro verbos: GET, POST, PUT, DELETE para las operaciones ABML."
