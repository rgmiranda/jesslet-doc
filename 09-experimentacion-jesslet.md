# Experimentación con el Jesslet

Una vez finalizada la construcción del servicio Jesslet y sus aplicaciones
clientes, mediante las métricas antes presentadas
([ver capítulo VI][metricas]), se llevó a cabo el análisis del prototipo con el
fin de afirmar o refutar las hipótesis inicialmente planteadas.

## Clases de Prueba
Esta etapa se ocupó de la programación de una aplicación, llamada _**Jessapp**_,
con dos *clases* que actúan como una interfaz entre un sistema y *Jess*. Una de
estas clases fue programada para **acceder de manera directa** a Jess, mientras
que la otra para que hacerlo **a través del Jesslet**. Esta experimentación
parte con la idea de que ambas clases posean la misma interfaz (firma de sus
métodos) y comportamiento, mientras que su implementación interna fuese
distinta, para así poder sobre estas implementaciones, calcular las métricas
anteriormente presentadas ([ver capítulo VI][metricas]).

Debido a que Jess se encuentra escrito en el lenguaje Java, para posibilitar un
acceso más fluido al shell, la programación de estas clases *interfaz* también
se realizó con este lenguaje. Ambas implementaron una `Interface` para
garantizar que poseyeran exactamente los mismos métodos, y sobre ambas se
ejecutaron las mismas pruebas automáticas y se exigieron exactamente los mismos
resultados.

La Figura 9.1 presenta un diagrama con las clases y su relación con la interfaz.
La clase `Jess` realiza un *acceso nativo* utilizando la clase `jess.Rete`,
entre otras; mientras que la clase `Jesslet` utiliza el servicio web
desarrollado para trabajar.

![Figura 9.1. Clases para Acceder a Jess](imgs/9_1_clases_jess.png "Figura 9.1. Clases para Acceder a Jess")

### La Interfaz
La interfaz se denomina `Shell`, y establece el conjunto de métodos que debe
poseer cada una de las clases para funcionar como un *punto de comunicación* con
Jess. La forma en la que sea llevada a cabo esta comunicación, queda al libre
albedrío de cada implementación; ya que la interfaz sólo determina qué
parámetros reciben los métodos y los resultados que deberían ser devueltos.

~~~java
public interface Shell {
    
    public void AgregarHecho(SimpleFact hecho) throws JessappException;
    public void EliminarHecho(int id) throws JessappException;
    public SimpleFact RecuperarHecho(int id) throws JessappException;
    public SimpleFact[] RecuperarTodosHechos() throws JessappException;
    public void AgregarRegla(Rule regla) throws JessappException;
    public void EliminarRegla(String nombreRegla) throws JessappException;
    public Rule RecuperarRegla(String nombreRegla) throws JessappException;
    public Rule[] RecuperarTodasReglas() throws JessappException;
    public void Reiniciar() throws JessappException;
    public Map<PartesRespuesta, Object> Ejecutar() throws JessappException;
    
}
~~~

El fragmento de código de arriba presenta los métodos establecidos para la
interfaz `Shell`. Como puede verse, cada uno de los métodos puede corresponderse
con otro método en la interfaz definida para el *Jesslet*
([ver capítulo VII][jesslet]). Esto se definió así sólo
con los fines de realizar las pruebas de una manera más específica en relación
con el *Jesslet*.

### Acceso por el Jesslet
La primera implementación de la interfaz antes presentada es la clase `Jesslet`.
la cual interactúa con el servicio web *Jesslet* ([ver capítulo VII][jesslet])
para la implementación del comportamiento de cada método. En realidad, podría
verse como una **envoltura** (*wrapper*) del servicio web, ya que, como se
mencionó antes, los métodos se corresponden uno a uno.

Para su implementación se recurrió a la herramienta `wsimport`
\[[3][WSIMPORT]\], la cual permite importar (generar) las clases necesarias para
acceder a un servicio web SOAP, utilizando el documento WSDL correspondiente.
Estas clases se corresponden con las involucradas en la interfaz definida para
el Jesslet, y sirven para facilitar la invocación de cada uno de los métodos y
la creación de los parámetros que estos reciben. Cada clase es *etiquetada*
adecuadamente para indicar a cómo ha de ser convertida desde y hacia una
representación XML necesaria para ser enviada sobre un mensaje SOAP
([ver capítulo IV][servicios-web]). Por ejemplo, la clase `Rule` posee las
siguientes etiquetas:

~~~java
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rule", propOrder = {
    "actions",
    "conditions",
    "description",
    "name"
})
public class Rule {
    // Definición de Rule.
}
~~~

Mediante el parámetro `@XmlType` se indica que esta clase se convertirá en un
elemento `<rule>`, y que será compuesto por elementos `<actions>`,
`<conditions>`, `<description>`, y `<name>`. Cada uno de los elementos
componentes poseen, a su vez, sus propias etiquetas para la conversión a XML.

~~~xml
<rule>
    <name><!-- nombre de la regla --></name>
    <description><!-- descripción de la regla --></description>
    <conditions><!-- condiciones para la activación de regla --></conditions>
    <actions><!-- acciones del consecuente de la regla --></actions>
</rule>
~~~

Todas las clases generadas por `wsimport` pueden ser encontradas bajo el
*paquete* `ar.edu.unse.fce.jesslet` y `ar.edu.unse.fce.jesslet.service`. 

### Acceso Directo
Para un acceso *nativo* al shell Jess, se desarrolló la clase `Jess`. Para
implementar los métodos de la interfaz `Shell`, esta hace uso de las clases que
se encuentran en la biblioteca de Jess (el archivo `jess.jar`), dentro del
paquete `jess.*`. El código que se muestra a continuación es un fragmento de las
importaciones de esta biblioteca que hace la clase `Jess`.

~~~java
// ...
import jess.Defrule;
import jess.Fact;
import jess.HasLHS;
import jess.JessException;
import jess.RU;
import jess.Rete;
import jess.Value;
import jess.ValueVector;
// ...
~~~

Por otro lado, las clases generadas por la herramienta `wsimport` fueron
adaptadas para poder dar soporte dar soporte al acceso directo a Jess. Esto se
hizo debido a que la interfaz `Shell` posee, como parámetros en sus métodos,
objetos de tipos generados por esta herramienta. Por ejemplo, la clase
`ar.edu.unse.fce.jesslet.Rule` fue dotada de un constructor que admite como
parámetros un objeto del tipo `jess.Defrule`, y otro del tipo `jess.Rete`; junto
con otro método `generateSource` para la generación de código en lenguaje Jess.

~~~java
// ...
import ar.edu.unse.fce.jesslet.Expression;
import ar.edu.unse.fce.jesslet.Rule;
import ar.edu.unse.fce.jesslet.SimpleFact;
import ar.edu.unse.fce.jesslet.ValueType;
// ...
~~~

Para cada clase que aparece importada en el código anterior, se ha programado un
método `generateSource`, el cual genera un fragmento de código con las sintaxis
de Jess para ser ejecutado sobre la instancia de `jess.Rete` (el motor de
inferencias de Jess). Esta instancia puede encontrarse en el atributo privado
`engine`. El único caso que no se utiliza `generateSource` para su operación es
cuando se necesita agregar un hecho a la memoria de trabajo. Esto se debe a la
imposibilidad de recuperar el identificador del hecho de hacerse de esta manera.

### Pruebas
Para llevar a cabo las pruebas sobre las clases `Jess` y `Jesslet` se utilizó la
herramienta **JUnit 3** \[[TAHCHIEV2010]\] provista dentro del entorno integrado
de desarrollo **Netbeans**. Todas las clases de prueba del proyecto se
encuentran contenidas en el directorio `test`.

Para asegurar que tanto `Jess` como `Jesslet` devuelven exactamente los mismos
resultados, se creó una clase abstracta denominada `ShellTest` en el espacio de
nombres `ar.edu.unse.fce.jessapp`. Esta clase define un conjunto de métodos
`test*` que utilizan un atributo del tipo `Shell` (la interfaz) para ejecutar el
conjunto de pruebas. De esta manera, estas pruebas se establecen a un nivel de
contrato de interfaz, independientemente de la forma en la que esta sea
implementada. Para probar cada implementación en sí, fueron programadas dos
clases hijas de `ShellTest`: `JessTest` y `JessletTest`. Ambas clases
implementan los métodos `setUp` y `tearDown`, propios de la clase abstracta
`junit.framework.TestCase` (clase base de un caso de prueba en JUnit), en donde
se crea la instancia de `Shell` para ser usadas en los métodos `test*`.

La Figura 9.2 presenta el diagrama de las clases creadas para las pruebas, junto
con estos métodos `test*` presentes en `ShellTest`.

![Figura 9.2. Clases de Pruebas de Acceso a Jess](imgs/9_2_clases_pruebas.png "Figura 9.2. Clases de Pruebas de Acceso a Jess")

El método `testAgregarHecho` ejecuta pruebas sobre la función `AgregarHecho` de
la interfaz `Shell`, el cual inserta un hecho en la memoria de trabajo. Para
esto, genera tres hechos (objetos del tipo `SimpleFact`), y los utiliza como
parámetros para esta función. `testRecuperarHecho` prueba el método
`RecuperarHecho`, el cual agrega hechos de la misma manera que el anterior, y
luego los recupera para determinar si esta última operación es llevada a cabo
correctamente. De manera semejante, `testRecuperarTodosHechos` carga un conjunto
de hechos en la memoria del motor de inferencias, para luego recuperarlos a
todos y determinar si han sido guardados correctamente.

Para las pruebas de los métodos relativos a las reglas de inferencias
(`AgregarRegla`, `EliminarRegla`, `RecuperarRegla`, y `RecuperarTodasReglas`) se
programó una función denominada `createRule`, la cual genera un instancia de la
clase `Rule`. Se procedió de esta forma debido a que la creación de un objeto de
este tipo es un proceso relativamente largo, en el cual deben crearse las
condiciones (`FactCondition`) de activación y las acciones (`Action`) a ser
realizadas al ser disparada. Cada condición posee, a su vez, un conjunto de una
o más expresiones. Por otro lado, cada acción puede tratarse de una creación de
un hecho (`AssertAction`), la eliminación de uno (`RemoveAction`), o la
presentación de un mensaje (`RecommendAction`). La creación de todos estos
objetos se hace en `createRule`. Tanto `testAgregarRegla`, `testEliminarRegla`,
`testRecuperarRegla`, como `testRecuperarTodasReglas` utilizan este método
privado para la creación del objeto `Rule` a ser pasado como parámetro de la
prueba.

Con `testReset` se testea la invocación del método `Reiniciar`, el cual limpia
la memoria de trabajo, y crea el *hecho inicial 0*. Cabe notar que este método
de reinicio no sólo es invocado en esta prueba, sino que aparece también en las
pruebas de funciones de manejo de hechos antes mencionadas.

Finalmente, los métodos `testEjecutar` y `testVerRecomendaciones` ejecutan el
sistema experto definido a través de `Ejecutar`. La diferencia entre estos es
que la segunda prueba está centrada en las recomendaciones devueltas por el
sistema tras la ejecución, mientras que la primera sólo controla que el método
devuelva un resultado, cualquiera sea. `testVerRecomendaciones` invoca a
`createRule`, ya que la regla que este genera imprime una recomendación.

### Métricas de Cohesión y Acoplamiento
Una vez establecidas las implementaciones, tanto para el acceso nativo como para
el acceso a través del servicio web, se procedió a llevar a cabo las mediciones
de la **cohesión (LCOM4)** y del **acoplamiento (CBO)**
([ver capítulo VI][metricas]) sobre las clases `Jess` y `Jesslet`.

#### Cohesión
En primer lugar se realiza el cálculo del valor de **LCOM4** de la clase de
acceso nativo `Jess`. El diagrama que se presenta en la Figura 9.3 muestra los
métodos y atributos de esta clase y sus relaciones. Como puede verse, todas las
funciones se encuentran interconectadas a través del atributo `engine`, lo cual
da como resultado un valor LCOM4 igual a 1.

![Figura 9.3. LCOM4 de la Clase Jess](imgs/9_3_lcom4_jess.png "Figura 9.3. LCOM4 de la Clase Jess")

Análogamente, una situación similar puede apreciarse al calcular esta métrica
sobre la clase `Jesslet`. En la siguiente Figura se presentan las relaciones de
los métodos y atributos, con lo que se obtiene un valor LCOM4, nuevamente, igual
a 1.

![Figura 9.4. LCOM4 de la Clase Jesslet](imgs/9_4_lcom4_jesslet.png "Figura 9.4. LCOM4 de la Clase Jesslet")

Esta no varianza de la métrica en ambas clases puede ser una consecuencia
directa del hecho de que ambas clases implementan la misma interfaz (`Shell`).
Al tener un conjunto de métodos públicos bien definidos, el objetivo de la clase
queda perfectamente identificado, razón por la cual resultaría difícil que LCOM4
arroje un valor distinto a 1. Esta es sólo una conjetura, y se encuentra basada
en la simple observación del caso en cuestión.

Algo que cabe destacar es que, habiéndose obtenido un valor 1 en el cálculo
sobre `Jess`, era esperable obtener el mismo resultado sobre la clase `Jesslet`.

#### Acoplamiento
Para el cálculo del acoplamiento de una clase se utiliza la métrica **CBO**, la
cual contabiliza los **acoples** hacia o desde otras clases. Se entiende por
acople una llamada a un método o acceso a un atributo. Cada clase se contabiliza
sólo una vez, por lo que si existe una que es accedida o invocada más de una
vez, sólo contará como 1.

Una manera sencilla de determinar las clases que utilizan `Jess` y `Jesslet` es
mediante las sentencias de importación al principio de cada clase. Para el
primer caso, se tiene el fragmento siguiente:

~~~java
// ...
import ar.edu.unse.fce.jessapp.config.PropertiesManager;
import ar.edu.unse.fce.jessapp.exceptions.JessappException;
import ar.edu.unse.fce.jesslet.Expression;
import ar.edu.unse.fce.jesslet.Rule;
import ar.edu.unse.fce.jesslet.SimpleFact;
import ar.edu.unse.fce.jesslet.ValueType;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import jess.Defrule;
import jess.Fact;
import jess.HasLHS;
import jess.JessException;
import jess.RU;
import jess.Rete;
import jess.Value;
import jess.ValueVector;
// ...
~~~

Hasta este punto, pueden ser contadas **18 clases acopladas**, sin embargo,
dentro del código también pueden ser encontrados accesos e invocaciones a
`Shell`, `StringArrayWriter` y `PartesRespuesta`, lo cual arroja un total de
**21**. Estas clases no aparecen en la parte de las importaciones debido a que
se encuentran en el mismo paquetes (`ar.edu.unse.fce.jessapp`) que la clase
`Jess`.

En la clase `Jesslet`, por otro lado, se encuentra el siguiente fragmento:

~~~java
// ...
import ar.edu.unse.fce.jesslet.Rule;
import ar.edu.unse.fce.jesslet.RuleArray;
import ar.edu.unse.fce.jesslet.SimpleFact;
import ar.edu.unse.fce.jessapp.exceptions.JessappException;
import ar.edu.unse.fce.jesslet.Response;
import ar.edu.unse.fce.jesslet.service.JessletFault;
import ar.edu.unse.fce.jesslet.service.JessletService;
import ar.edu.unse.fce.jesslet.service.JessletService_Service;
import java.util.HashMap;
import java.util.Map;
// ...
~~~

A estas **10 clases** utilizadas, se les adiciona la interfaz `Shell` y la clase
`PartesRespuesta`, para un total de **12**. Como puede apreciarse, existe una
considerable disminución en el número de clases referenciadas entre la
implementación directa y la realizada a través del *Jesslet*. Este descenso era
lo esperable para esta métrica, debido a que el servicio web permite la
abstracción de las bibliotecas, clases y configuraciones específicas de *Jess*.
Esto queda evidenciado más claramente al notar que la única diferencia entre las
sentencias de importación de clases de ambas implementaciones son las sentencias
en donde se especifican clases dentro de la biblioteca del shell (`jess.*`).

## Repositorio
La aplicación *Jessapp* puede ser encontrada en la dirección web
<https://bitbucket.org/rgmiranda/jessapp>. Dentro del directorio `src` se
encuentra el código fuente para la aplicación con las dos clases usadas para las
pruebas: `Jess` y `Jesslet`. Por otro lado, en la carpeta `test` se encuentran
ubicadas las *pruebas de unidades* (clases `*Test`) sobre la interfaz `Shell`.
Como se mencionó arriba, se utiliza *JUnit 3* para realizar la ejecución de las
pruebas, en el entorno de desarrollo *NetBeans*.

## Resumen
En este capítulo se ha presentado el desarrollo de la aplicación *Jessapp*, la
cual tiene el solo objetivo de servir como un ambiente de pruebas para el
análisis de las métricas propuestas para el análisis del *Jesslet*. A través de
esta, se realizaron los cálculos correspondientes a la **cohesión** y el
**acoplamiento** en una implementación de Jess dentro de otro sistema, tanto de
manera **nativa**, como **a través del Jesslet**. Con los resultados obtenidos,
se pudo apreciar una evidente disminución en el acoplamiento cuando el acceso se
hace a través del Jesslet, mientras que la cohesión se mantuvo sin alteraciones.

## Referencias
1. \[[TAHCHIEV2010]\] Petar Tahchiev, Felipe Leme, Vincent Massol, Gary Gregory.
JUnit in Action Second Edition. 2010. Manning. ISBN 9781935182023.

[TAHCHIEV2010]:https://www.manning.com/books/junit-in-action-second-edition "Petar Tahchiev, Felipe Leme, Vincent Massol, Gary Gregory. JUnit in Action Second Edition."

[metricas]:06-metricas.md "Métricas de Cohesión y Acoplamiento"
[jesslet]:07-jesslet.md "Desarrollo del Jesslet"
[servicios-web]:04-servicios-web.md "Marco Tecnológico: Servicios Web"
[sevac]:08-vacunas-jess.md "Sistema de Sugerencia de Vacunas"

[WSIMPORT]:https://docs.oracle.com/javase/6/docs/technotes/tools/share/wsimport.html "Se usa para generar los archivos necesarios de JAX-WS para hacer a una servicio web accesible, en base a un documento WSDL."
