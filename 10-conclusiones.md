# Conclusiones

En la última etapa de este trabajo se expondrán distintas conclusiones en lo
relativo a varios aspectos del mismo, a saber: el **objetivo general**, los 
**objetivos específicos**, **cuestiones pendientes** que pueden servir de guía
para investigaciones futuras, y, finalmente, una **conclusión final** del
trabajo.

## Objetivo General
Este trabajo supuso la combinación de dos tecnologías relativamente maduras en
el campo de la informática como lo son los *servicios web* y los *sistemas
expertos*. De la misma resultó el **Jesslet**, un software con diversas
posibilidades de aplicación en diferentes campos y disciplinas, y con la ventaja
de poder ser insertado de manera sencilla en los sistemas del mundo
interconectado contemporáneo. Esto fue demostrado con el sistema **PyJesslet**,
el cual fue ademas utilizado para implementar el **sistema de recomendación de
vacunas SEVAC**, como un claro ejemplo de una solución mediante un sistema
experto a un problema concreto. Fue ratificado ademas por el cliente
**Chrolie**, el cual, aunque fue desarrollado utilizando otras herramientas,
utiliza al Jesslet y sus recomendaciones de inmunizaciones, para brindar una
herramienta complementaria a las labores de los encargados de la aplicación de
vacunas en las instituciones de salud.

En funcion de, los resultados obtenidos con el desarrollo del *Jesslet*, y al
correcto funcionamiento de las aplicaciones clientes para las pruebas, se puede
concluir que el objetivo general de **tender a facilitar el uso e implementación
de las prestaciones de los sistemas expertos dentro de otros sistemas y/o
plataformas**, **ha sido cumplimentado**.

## Objetivos Específicos
El primer objetivo específico establecido, el de **eliminar el acoplamiento no
deseado entre Jess y otros sistemas al ser usado como componente**, quizás fue
demasiado ambicioso en un principio, debido a que el acoplamiento es necesario
debido a que los componentes deben comunicarse. Sin embargo, considerando la
primera de las dos hipótesis formuladas en el capítulo VI para este trabajo, es
posible afirmar que la misma fue validada. De acuerdo con la métrica **CBO**
([ver capítulo VI][metricas]) obtenida en la *fase de experimentación*
([ver capítulo IX][experimentacion]), para los casos propuestos, el acoplamiento
ha disminuido de **21 unidades** para el acceso directo al shell Jess, a **12
unidades** para el caso de implementación mediante el Jesslet, con lo cual se
está cumpliendo con el objetivo de diseño que es el de mantener el nivel del
acoplamiento lo mas bajo posible. Esta variación, resulta sensiblemente
significativa para mejorar la mantenibilidad, la reutilización, y la detección y
corrección de errores de software. Vale destacar que se trata de solo un caso de
prueba, por lo que generalizar esta cualidad para todas las aplicaciones puede
resultar altamente especulativo y hasta ingenuo. Sin embargo conviene recordar
que, conceptualmente hablando, uno de los objetivos fundamentales de los
servicios web es la **abstracción** de la tecnología específica para el acceso a
una funcionalidad, por lo que una mejora en la métrica de acoplamiento es lo
esperable.

El segundo objetivo específico buscaba **incrementar la cohesión de Jess al
integrarse dentro de otro sistema**. En relación con esto y considerando ahora
la segunda de las dos hipótesis formuladas, es posible afirmar que la misma no
fue validada. Para realizar la medición de la *cohesión* se utilizó la métrica
**LCOM4** ([ver capítulo VI][metricas]) sobre los mismos componentes tratados
para el cálculo del *acoplamiento*. Las recomendaciones y patrones de diseño
indican que el valor *LCOM4* no debe ser distinto de 1, ya que este determina el
número de *incumbencias* que una clase posee. Tanto el cálculo realizado sobre
la clase que accede directamente a Jess como la que lo hace mediante el Jesslet
han devuelto este valor, con lo cual la hipótesis de que los servicios web
alteran de alguna manera la cohesión no puede ser verificada. No obstante, puede
sugerirse un motivo por el cual puede haberse presentado esta situación.

En este caso puntual, la métrica *LCOM4* aplicada tiene el objetivo principal de
encontrar la cantidad de ocupaciones (*concerns*) que una clase posee. Y para
ello, cuenta la cantidad de conjuntos de métodos interrelacionados dentro de la
misma. Con la definición de un contrato (`Interface`) para el servicio web, se
consigue que las clases que la implementen posean un conjunto de métodos
preestablecidos. La única forma de que la cohesión varíe de una implementación
respecto de otra es que se adicionen nuevos métodos sin relación alguna con los
métodos ya definidos en la interfaz.

## Limitaciones y Aspectos Pendientes
Tal vez uno de los mayores retos que presentó la creación del Jesslet fue la
abstracción del lenguaje Jess y la correcta representación de las estructuras
propias de ese entorno (por ejemplo, reglas, hechos, e invocaciones de
funciones) en un mensaje SOAP. Este shell define un lenguaje mediante el cual
puede realizarse un número ilimitado de acciones como invocar funciones, crear
reglas y hechos, definir clases, e importar clases desde bibliotecas Java
externas. Todo esto implica la formulación de un nuevo lenguaje completo, lo
cual atentaría contra el espíritu y la esencia del proyecto: **abstraer**.

Por estos motivos, y a los fines de aislar y separar los conceptos propios de
Jess, tuvieron que realizarse algunos ajustes sobre lo que el servicio
permite realizar. Jess permite la definición de dos tipos de hechos: los
*hechos ordenados* y los *hechos no ordenados* ([ver capítulo III][jess]).
Para el Jesslet, este conjunto de hechos que es posible manejar tuvo que
limitarse a los **hechos ordenados con un único valor**
([ver capítulo VII][jesslet]). A futuro, debe considerarse el soporte a los
restantes hechos, pero hay que considerar que esto impacta directamente en la
definición de las reglas de inferencias. La decisión de limitar los hechos se
hizo principalmente debido a la heterogeneidad de los patrones que pueden
aparecer en los antecedentes de las reglas de inferencias.

El conjunto de acciones en el consecuente de las reglas de inferencias también
tuvo que ser restringido. En esta parte de una regla puede ejecutarse cualquier
función en Jess. Ya que se pretendía mantener el foco en el manejo de los hechos
en la memoria de trabajo y las recomendaciones para el operador, las únicas
acciones que pueden ser especificadas en una regla en el Jesslet son las de
**creación de un hecho**, **eliminación de un hecho**, y **presentación de un
mensaje** para el usuario ([ver capítulo VII][jesslet]). En desarrollos
sucesivos, puede ser conveniente ampliar este conjunto para darle al operador
mayores opciones a la hora plantear una solución experta. Una de estas acciones
puede ser el uso y asignación de valor a *variables* en Jess.

A medida que se avanzaba con el desarrollo se observó que una propiedad de los
servicios web sobre SOAP limitaba cierto funcionamiento de los sistemas expertos
([ver capítulo VII][jesslet]). Un servicio web en SOAP espera a que un cliente
invoque alguno de sus métodos, en donde todas las invocaciones son
independientes entre sí ([ver capítulo IV][servicios-web]). Empero, en muchos
sistemas expertos es necesaria una interacción con el operador para la solicitud
de datos para la inferencia. Esto no es posible en la versión actual del
*Jesslet* debido a que siempre es el cliente quien inicia la solicitud, nunca el
servidor. Para un correcto funcionamiento, el sistema debe tener cargados todos
los datos antes de la ejecución, ya que no tendrá manera de conseguirlos hasta
que este no haya concluido. En otras palabras, se rige bajo el principio de un
**universo cerrado** ([ver capítulo II][sistemas-expertos]). Esta dificultad,
teóricamente, puede ser atacada en el futuro mediante el concepto de
**sockets**, el cual permite el intercambio de información entre dos programas
en dos vías.

Otro detalle que merece ser notado es que la comparación de las métricas ha sido
realizada sobre una plataforma y un lenguaje particular: Java. Como se explicó
en la etapa de experimentación, su elección no se debió a algo arbitrario, sino
a que el shell Jess se encuentra escrito en este lenguaje, lo que permite su
acceso como una biblioteca desde otros programas en la misma plataforma. Cabría
investigar cuál es el impacto del uso del Jesslet en otras plataformas, de ser
posible la implementación directa de la biblioteca de Jess.

Por último, uno de los objetivos tácitos del trabajo era el de **dar a conocer
los avances y resultados a través de redes sociales y blogs**. Por desgracia, no
fue posible llevar a cabo estas actividades en la forma que se hubiese esperado.
Si bien este documento, junto con todos los sistemas que se describen en él, se
encuentran bajo una licencia pública y es posible descargarlos desde sus
repositorios, la afluencia de entradas en los blogs y publicaciones relacionadas
en las redes sociales no ha alcanzado el nivel o la calidad deseable. Vale
mencionar que esta última consideración posee un carácter altamente subjetivo.
Al tratarse de un objetivo no definido, no están establecidos los parámetros
para su verificación, por lo que el enunciar que solo ha sido cumplimentado
parcialmente no resulta ser más que una apreciación personal.

## Conclusión Final
Como resultado de la investigación y desarrollo realizados, es posible concluir
que el proyecto ha cumplido con los objetivos perseguidos. Además, se ha
conseguido profundizar el conocimiento sobre el shell Jess y sus aplicaciones.
Esto ha hecho que se conozcan sus limitaciones y proponer formas para mejorarlo.
Y el resultado de esas propuestas es el Jesslet; un prototipo funcional de
servicio web que abstrae del shell Jess sus funcionalidades, y permite que éstas
sean fácilmente incorporables en otros entornos.

Sin embargo, este trabajo se encuentra aún con diversas oportunidades para su
mejora. Por un lado, como se mencionó anteriormente, el acceso mediante _SOAP_
al servicio web limita en parte las respuestas y la posibilidad de interacción
de este con las aplicaciones clientes. Una aproximación para atacar este
problema sería la implementación del _Jesslet_ mediante _sockets_, los cuales
permiten un intercambio más fluido y bidireccional de la información.

Por otro lado, de ser utilizado el _Jesslet_ en un ámbito real y de producción,
puede que resulte necesario ampliar o revisar el conjunto de reglas que este
puede soportar, y, sobre todo, las acciones que pueden aparecer en el
consecuente. No obstante, esto implicaría un rediseño de las estructuras de
datos fundamentales para la transferencia de reglas y hechos desde el servicio a
sus clientes y viceversa. Asimismo puede sugerirse una reestructuración interna
para permitir que el servicio pueda utilizar más de un _shell_.

Resulta importante destacar que este proyecto estuvo enmarcado dentro del
**Programa de Becas de Estímulo a las vocaciones científicas** (EVC CIN) en el
período 2012. Además, se ha participado en el **XVIII Congreso Argentino de
Ciencias de la Computación** (CACIC) mediante la presentación de un *paper*
homónimo a este trabajo. En octubre y noviembre del 2012, se realizaron
exposiciones mediante la modalidad póster sobre el Jesslet en las **VII Jornadas
de Ciencia y Tecnología de Facultades de Ingeniería del NOA**, y las en
**Jornadas de Becarios UNSE** respectivamente. Finalmente, en octubre del 2017,
se participó de la **I Jornada de Vinculación con el Medio y Difusión de
Resultados de Investigación y Desarrollos en Informática**.

[sistemas-expertos]:02-sistemas-expertos.md "Sistemas Expertos"
[jess]:03-jess.md "Jess"
[servicios-web]:04-servicios-web.md "Servicios Web"
[metricas]:06-metricas.md "Métricas de Cohesión y Acoplamiento"
[jesslet]:07-jesslet.md "Jesslet"
[experimentacion]:09-experimentacion-jesslet.md "Experimentación con el Jesslet"
