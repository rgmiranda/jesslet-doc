# Anexo A - El Lenguaje Jess
Utilizando una sintaxis similar a la de Lisp, Jess define un lenguaje mediante
el cual es posible no sólo definir reglas de inferencia, sino también la
creación de programas funcionales con cierto grado de complejidad. A esto se le
suma el acceso a las librerías de Java, con lo cual las posibilidades para el
desarrollo con Jess se vuelven infinitas. Los componentes elementales del
lenguaje Jess son: los *símbolos*, los *números*, las *cadenas* y los
*comentarios*.

Los **símbolos** son similares a los identificadores en el lenguaje
Java, pero admiten un conjunto más amplio de caracteres. Junto con los
alfanuméricos, un símbolo puede contener estos caracteres:
`A-Z a-z 0-9 $ * . = + / < > _ ? #`.

Sin embargo, los símbolos poseen algunas restricciones. No puede comenzar con un
número, o con `$`, `?` o `=`, y son sensibles a mayúsculas y minúsculas. Existen
también símbolos con significados especiales. El símbolo `nil` es análogo a 
`null` en Java, y los valores `TRUE` y `FALSE` son los valores booleanos. En los
ejemplos que aparecen a continuación se puede ver el símbolo `crlf`; este
representa un salto de línea dentro de la función `printout`.

~~~commonlisp
C3PO foo-fighters _bar numero#13 e=m*c2 quien?
~~~

Los **números** pueden ser de tres tipos: `INTEGER`, `FLOAT` o `LONG`. El
primero se corresponde con el tipo `int` de Java, el segundo al `double`, y el
último al `long`.

Las **cadenas** de caracteres están delimitadas por comillas dobles. Dentro de
una cadena es posible utilizar la barra invertida `\` para escapar las comillas
dobles y nuevas líneas (`\n`). Estas cadenas son del tipo `STRING` en Jess.

~~~commonlisp
"Madness!" "¡Hola Mundo!" "- ¿Me dice la hora por favor? \n – La hora."
~~~

Los **comentarios** para describir y documentar el código fuente comienzan con
el punto y coma, y se extienden hasta el final de la línea. Pueden aparecer en
cualquier lugar del programa ya que son simplemente ignorados.

~~~commonlisp
; El número PI. No toda la secuencia irracional, por supuesto.
; Supondremos que con este grado de precisión vamos a andar bien.
(bind ?pi 3.14159265359)
~~~

Los espacios en blanco no poseen significado alguno fuera de los límites de las
comillas dobles. Se pueden utilizar para identar el código y hacerlo más legible
a voluntad.

## Listas y Funciones
La unidad estructural básica de Jess es la lista. Una lista es un conjunto de
elementos delimitados por paréntesis, los cuales pueden ser algunos de los 
descriptos anteriormente, u otra lista. Una lista sirve para estructurar tanto
datos como código. El primer elemento de una lista se llama cabecera, y en la
mayoría de los casos tiene un significado especial.

En el lenguaje Jess, todo código es una invocación a una función; y toda función
es una lista en donde la cabecera es un símbolo que indica el nombre de la
función, y los restantes elementos son los parámetros de la misma. En el
extracto siguiente pueden verse algunos ejemplos de llamadas a funciones
utilizando la consola de Jess.

~~~bash
Jess> (* 2 2) 
4 
Jess> (bind ?x 256) 
256 
Jess> ; Un poco de complejidad 
(= (* 2 (* 2 (* 2 (* 2 (* 2 (* 2 (* 2 2))))))) (** 2 8)) 
TRUE 
Jess> ; y tal vez de simplificación... 
(= (* 2 2 2 2 2 2 2 2) 256) 
TRUE 
Jess> ; Es posible incluso jugar con el espacio
; en blanco para la identación del código. 
(bind ?resultado (= 
	(* 81 9) 
	(** 9 3))) 
TRUE 
~~~

Algo que también puede notarse es el hecho de que todo operador (`*`, `+` , `-`,
etc.) es en realidad una función. En Jess no existe el concepto de operador como
se lo conoce en otros lenguajes. Además de las funciones nativas del lenguaje,
en Jess es posible utilizar la API de Java para incorporar librerías, o bien
definir nuevos procedimientos utilizando `deffunction`.

## Variables
Una variable es un contenedor que puede mantener un valor único. Las variables
en Jess tienen la particularidad de no ser tipadas y no necesitan ser declaradas
antes de su uso. El identificador de una variable es un símbolo que comienza con
el signo de interrogación (`?`). Como buena práctica, se recomienda evitar el
uso de asteriscos (`*`), y utilizar sólo guiones medios (`-`) o bajos (`_`) para
separar palabras en estos identificadores.

Los asteriscos están reservados para las variables globales. Una variable global
es un tipo especial de variable que se define utilizando la función `defglobal`,
y su identificador debe comenzar y finalizar con un asterisco. Al ser creada,
una variable global es inicializada mediante un valor determinado. Lo que las
diferencia de las variables comunes es que las globales no son eliminadas cuando
se invoca la función `reset`, sino que se les reasigna el valor con el que
fueron definidas.

~~~bash
Jess> (defglobal ?*y* = 256) ; Se asigna 256 en la variable global ?*y*...
TRUE
Jess> (bind ?x 1024) ; y 1024 en ?x.
1024 
Jess> ?x 
1024 
Jess> ?*y* 
256 
Jess> (bind ?*y* 128) ; Se cambia el valor de ?*y*.
128 
Jess> ?*y* 
128 
Jess> (reset) 
TRUE 
Jess> ?*y* ; y ?*y* vuelve a su valor original (256) y ?x ha sido borrada.
256
~~~

En el fragmento de código que aparece a arriba se pueden ver ejemplos de
asignaciones a variables globales y ordinarias. La función `bind` se utiliza
para asignar valores. Los ejemplos que se han mostrado hasta entonces han sido
realizados ingresando el código directamente en la consola. Esto no resulta para
nada conveniente si se tiene un programa medianamente largo para ser ejecutado y
se lo tiene que reingresar cada vez que se sale de la consola de Jess. La
función `batch` soluciona este problema permitiendo ejecutar un archivo con
código fuente en lenguaje Jess sobre el contexto actual.

Dentro de las funciones disponibles por defecto, también existe un conjunto de
funciones especiales que permiten crear y recuperar información de las listas
como valores. Estas listas se conocen como listas planas. Las funciones para
manejar estas listas, por convención, terminan con `$`.

## Estructuras de Control
El flujo de los algoritmos creados con Jess se controla mediante funciones de
control de flujos especiales como `if`, `for`, `foreach`, `while`, `break`, o
`continue`. Esta funciones se utilizan de manera análoga a sus contrapartes en
Java.

La función `if` recibe como primer parámetro una expresión booleana. Si el
resultado de esta expresión es `TRUE`, se ejecutan las funciones que aparecen
justo después del símbolo `then`. Si es `FALSE`, se ejecutan las funciones luego
de `else`, si es que hubieran algunos.

~~~bash
Jess> (bind ?n (random)); Se asigna un número aleatorio a ?n. 
61597 
Jess> (if (> ?n 10000) then 
    ; Si es mayor que 10000... 
    (printout t ?n " es mayor que 10000." crlf) 
    else 
    ; sino, necesariamente debe ser menor o igual. 
    (printout t ?n " es menor o igual que 10000." crlf)) 
61597 es mayor que 10000. 
~~~

Para las iteraciones pueden ser utilizadas `for`, `foreach` o `while`. En el
fragmento siguiente se puede ver un ejemplo del uso de esta última.

~~~bash
Jess> (bind ?n (mod (random) 10)) 
4 
Jess> (bind ?i 0); Índice para la iteración.. 
0 
Jess> (while (< ?i ?n) do
	(printout t "Imprimiendo " ?i " de " ?n crlf) 
	(++ ?i)) 
Imprimiendo 0 de 4 
Imprimiendo 1 de 4 
Imprimiendo 2 de 4 
Imprimiendo 3 de 4 
FALSE 
~~~

La función `foreach` se utiliza, por ejemplo, para recorrer listas y realizar
acciones sobre cada elemento de estas; y `for` trabaja de manera análoga a Java.
La iteraciones pueden ser controladas mediante las funciones `break` y
`continue`. La primera obliga a una ciclo a terminar inmediatamente, mientras
que `continue` obliga a la iteración a terminar con el elemento actual y
comenzar a trabajar con el siguiente. Un ejemplo puede ayudar a aclarar su
funcionalidad.

~~~bash
Jess> (bind ?lista (create$ uno dos tres cuatro cinco seis)) ; Se crea una lista...
(uno dos tres cuatro cinco seis) 
Jess> (foreach ?item ?lista ; Para cada elemento en la lista...
	(printout t "Estamos en " ?item "." crlf) ; se lo imprime.
	(if (= ?item cuatro) then ; Pero si se ha llegado al cuarto...
		(printout t "¡Guarda! Hemos llegado al cuarto. De aquí no pasamos." crlf) 
		(break))) ; se detiene el ciclo con break.
Estamos en uno. 
Estamos en dos. 
Estamos en tres. 
Estamos en cuatro. 
¡Guarda! Hemos llegado al cuarto. De aquí no pasamos. 
Jess> ; Y ahora un ejemplo con for y continue... 
(for (bind ?i 1) (<= ?i ?n) (++ ?i) 
	(if (= ?i 3) then
		(printout t "Mmmmm noup... No podemos imprimir el tres." crlf) 
		; continue le indica que termine con el elemento actual y
		; siga con el siguiente.
		(continue)) 
	(printout t ?i / ?n crlf)) 
1/5 
2/5 
Mmmmm noup... No podemos imprimir el tres. 
4/5 
5/5 
FALSE 
~~~

## Nuevas Funciones
La función `deffunction` permite crear funciones nuevas que pueden luego ser
usadas como cualquier otra de Jess. El nombre de la nueva función debe ser un
símbolo, y cada parámetro especificado una variable (debe comenzar con el signo
de interrogación `?`). Es posible definir un comentario como una cadena de
caracteres, el cual documentará el propósito de la función. Dentro del cuerpo de
la función se pueden definir cualquier número de expresiones, y se utiliza
`return` para devolver el resultado.

~~~bash
Jess> (deffunction saludar (?nombre) 
	"Saluda a la persona." 
	(printout t "¡¡Hola " ?nombre "!!" crlf)) 
TRUE 
Jess> (saludar "Yoda") 
¡¡Hola Yoda!! 
~~~

## Los Hechos
Los hechos son los datos que las reglas de inferencia utilizan para el
procesamiento y realización de recomendaciones. El conjunto o colección de
hechos se conoce como memoria de trabajo, y Jess cuenta con ciertas funciones
que permiten agregar, quitar y recuperarlos. Internamente, estos hechos son
almacenados como listas, en donde la cabecera es el nombre del hecho.

~~~bash
(humedad 0.89)
(verde 0 255 0)
(nikola tesla)
~~~

Utilizando la función `facts` se puede recuperar una lista con todos los hechos
presentes en la memoria de trabajo. En un principio, esta no posee hecho alguno,
sin embargo al llamar a la función `reset` se crear un hecho inicial por defecto
(hecho `0`). Este hecho es muy importante para poder trabajar con reglas que
poseen ciertos antecedentes, por lo que se recomienda siempre invocar a esta
función antes de cualquier ejecución.

~~~bash
Jess> (facts) ; Se lista los hechos. Inicialmente, no hay hechos.
For a total of 0 facts in module MAIN. 
Jess> (reset) ; Pero después de invocar a reset...
TRUE 
Jess> (facts) ; aparece el hecho 0 en la memoria de trabajo.
f-0   (MAIN::initial-fact) 
For a total of 1 facts in module MAIN. 
~~~

Un punto que resulta conveniente notar es la presencia del prefijo `MAIN::`
antepuesto al nombre del hecho en el listado. Este prefijo indica el módulo en
donde se encuentra definido este hecho. `MAIN` es el módulo por defecto, y Jess
da a posibilidad de definir otros para tener un mejor control de la ejecución
del sistema. En los capítulos posteriores se ampliará un poco más este concepto,
aunque no en demasiada profundidad debido a que no es una característica
utilizada pare el desarrollo del Jesslet.

Existe otra función que permite realizar un seguimiento de los hechos que van
apareciendo y desapareciendo de la memoria de trabajo. La función `watch`
devuelve mensajes para el usuario reportando diferentes eventos dependiendo de
los parámetros que recibe. Si es invocada `(watch facts)`, entonces se
presentarán mensajes cada vez que un hecho se agregado o eliminado de la memoria
de trabajo. Modificando un poco el ejemplo anterior, podríamos ver lo que ocurre
con un `reset`.

~~~bash
Jess> (watch facts) ; Ahora Jess informará cada vez que suceda algo con los hechos 
TRUE 
Jess> (reset) 
 ==> f-0 (MAIN::initial-fact) 
TRUE 
~~~

### Agregando Hechos
La creación de nuevos hechos se consigue a través de `assert`. Esta función
recibe como parámetros cualquier cantidad de hechos, y devuelve el ID del último
hecho agregado. En el caso del hecho inicial, este ID es `0` (cero). Los
identificadores pueden ser luego utilizados para modificar o eliminar los hechos
correspondientes.

~~~bash
Jess> (watch facts) 
TRUE 
Jess> (reset) 
 ==> f-0 (MAIN::initial-fact) 
TRUE 
Jess> (assert (codigo 3301)) 
 ==> f-1 (MAIN::codigo 3301) 
<Fact-1> 
Jess> (assert (lenguajes Java Clojure Jess PHP Javascript Python Perl)) 
 ==> f-2 (MAIN::lenguajes Java Clojure Jess PHP Javascript Python Perl) 
<Fact-2> 
Jess> (facts) 
f-0   (MAIN::initial-fact) 
f-1   (MAIN::codigo 3301) 
f-2   (MAIN::lenguajes Java Clojure Jess PHP Javascript Python Perl) 
For a total of 3 facts in module MAIN. 
~~~

Si ha ocurrido un error en la inserción, entonces la función devuelve `FALSE`.
Generalmente, este error indica que el hecho es un duplicado, ya que Jess sólo
puede almacenar hechos únicos en la memoria de trabajo.

### Eliminando Hechos
Para eliminar hechos individuales se utiliza la función `retract`. Esta función
recibe como parámetros los identificadores de los hechos que se quieren remover,
o bien los hechos en sí. Siguiendo con los hechos que se tenían desde el ejemplo
anterior.

~~~bash
Jess> (facts) 
f-0   (MAIN::initial-fact) 
f-1   (MAIN::codigo 3301) 
f-2   (MAIN::lenguajes Java Clojure Jess PHP Javascript Python Perl) 
For a total of 3 facts in module MAIN. 
Jess> (retract 1) ; Se quita el hecho con ID 1. 
 <== f-1 (MAIN::codigo 3301) 
TRUE 
Jess> (bind ?h (fact-id 2)) ; Se usa la función fact-id para recuperar el hecho.
<Fact-2> 
Jess> ?h 
<Fact-2> 
Jess> (retract ?h) ; Se quita el hecho en la variable. 
 <== f-2 (MAIN::lenguajes Java Clojure Jess PHP Javascript Python Perl) 
TRUE 
Jess> (facts) 
f-0   (MAIN::initial-fact) 
For a total of 1 facts in module MAIN. 
~~~

El resultado de utilizar `retract` con un identificador o con una instancia de
un hecho es exactamente el mismo, pero este último resulta más eficiente y
rápido que utilizando el identificador del hecho.

Cuando se quiere reiniciar la memoria de trabajo, Jess las funciones `clear` y
`reset` que pueden ser de utilidad. Ambas eliminan los hechos de la memoria de
trabajo, pero `clear` va un poco más allá e incluye entre sus objetivos a las
reglas de inferencia, variables y funciones definidas por el usuario. Para
reiniciar el estado de un sistema sin borrarlo, es necesario utilizar `reset`.
Esta función elimina todos los hechos y deja solamente el hecho `0` en memoria.

~~~bash
Jess> (assert (lenguajes Java Jess C++ C# Python Clojure)) 
<Fact-1> 
Jess> (assert (foo bar baz)) 
<Fact-2> 
Jess> (facts) ; Se recuperan los hechos en la memoria de trabajo.
f-0   (MAIN::initial-fact) 
f-1   (MAIN::lenguajes Java Jess C++ C# Python Clojure) 
f-2   (MAIN::foo bar baz) 
For a total of 3 facts in module MAIN. 
Jess> (reset) ; Y se aplica un reset.
TRUE 
Jess> (facts) ; Ahora sólo aparece el hecho inicial.
f-0   (MAIN::initial-fact) 
For a total of 1 facts in module MAIN. 
~~~

### Hechos Por Defecto
Utilizando la función `deffacts` es posible definir un conjunto de hechos
iniciales que aparecerán cada vez que se invoque a la función `reset`. Con esto
se consigue llevar al sistema a un estado inicial conocido sin la necesidad de
insertar uno a uno los hechos mediante un `assert` cada vez que se restablezca
la memoria de trabajo.

La función `deffacts` recibe como parámetros el nombre de la lista, un
comentario opcional a modo de documentación, y el listado de hechos iniciales.

~~~bash
Jess> (deffacts memes "Cantidad de memes" 
	(yao-ming 191) 
	(troll-face 8098) 
	(poker-face 789) 
	(forever-alone 437) 
	(cereal-guy 90)) 
TRUE 
Jess> (reset) 
TRUE 
Jess> (facts) 
f-0   (MAIN::initial-fact) 
f-1   (MAIN::yao-ming 191) 
f-2   (MAIN::troll-face 8098) 
f-3   (MAIN::poker-face 789) 
f-4   (MAIN::forever-alone 437) 
f-5   (MAIN::cereal-guy 90) 
For a total of 6 facts in module MAIN. 
~~~

### Hechos No Ordenados
Los hechos no ordenados son hechos que poseen campos análogos a una tabla en una
base de datos. Sin embargo antes de poder crear un hecho de este tipo,
primeramente hay que definir su estructura. Para ello se utiliza la función
`deftemplate`. Esta recibe como parámetros el nombre de la estructura, junto con
los campos de la misma establecidos mediante `slot`.

~~~bash
Jess> (deftemplate libro "Un libro" 
	(slot titulo) 
	(slot autor) 
	(slot isbn)) 
TRUE 
~~~

Cuando se crean hechos no ordenados, los valores de los campos pueden ser
especificados en cualquier orden.

~~~bash
Jess> (assert (libro (autor "Julio Cortázar") (isbn 9789875780620)
	(titulo Rayuela))) 
<Fact-0> 
Jess> (facts) 
f-0   (MAIN::libro (titulo Rayuela) (autor "Julio Cortázar") (isbn 9789875780620)) 
For a total of 1 facts in module MAIN. 
~~~

Si el valor de un campo no es especificado, entonces se la asigna el valor por
defecto. Los valores por defecto para cada campo pueden ser especificados al
definir cada campo.

~~~bash
Jess> (clear) 
TRUE 
Jess> (deftemplate libro "" 
	(slot titulo) 
	(slot autor (default Anónimo)) 
	(slot isbn)) 
TRUE 
Jess> (assert (libro (titulo "Sangre Estival") 
	(isbn 9789876831888))) 
<Fact-0> 
Jess> (facts) 
f-0   (MAIN::libro (titulo "Sangre Estival") (autor Anónimo) (isbn 9789876831888)) 
For a total of 1 facts in module MAIN. 
~~~

Los valores contenidos en los hechos no ordenados no son constantes y pueden ser
alterados. Para este fin se hace uso de la función `modify`.

~~~bash
Jess> (bind ?h (fact-id 0)) 
<Fact-0> 
Jess> (modify ?h (autor "Virginio Tamaro")) 
<Fact-0> 
Jess> (facts) 
f-0   (MAIN::libro (titulo "Sangre Estival") (autor "Virginio Tamaro") (isbn 9789876831888)) 
For a total of 1 facts in module MAIN. 
~~~

Esta función recibe como primer parámetro el hecho a modificarse, o su ID. Lo
que sigue es un conjunto de pares ordenados de la forma `(<campo>,
<nuevo valor>)`.

### Hechos Ordenados
Los hechos no ordenados resultan de gran utilidad cuando se requiere organizar
los datos dentro en estructuras, pero cuando lo que se necesita es almacenar un
único dato, entonces la definición de una estructura para este caso resulta
innecesaria y excesiva. Como ya ha sido presentado en alguno de los ejemplos
anteriores, Jess posibilita la creación de hechos de la siguiente manera:

~~~bash
Jess> (assert (river 35)) 
<Fact-0> 
Jess> (facts) 
f-0   (MAIN::river 35) 
For a total of 1 facts in module MAIN. 
Es posible incluso definir una lista de elementos como valor de un hecho.
Jess> (assert (proporciones 0.5 0.3 0.2))
<Fact-1> 
Jess> (facts) 
f-0   (MAIN::river 35) 
f-1   (MAIN::proporciones 0.5 0.3 0.2) 
For a total of 2 facts in module MAIN. 
~~~

Todas estas acciones se realizan sin la necesidad de definir una estructura
mediante `deftemplate`, siempre que no haya una estructura con el mismo nombre
previamente especificada. Esto se debe a que cuando un hecho ordenado es creado
por primera vez, Jess crea automáticamente su estructura con el nombre del
hecho. Con la función `showdeftemplates` se pueden recuperar todas las
estructuras de hechos definidas, y con `ppdeftemplate` se puede obtener un
detalle de una estructura determinada.

~~~bash
Jess> (show-deftemplates) 
 
(deftemplate MAIN::__clear 
   "(Implied)") 

(deftemplate MAIN::__fact 
   "Parent template") 

(deftemplate MAIN::__not_or_test_CE 
   "(Implied)") 

(deftemplate MAIN::initial-fact 
   "(Implied)") 

(deftemplate MAIN::proporciones 
   "(Implied)" 
   (multislot __data)) 

(deftemplate MAIN::river 
   "(Implied)" 
   (multislot __data)) 
FALSE 
Jess> (ppdeftemplate river) 
"(deftemplate MAIN::river 
   \"(Implied)\" 
   (multislot __data))" 
~~~

La estructura generada para el hecho posee un único campo multiple `__data`.
Este campo se encuentra normalmente oculto cuando los hecho son presentados. En
sí, los hechos ordenados son un subconjuto de los hecho no ordenados con un
único campo llamado `__data`.

Se recomienda utilizar hechos ordenados cuando se requiera tener información
volátil y con poca trascendencia a lo largo del programa. Los hechos no
ordenados son preferibles debido a que poseen mayor flexibilidad, seguridad con
respecto a los datos que mantienen, tienden a producir menos errores de
programación, y tienen un mejor desempeño en términos computacionales.

## Las Reglas
Las reglas componen lo que se conoce como base de conocimientos, y estas pueden
llevar a cabo acciones basándose en los hechos que se encuentran en la memoria
de trabajo. Existen dos tipos de reglas de inferencia: las de **encadenamiento
hacia adelante** y las de **encadenamiento hacia atrás**.

En el encadenamiento hacia adelante tiene una forma análoga a la estructura `if
... then` presente en varios lenguajes de programación, en donde la parte `then`
(**consecuente**) es ejecutada siempre que se cumplan las condiciones en el `if`
(**antecedente**). Las reglas de encadenamiento hacia atrás también poseen esta
estructura, con la diferencia de que en estas se trata de satisfacer de manera
activa las premisas en la parte de `if`. Para este proyecto sólo serán
utilizadas reglas de encadenamiento hacia adelante, por lo que solamente estas
serán analizadas en profundidad.

### Creación de una Regla
Toda regla en Jess es definida utilizando la función `defrule`. En su versión
más simple, es posible escribir una regla sin condiciones y que no realice
acciones algunas.

~~~bash
Jess> (defrule regla-vacia 
	"Esta regla no realiza acción alguna" 
	=> 
	) 
TRUE 
Jess> (rules) 
MAIN::regla-vacia 
For a total of 1 rules in module MAIN. 
~~~

El primer parámetro que recibe `defrule` es el nombre de la regla (`regla-vacia`
en el ejemplo), y este debe ser un símbolo. El segundo es una cadena opcional
de documentación que describe el propósito de la regla. El símbolo `=>` separa
las condiciones (`if`) de las acciones (`then`). La condiciones son un conjunto
de patrones que serán comparados con los hechos en la base de hechos, mientras
que las acciones son llamadas a funciones que pueden modificar estos hechos, las
reglas de inferencia, o enviar mensajes para el usuario.

Paralelamente a lo que sucede con los hechos, la función `watch` también puede
ser utilizada para mostrar mensaje cada vez que ocurra algo de interés con las
reglas. Utilizando `(watch activations)` se le indica a Jess que muestre un
mensaje un registro de activación sea agregado o eliminado. Un registro de
activación asocia un conjunto de hechos con una regla, e implica que dicho
conjunto cumple las condiciones de la regla por lo que esta debe ser disparada.

Siguiendo el ejemplo anterior:

~~~bash
Jess> (watch rules) 
TRUE 
Jess> (watch activations) 
TRUE 
Jess> (watch facts) 
TRUE 
Jess> (reset) 
 ==> f-0 (MAIN::initial-fact) 
==> Activation: MAIN::regla-vacia :  f-0 
TRUE 
Jess> (run) 
FIRE 1 MAIN::regla-vacia f-0 
1 
~~~

Como se puede ver en el ejemplo, la regla se activa luego de que aparece en la
memoria de trabajo el hecho inicial creado por `reset`. Este ejemplo pone de
manifiesto una de las funcionalidades que posee este hecho inicial: **activar
reglas que poseen un conjunto vacío de condiciones**.

La función `rules` (en el primer ejemplo) presenta un listado de las reglas que
se han definido, y se puede utilizar `ppdefrule` para presentar el detalle
interno de una de estas. 

Mediante `(watch rules)` se le indica a Jess que presente un mensaje cuando una
regla sea disparada. Se dice que una regla es disparada cuando se ejecutan las
acciones en la parte del then. La función `run` hace que las reglas activas
comiencen a dispararse, y continúa una a una hasta que no existan reglas
activas. Ninguna regla puede ser disparada antes de invocar a esta función.

La siguiente regla es un ejemplo más complejo en donde aparecen un antecedente
junto con ciertas acciones en el consecuente.

~~~bash
Jess> (defrule cargar-bateria-baja 
	"Si la batería del celular está baja, recargarla." 
	?bat <- (bateria-baja) 
	=> 
	(printout t "Cargando batería..." crlf) 
	(retract ?bat) ) 
TRUE 
~~~

En este punto es conveniente remarcar nuevamente que **todo lo que aparece en el
antecedente no son funciones**. Se trata de patrones que serán aplicados sobre
los hechos. Debido a la forma en la que trabajan los lenguajes de programación y
su analogía con la estructura `if ... then`, se tiende a pensar en expresiones y
condiciones booleanas. Jess no intenta evaluar a `TRUE` o `FALSE` el
antecedente, sino que trabaja con los patrones para obtener el conjunto de
hechos que satisfaga las condiciones y de lugar a la creación de un registro de
activación.

En el siguiente ejemplo se utiliza `(watch all)` para conseguir que Jess
presente un mensaje para cada evento de interés en las reglas, hechos y
activaciones.

~~~bash
Jess> (watch all) 
TRUE 
Jess> (defrule cargar-bateria-baja 
	"Si la batería del celular está baja, recargarla." 
	?bat <- (bateria-baja) 
	=> 
	(printout t "Cargando batería..." crlf) 
	(retract ?bat) 
	) 
MAIN::cargar-bateria-baja: +1+1+1+t 
TRUE 
Jess> (reset) 
 ==> Focus MAIN 
 ==> f-0 (MAIN::initial-fact) 
TRUE 
Jess> (assert (bateria-baja)) 
 ==> f-1 (MAIN::bateria-baja) 
==> Activation: MAIN::cargar-bateria-baja :  f-1 
<Fact-1> 
Jess> (run) 
FIRE 1 MAIN::cargar-bateria-baja f-1 
Cargando batería... 
 <== f-1 (MAIN::bateria-baja) 
 <== Focus MAIN 
1 
~~~

La regla es activada cuando se crea el hecho `(bateria-baja)` y se dispara una
vez que se invoca a la función `run`. Lo que ocurre a continuación está dado por
las acciones en el consecuente. En primer lugar se presenta el mensaje para el
usuario, y a continuación se elimina el hecho de la memoria de trabajo. Es de
esta manera como las reglas pueden modificar la memoria de trabajo: agregando o
quitando hechos en sus acciones.

El número `1` que aparece al final de la ejecución es el número total de reglas
que han sido disparadas.

### Restricciones y Filtros de los Hechos
La mayor parte de los hechos que aparecen en los sistemas expertos no se tratan
de hechos vacíos (como `(barteria-cargada)`), sino que poseen datos adicionales.
Y los patrones en las condiciones de las reglas pueden utilizar estos datos como
un filtro para especificar un subconjunto de hechos. Estos filtros se conocen
como restricciones, puesto que restringen los valores que puede contener un
hecho en un patrón, y pueden pertenecer a alguno de los siguientes tipos:

1. Literales
2. De variable
3. Conectivas
4. De función predicado
5. De retorno de función

Debido a que para el desarrollo de este trabajo sólo se dará soporte a los
hechos ordenados, los desarrollos conceptuales y ejemplificaciones estarán
orientados exclusivamente hacia este tipo de hechos.

#### Restricciones Literales
Pueden establecerse valores literales como restricciones dentro de los patrones.
De esta manera, sólo se verificarán con estos patrones los hechos que contengan
esos valores en las posiciones correspondientes. En el ejemplo de abajo se
especifica un patrón que se cumple cuando existe un hecho `(lenguaje Python)` en
la memoria de trabajo.

~~~bash
Jess> (watch all) 
TRUE 
Jess> (defrule regla-literal 
	(lenguaje Python) 
	=> 
	(printout t "Se cumple el valor literal." crlf))
MAIN::regla-literal: +1+1+1+1+t 
TRUE 
Jess> (reset) 
 ==> Focus MAIN 
 ==> f-0 (MAIN::initial-fact) 
TRUE 
Jess> (assert (lenguaje Java)) 
 ==> f-1 (MAIN::lenguaje Java) 
<Fact-1> 
Jess> (assert (lenguaje "Python")) 
 ==> f-2 (MAIN::lenguaje "Python") 
<Fact-2> 
Jess> (assert (lenguaje Python)) 
 ==> f-3 (MAIN::lenguaje Python) 
==> Activation: MAIN::regla-literal :  f-3 
<Fact-3> 
Jess> (facts) 
f-0   (MAIN::initial-fact) 
f-1   (MAIN::lenguaje Java) 
f-2   (MAIN::lenguaje "Python") 
f-3   (MAIN::lenguaje Python) 
For a total of 4 facts in module MAIN.
~~~

A partir del ejemplo de arriba se debe hacer hincapié en un punto. Las
restricciones literales obligan una correspondencia exacta del valor dado; no se
realizan conversiones de datos. Es por eso que al crearse el hecho `(lenguaje
"Python")` la regla no es activada, pero sí cuando aparece en la memoria de
trabajo `(lenguaje Python)`. `"Python"` es una cadena de caracteres (`STRING`),
mientras que `Python` es un símbolo (`SYMBOL`).

~~~bash
Jess> (jess-type "Python") 
STRING 
Jess> (jess-type Python) 
SYMBOL 
~~~

#### Restricciones de Variables
Es posible definir una variable en lugar de un valor literal para los valores de
los hechos en los patrones. Una variable es asignada con el valor que posea el
hecho en la posición en la que esta aparezca.

~~~bash
Jess> (defrule regla-variables 
	"Esta regla tiene variables como restricciones." 
	(hecho ?foo ?bar) 
	=> 
	(printout t "Las variables son " ?foo " y " ?bar "." crlf)) 
~~~

Esta regla se activa cada vez que aparece un hecho con valores asignados en sus
dos primeras posiciones, por ejemplo `(hecho 1 2)`, `(hecho uno dos)`. Estos
valores se asignan a las variables `?foo` y `?bar` en sus respectivas
posiciones, y estas se encuentran disponibles para ser utilizadas en las
acciones presentes en el consecuente. Siguiendo el ejemplo anterior:

~~~bash
Jess> (assert (hecho uno dos)) 
<Fact-0> 
Jess> (assert (hecho tres cuatro cinco)) 
<Fact-1> 
Jess> (run) 
Las variables son uno y dos. 
1 
~~~

Como puede verse, sólo activan a la regla los hechos que contienen sólo dos
valores. Si poseen un número distinto, cualquiera sea, no se verificarán con el
patrón. El valor uno se asigna a `?foo`, y dos a `?bar`.

Una variable puede aparecer más de un patrón en la misma regla, con la
restricción de que para cada aparición debe tener asignado exactamente el mismo
valor.

~~~bash
Jess> (defrule regla-variables 
	"Otro ejemplo de variables en las restricciones." 
	(hecho ?foo ?foo) 
	=> 
	(printout t "Tenemos un hecho con el valor " ?foo 
		" en sus dos primeras posiciones." crlf)) 
TRUE 
Jess> (reset) 
TRUE 
Jess> (assert (hecho c3p0 c3p0)) 
<Fact-1> 
Jess> (assert (hecho uno dos)) 
<Fact-2> 
Jess> (assert (hecho "" "")) 
<Fact-3> 
Jess> (run) 
Tenemos un hecho con el valor  en sus dos primeras posiciones. 
Tenemos un hecho con el valor c3p0 en sus dos primeras posiciones. 
2 
~~~

Es posible restringir un valor en un hecho sin establecer una variable a la cual
vincularla utilizando un signo de interrogación (`?`) en lugar de un
identificador. En general esto es útil sólo cuando se requiere especificar que
cierto hecho contiene algunos valores, sin importar cuáles sean estos.

#### Restricciones Conectivas
El sólo hecho de limitar los valores de los hechos a literales únicos no es
suficiente para la representación del conocimiento experto en las reglas de
inferencia. Las restricciones conectivas pueden percibirse como una mejora sobre
los patrones de las literales que se presentaron anteriormente, en donde pueden
utilizarse los conectores `&` (conjunción), `|` (disyunción), y `~` (negación).

Una restricción que sea precedida por el tilde `~` concordará con opuesto a lo
que esta restricción originalmente lo hubiese hecho.

~~~bash
Jess> (defrule regla-conectiva 
	"Esta regla sólo se activa si hay algún hecho que no 
	contenga el valor Python en su primera posición." 
	(lenguaje ~Python) 
	=> 
	(printout t "Tenemos uno que no es Python." crlf)) 
TRUE 
Jess> (assert (lenguaje Java)) 
<Fact-4> 
Jess> (reset) 
TRUE 
Jess> (assert (lenguaje Java)) 
<Fact-1> 
Jess> (assert (lenguaje Python)) 
<Fact-2> 
Jess> (assert (lenguaje "Python")) 
<Fact-3> 
Jess> (run) 
Tenemos uno que no es Python. 
Tenemos uno que no es Python. 
2 
~~~

Es posible utilizar la conjunción y la disyunción para armar restricciones
simples.

~~~bash
Jess> (defrule regla-conectiva 
	(protocolo http|ftp) 
	=> 
	(printout t "Trabajando sobre HTTP o FTP." crlf)) 
TRUE 
Jess> (reset) 
TRUE 
Jess> (assert (protocolo ftp)) 
<Fact-1> 
Jess> (run) 
Trabajando sobre HTTP o FTP. 
1 
~~~

También es posible vincular una variable mediante el conector de la conjunción.

~~~bash
Jess> (defrule regla-conectiva 
	(provincia ?p&~Corrientes) 
	(ciudad "Santos Lugares") 
	=> 
	(printout t "Tenemos una ciudad llamada Santos Lugares " 
	"en una provincia " ?p crlf)) 
TRUE 
Jess> (reset) 
TRUE 
Jess> (assert (ciudad "Santos Lugares")) 
<Fact-1> 
Jess> (assert (provincia Formosa)) 
<Fact-2> 
Jess> (run) 
Tenemos una ciudad llamada Santos Lugares en una provincia Formosa 
1 
~~~

Se debe notar que el orden de precedencia para estos patrones es la negación,
seguido de la conjunción y la disyunción, y no es posible la utilización de
paréntesis para modificar esto.

#### Restricciones de Función Predicado
A pesar de la aparente flexibilidad que brindan las restricciones literales y
conectivas, su alcance puede verse limitado al intentar obtener patrones más
complejos en donde deban realizarse llamadas a funciones o cálculos sobre los
valores. Por ejemplo, supóngase que se tienen en la memoria de trabajo hechos
que contienen los datos personales de una persona. Entre estos datos se
encuentra el de su fecha de nacimiento. Si en alguna regla se requiere que el
antecedente trabaje con la edad de la persona, este cálculo sería imposible de
realizar utilizando solamente las restricciones hasta aquí vistas. Para estos
casos, Jess dispone de las restricciones de función predicado, mediante las
cuales puede expresarse casi cualquier restricción que pueda ser necesaria.

Una función predicado es una función que devuelve un valor booleano (`TRUE` o
`FALSE`), y  puede ser establecida como restricción en los patrones mediante los
dos puntos (`:`). Si se quiere utilizar el valor del campo en cuestión, debe ser
asignado con una variable.

~~~bash
Jess> (defrule regla 
	"Esta regla sólo será activada si el nombre de 
	la persona tiene más de cuatro caracteres." 
	(nombre ?n&:(> (str-length ?n) 4)) 
	=> 
	(printout t ?n " tiene más de 4 caracteres." crlf)) 
TRUE 
Jess> (reset) 
TRUE 
Jess> (assert (nombre "Dominic")) 
<Fact-1> 
Jess> (assert (nombre "Harold")) 
<Fact-2> 
Jess> (assert (nombre "Elias")) 
<Fact-3> 
Jess> (assert (nombre "Root")) 
<Fact-4> 
Jess> (run) 
Dominic tiene más de 4 caracteres. 
Harold tiene más de 4 caracteres.
Elias tiene más de 4 caracteres.
2 
~~~

La potencia de las funciones de predicado radica en el hecho de que pueden ser
agrupadas en estructuras más complejas mediante conjunciones (`and`) y
disyunciones (`or`).

~~~bash
Jess> (defrule mayores-solamente 
	"Esta regla sólo permite el paso de personas 
	entre los 35 y los 50 años." 
	(edad ?e&:(and 
		(>= ?e 35) 
		(<= ?e 50))) 
	=> 
	(printout t ?e " años." crlf)) 
TRUE 
Jess> (assert (edad 25)) 
<Fact-0> 
Jess> (assert (edad 49)) 
<Fact-1> 
Jess> (run) 
49 años. 
1 
~~~

#### Restricciones de Retorno de Función
En ciertas situaciones es necesario que el valor en un hecho concuerde con el
valor devuelto por una función. Supóngase que, en una condición de una regla,
se necesitan dos edades de tal manera que una sea exactamente tres veces la
otra. De repente, con lo visto hasta aquí, se podría plantear algo como lo
siguiente:

~~~bash
Jess> (defrule triple-edad 
	"Esta regla se dispara si hay dos edades tal que 
	una sea tres veces la otra." 
	(edad ?e1) 
	(edad ?e2&:(= 
		?e2 (* ?e1 3))) 
	=> 
	(printout t ?e2 " = " ?e1 "x3." crlf)) 
TRUE 
~~~

Si se agregan los hechos necesarios y se ejecuta, se tiene lo siguiente.

~~~bash
Jess> (assert (edad 21)) 
<Fact-0> 
Jess> (assert (edad 7)) 
<Fact-1> 
Jess> (run) 
21 = 7x3. 
1 
~~~

Cuando se quiere restringir un valor a lo retornado por una función, esta es
precedida por un signo igual (`=`). El condicional de la regla puede ser
reescrito de la siguiente manera.

~~~bash
Jess> (defrule triple-edad 
	"Esta regla se dispara si hay dos edades tal que 
	una sea tres veces la otra." 
	(edad ?e1) 
	(edad =(* ?e1 3))
		=>
	(printout t (* ?e1 3) " = " ?e1 "x3." crlf)) 
TRUE 
~~~

Lo que consigue de esta manera es simplificar los patrones al no escribir la
función `eq`. El ejemplo también tiene la particularidad de que la variable `?y`
no es utilizada en su restricción, por lo que la aplicación de esta restricción
resulta idónea. En sí, cuando Jess es ejecutado, esta nueva forma se convierte
hacia la función `eq`, por lo que no hay diferencia en cuanto al rendimiento de
una u otra forma.

### Asignación de Patrones
Para poder ser utilizados en las acciones del consecuente, los hechos que
concuerdan en el antecedente deben ser asignados a variables. Para esto se
utiliza el operador `<-` como aparece en el siguiente fragmento.

~~~bash
Jess> (defrule regla 
	?hecho <- (password 123456) 
	=> 
	(printout t "Hay una contraseña demasiado sencilla." crlf) 
	(retract ?hecho)) 
TRUE 
Jess> (reset) 
TRUE 
Jess> (watch facts) 
TRUE 
Jess> (assert (password admin)) 
 ==> f-1 (MAIN::password admin) 
<Fact-1> 
Jess> (assert (password 123456)) 
 ==> f-2 (MAIN::password 123456) 
<Fact-2> 
Jess> (run) 
Hay una contraseña demasiado sencilla. 
 <== f-2 (MAIN::password 123456) 
1
~~~

Así, el hecho `(password 123456)` queda asignado en la variable `?hecho`, para
luego ser quitado de la memoria mediante la función `retract`.

### Los Elementos Condicionales
Así como es posible asignar variables y aplicar funciones en los patrones para
la restricción de los valores en los hechos, Jess también cuenta con ciertas
funcionalidades que permiten expresar relaciones más complejas entre los hechos.
Se tratan de los elementos condicionales, y permiten modificar los patrones al
agruparlos en estructuras lógicas.

Muchos de estos elementos condicionales poseen exactamente el mismo nombre que
funciones usadas como funciones predicado, lo que puede prestarse a confusión.
Por ello hay que remarcar que no se tratan de las mismas funciones. Por ejemplo,
la función and que aparece en un predicado es una función **booleana** que
devuelve `TRUE` o `FALSE`, mientras que el elemento condicional `and` se
verifica si se cumple un conjunto de patrones de hechos.

Los elementos condicionales incluidos en Jess son:

* `and` – verifica varios patrones al mismo tiempo
* `or` – verifica patrones alternativos
* `not` – se verifica si no se cumple patrón alguno dentro de un conjunto
* `exists` – se verifica si al menos un patrón dentro de un conjunto se verifica
* `test` – se cumple si una función no devuelve FALSE
* `logical` - permite definir hechos como consecuencias de otros

#### El Elemento Condicional `and`
Una regla de inferencia contiene en su condición un conjunto de cero o más
patrones. Cada patrón define un conjunto de hechos, y todos estos patrones deben
cumplirse para que la regla se active. El elemento condicional `and` funciona de
esta manera, y puede verse como la intersección de los conjuntos generados por
estos patrones. De hecho, toda condición de una regla es envuelta dentro de un
`and` tácito.

~~~bash
Jess> (defrule regla-and 
	(and (edad ?e&:(>= ?e 18))
		(acreditado)) 
	=> 
	(printout t "Está acreditato y es mayor de edad." crlf)) 
TRUE 
~~~

Hay que notar que la regla del ejemplo de arriba funcionaría exactamente de la
misma manera que si se hubiese omitido el `and`. El elemento condicional, en
este caso, sólo fue agregado a los fines de presentación. La potencia de estos
radica en las posibilidades de combinación con otros, como el `or` o el `not`.

#### El Elemento Condicional `or`
Este elemento se cumple si uno o más de los patrones que incluye se verifica. Si
se cumple más de uno de los patrones, el elemento `or` se verificará más de una
vez también.

~~~bash
(defrule regla-or 
	?f <- (or (role administrator) 
		(username root)) 
	=> 
	(printout t "El usuario está autorizado." crlf)) 
~~~

Si con esta regla aparecen los hechos `(username root)` y
`(role administrator)`, entonces esta se activaría y dispararía dos veces, y la
variable `?f` quedaría asignada con cualquiera de los dos hechos que provoquen
la activación.

~~~bash
Jess> (reset) 
TRUE 
Jess> (assert (username root)) 
<Fact-1> 
Jess> (assert (role administrator)) 
<Fact-2> 
Jess> (run) 
El usuario está autorizado. 
El usuario está autorizado. 
2 
~~~

Como puede verse, con la utilización del `or` en la regla del ejemplo se obtiene
un funcionamiento análogo al de tener dos reglas. Internamente, Jess convierte
este elemento condicional en `n` reglas de inferencia, una por cada patrón que
posee.

#### El Elemento Condicional `not`
Este elemento condicional se utiliza para controlar la no existencia de un
conjunto de uno o más patrones dentro de la memoria de trabajo.

~~~bash
Jess> (defrule sin-alfiles-negros 
	(not (alfil negro)) 
	=> 
	(printout t "No hay alfiles negros en el tablero." crlf)) 
TRUE 
~~~

Debido al funcionamiento de este elemento condicional, no se pueden definir
variables para ser asignadas con el hecho. Es posible, sin embargo, utilizar
variables dentro de un patrón `not`, siempre que sólo sean utilizadas dentro del
mismo patrón.

El `not` es evaluado ante tres situaciones:

1. Cuando se crea un hecho que concuerda con uno de los patrones que incluye,
fallando en este caso.
2. Cuando se elimina un hecho que concuerda con uno de los patrones incluidos,
verificándose esta vez.
3. Cuando un patrón inmediato anterior dentro del condicional de la regla es
evaluado.

Si el elemento condicional `not` está primero dentro de un grupo de patrones,
entonces el patrón `(initial-fact)` es antepuesto para cumplir la función de
este patrón anterior. En este punto puede notarse la importancia de la llamada a
la función `reset` previo a la ejecución, debido a que esta crea el hecho
inicial, garantizando el correcto funcionamiento de los patrones con `not`.

Como un ejemplo más complejo, se podría tener la siguiente regla:

~~~bash
Jess> (defrule enroque 
	?t <- (pieza torre blanco h 1) 
	?r <- (pieza rey blanco e 1) 
	(not (pieza ? ? f|g 1)) 
	=> 
	(printout t "Realizamos el enroque." crlf) 
	(retract ?t) 
	(retract ?r) 
	(assert (pieza torre blanco f 1)) 
	(assert (pieza rey blanco g 1))) 
TRUE
~~~

Aquí se establecen las condiciones que tienen que darse en un tablero de ajedrez
para que sea posible realizar un enroque. Por supuesto, falta saber si tanto el
rey como la torre involucrados han sido movidos o no, pero a los fines de la
simplicidad, se obviará esa condición. El elemento condicional `not`, en este
caso, se verificará cuando no exista ninguna pieza del tipo color que sea, en
las posiciones `F1` y `G1`.

#### El Elemento Condicional `exists`
El elemento condicional `exists`, de manera análoga al `not`, puede abarcar uno
o más patrones, y se verifica si se cumplen uno o más de estos. Este elemento
condicional es útil cuando se requiere disparar una regla sólo una vez, aunque
puedan existir muchos hechos en la memoria de trabajo que podrían activarla.
Usar `(exists (hecho))` es lo mismo que `(not (not (hecho))`.

#### El Elemento Condicional `test`
Este elemento posee una particularidad que lo distingue del resto: sus
parámetros no se tratan de otros patrones, sino que es una expresión o función
booleana. El resultado determina si el patrón se cumple o falla. Un patrón
`test` falla si y sólo si la función devuelve `FALSE`.

La evaluación del `test` es igual al `not`, por lo que, nuevamente, es
importante el uso del `reset` previo a la ejecución. Una función o expresión que
aparezca en el `test` puede ser igualmente escrita utilizando restricciones de
predicados, con la diferencia que con la primera opción, ante una evaluación
booleana demasiado larga, el `test` puede mejorar la legibilidad del código
fuente.

De esta manera, la regla:

~~~bash
Jess> (defrule mayores 
	(persona ?nombre ?apellido ?edad) 
	(test (> ?edad 17)) 
	=> 
	(printout t ?nombre " " ?apellido " es mayor." crlf)) 
TRUE
~~~

Equivale a la siguiente:

~~~bash
Jess> (defrule mayores 
	(persona ?nombre ?apellido ?edad&:(> ?edad 17)) 
	=> 
	(printout t ?nombre " " ?apellido " es mayor." crlf)) 
TRUE
~~~

#### El Elemento Condicional `logical`
Existen situaciones en donde un hecho es una consecuencia directa de otro. Por
ejemplo, si se activa un interruptor de una lámpara, se espera que el foco se
encienda. Y a la inversa, si el interruptor es desactivado, la lógica indica que
el foco debe apagarse. Este tipo de relaciones se llaman dependencias lógicas,
y una forma de representarlas en Jess es la siguiente:

~~~bash
Jess> (defrule foco-encendido-interruptor 
	(interruptor 1) 
	=> 
	(assert (foco 1))) 
TRUE 
Jess> (defrule foco-apagado-interruptor 
	(not (interruptor 1)) 
	?f <- (foco 1) 
	=> 
	(retract ?f)) 
TRUE 
~~~

Sin embargo, es posible utilizar el elemento `logical` para conseguir los mismos
resultados de manera más concisa. El anterior ejemplo puede simplificarse de
esta manera:

~~~bash
Jess> (defrule foco-interruptor 
	(logical (interruptor 1)) 
	=> 
	(assert (foco 1))) 
TRUE 
~~~

De esta manera, se obtendría el siguiente funcionamiento:

~~~bash
Jess> (watch facts) 
TRUE 
Jess> (reset) 
 ==> f-0 (MAIN::initial-fact) 
TRUE 
Jess> (assert (interruptor 1)) 
 ==> f-1 (MAIN::interruptor 1) 
<Fact-1> 
Jess> (run) 
 ==> f-2 (MAIN::foco 1) 
1 
Jess> (retract 1) 
 <== f-1 (MAIN::interruptor 1) 
 <== f-2 (MAIN::foco 1) 
TRUE
~~~

### Restricciones en la Representación de las Reglas
Debido a que la creación de un servicio web que brinde todas las posibilidades
que provee Jess para la representación de las reglas de inferencia requeriría un
desarrollo complejo, para el Jesslet se realizarán ciertas restricciones en este
sentido.

En primer lugar, para las restricciones en los patrones de hechos, sólo será
posible la utilización de las de función de predicado. El motivo de esto es que
mediante estas restricciones es posible representar cualquiera de las restantes.
Un función de predicado brinda la flexibilidad suficiente para poder trabajar
con diversas expresiones y patrones, aunque en detrimento, por supuesto, de la
simplicidad.

Por otro lado, sólo podrán ser utilizados los elementos condicionales `and` ,
`not` y `exists`. Como se mencionó arriba, el `or` puede ser traducido como
varias reglas de inferencia, y los elementos `test` y `logical` pueden ser
representados sin problemas mediante restricciones de función de predicado. La
omisión de estos elementos condicionales tiene el fin de evitar volver
innecesariamente más compleja al contrato del servicio web y al Jesslet en sí.
