# Anexo B - SOAP
SOAP (*Simple Object Access Protocol*) es un protocolo basado en XML e
independiente de la plataforma, para la comunicación entre aplicaciones. La
especificación define solamente un formato para el envío y la recepción de
mensajes, junto con un conjunto de reglas para traducir tipos de datos
específicos de la aplicación y la plataforma a representaciones en texto XML y
viceversa \[[W3SCHOOLSWS], [SNELL2001]\].

En SOAP, las aplicaciones se comunican mediante mensajes XML. Esta forma de
comunicación se caracteriza por su gran flexibilidad, ya que XML es
independiente de plataforma, lenguaje de programación o sistema operativo
alguno.

## Mensajes SOAP
Un mensaje SOAP consiste en un **sobre** (`Envelope`) que contiene una cabecera
y un cuerpo. La **cabecera**, la cual es opcional, lleva información referente a
cómo debe ser procesado el mensaje, los cuales pueden incluir configuraciones de
direccionamiento y entrega del mensaje, u opciones de autenticación y
autorización. El **cuerpo** contiene el mensaje en sí, y siempre debe aparecer
en el mensaje. Cualquier información que pueda ser representada en XML debe ir
en el cuerpo.

~~~xml
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope
    xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
    soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">
    <soap:Header>
        <!-- Cabecera -->
    </soap:Header>
    <soap:Body>
        <!-- Cuerpo del mensaje -->
        <soap:Fault>
            <!-- Error en SOAP -->
        </soap:Fault>
    </soap:Body>
</soap:Envelope>
~~~

El elemento `Envelope` es el elemento raíz del mensaje, y define al documento
XML como tal. El espacio de nombres `http://www.w3.org/2001/12/soap-envelope` 
define la sintaxis para armar un mensaje SOAP. En esta URL se puede encontrar la
especificación de un esquema (XSD) para uno de estos documentos XML.

Mediante el atributo `encodingStyle` se definen los tipos de datos usados en el
documento. Hay que notar que un mensaje SOAP no posee una codificación por
defecto.

### Cabecera y Cuerpo
Si el elemento `Header` está presente, entonces debe ser el primero dentro del
mensaje. Su objetivo es comunicar información contextual relativa al
procesamiento del mensaje SOAP. En los elementos de la cabecera pueden aparecer
tres atributos: `actor`, `mustUnderstand`, y `encodingStyle`. Estos definen como
el receptor deberá procesar el mensaje.

Desde que el mensaje parte desde el emisor, hasta llegar al receptor, puede
pasar por diferentes intermediarios. Este conjunto de intermediarios se conoce
como **camino del mensaje**, y cada uno de ellos se denomina **actor**. La
construcción del mensaje no está definida en la especificación SOAP. Aunque
existen ciertas extensiones que pretenden cubrir este vacío, no existe una
estándar o utilizada de manera general. Sin embargo, SOAP sí especifica un
mecanismo que permite identificar qué partes del mensaje están dirigidas a
actores específicos. El atributo `actor` se utiliza para dirigir la cabecera de
un mensaje a un actor determinado, y su valor debe ser un identificador único
(URI). Todo intermediario que no se corresponda con este, deberá ignorar la
cabecera.

Mediante `mustUnderstand` se puede especificar si el procesamiento del
encabezamiento es obligatorio u opcional para el receptor. Si toma el valor `1`,
entonces este debe necesariamente reconocer el elemento, de otra manera todo el
proceso falla.

El atributo `encodingStyle` se usa para definir la manera en la que serán
codificadas en XML las estructuras de datos nativas de la aplicación y la
plataforma utilizadas en el documento. Puede aparecer en cualquier elemento
SOAP, y se aplicará sobre ese elemento y sus descendientes.

El **cuerpo** de un mensaje SOAP contiene el mensaje en sí para el destinatario
final. Los elementos inmediatos descendientes pueden estar implementados
mediante espacios de nombres.

### Errores
El elemento `Fault` se utiliza para informar errores dentro de los mensajes
SOAP, y posee los siguientes subelementos:

- `<faultcode>` - Código para identificar el error.
- `<faultstring>` - Una descripción legible del error.
- `<faultactor>` - Información sobre qué causó la falla.
- `<detail>` - Información del error específica de la aplicación.

En el espacio de nombre `http://www.w3.org/2001/06/soap-envelope` se definen
cuatro tipos estándar de fallas, que se detallan en el tabla que aparece a
continuación.

Código           | Causa
----------------:|:-------------------------------------------------------------
`VersionMismatch`|Hay un espacio de nombre inválido para el elemento `Envelope`.
`MustUnderstand` |Un elemento inmediato descendiente del `Header` con el atributo `mustUnderstand` en 1 no fue procesado.
`Client`         |El mensaje estaba mal formado o tenía información errónea.
`Server`         |Un problema con el servidor impidió el correcto procesamiento de mensaje.


Cabe notar que estos códigos pueden ser extendidos hacia otros tipos de fallas
más granulares y pormenorizados, mientras se mantiene la compatibilidad con los
tipos de fallas estándar. Por ejemplo, el código de error `Server.Database`
deriva de `Server`. La notación separada por punto indica que el código de la
izquierda es más general que el de la derecha.

## WSDL
Al invocar un procedimiento en un componente, se requiere conocer el nombre de
la función, los parámetros que recibe, y lo que se espera devuelva una vez que
concluya. WSDL (*Web Services Description Language*) es un lenguaje basado en
XML para describir servicios web y su acceso. Los documentos WSDL permiten a las
herramientas de generación de código simplificar la creación de clientes de los
servicios web, y forman una parte fundamental en el proceso de descubrimiento de
los mismos.

Algunas de las ventajas que tiene la utilización de WSDL son:

1. Facilita la creación y mantenimiento de servicios web al proveer de un
enfoque más estructurado para la definición de las interfaces y contratos.
2. Facilita el acceso a los servicios al reducir la programación requerida por
los clientes.
3. Permite implementar cambios, los cuales, gracias al descubrimiento dinámico,
impactan en los clientes de manera automática.

Sin embargo, WSDL no está completo. De momento no posee soporte para el
versionamiento, de manera que los clientes no tienen manera de saber si se está
trabajando con una descripción distinta. Esto puede producir serios problemas si
las interfaces son modificadas.

### El Documento WSDL
La descripción de un servicio web especifica la interfaz abstracta a través de
la cual un cliente se comunica con el proveedor del servicio, así como también
detalles específicos sobre cómo dicha interfaz ha sido implementada. Para ello,
se definen cuatro aspectos: tipos de datos, mensajes, interfaces y servicios.

La estructura básica de un documento WSDL se presenta en el código siguiente:

~~~xml
<?xml version="1.0"?>
<definitions>
    <types>
        <!-- Definición de tipos de datos a ser usados en el servicio. -->
    </types>
    <message>
        <!-- Información a ser transferida. -->
    </message>
    <portType>
        <!-- Conjunto de operaciones. -->
    </portType>
    <binding>
        <!-- Especificación de protocolos y formato de datos. -->
    </binding>
    <service>
        <!-- Especificación de servicios. -->
    </service>
</definitions>
~~~

Un servicio (`<service>`) es un conjunto de puertos. La definición abstracta de
un puerto (`<portType>`) describe al servicio web, y se compone de una colección
de operaciones (`<operation>`) que establecen el intercambio ordenado de
mensajes (`<message>`). Los mensajes están integrados por una o más partes
(`<part>`) de un tipo determinado, y cada una de estas puede verse como los
parámetros en una función. El tipo de dato utilizado por las partes de los
mensajes puede precisarse dentro del mismo documento (`<types>`), o bien
mediante un estándar de tipos de datos, como los esquemas XML. Mediante la
definición específica del puerto (`<binding>`) se determinan los protocolos
usados por este.

### Tipos de Datos
La interoperabilidad entre aplicaciones ejecutándose en diferentes sistemas operativos y
plataformas encuentra su principal dificultad en la representación de los tipos
de datos. Diferentes lenguajes de programación no sólo poseen diferentes
definiciones sobre sus tipos de datos primitivos, sino también cómo son
expresados al ser enviados a través de la red.

A los fines de posibilitar una interoperabilidad entre plataformas, debe existir
un mecanismo mediante el cual el servidor y el cliente acuerden en la
utilización de datos comunes y su representación. En WSDL, el método principal
para definir estas estructuras de datos compartidas es la especificación de
esquema de XML (*XML Schema*) del W3C. Sin embargo, vale aclarar que, al
describir un servicio, es posible utilizar cualquier otra forma de definición de
tipos de datos. Cualquiera sea, tanto el proveedor del servicio como sus
clientes deben acordar y manejarla de igual manera; de otra manera, la
descripción del servicio resulta inútil. Es por este motivo que los autores del
WSDL recomiendan utilizar los esquemas de XML: no dependen de plataforma alguna.

El objetivo de los esquemas es definir una clase de documentos XML. Si los datos
a ser enviados en los mensajes pueden ser representados mediante XML, entonces
estos esquemas pueden utilizarse para describir las reglas que definen a estos
datos.

Profundizar en los esquemas de XML escapa de los fines de este trabajo, sin
embargo, entenderlos es de vital importancia si desea crear servicios complejos
que intercambien estructuras de datos complejas.

### La Interfaz y la Implementación
La **interfaz** de un servicio web no difiere demasiado de la de una clase en la
programación orientada a objetos. Se tienen los mensajes de entrada (conjunto de
parámetros enviados para una operación), mensajes de salida (conjunto de valores
devueltos por la operación), y mensajes de errores (el conjunto de condiciones
de error que pueden darse al se invocada una función). En WSDL, a esta interfaz
se la define en el elemento `<portType>`.

Con WSDL también es posible especificar la **implementación** de la interfaz
dada dentro del elemento `<binding>`. Esta implementación puede dividirse en dos
partes: los *protocolos* y el *servicio*.

Los **protocolos** determinan, entre otras cosas, la forma de comunicación que
usarán los protocolos, y esto puede ser del tipo RPC (`rpc`) o documento
(`document`). RPC (*Remote Procedure Call*) indica que los mensajes SOAP
cumplirán el convenio RPC SOAP, mientras que una comunicación como documento
establece que los mensajes SOAP transportarán XML arbitrario.

El **servicio** establece las operaciones involucradas, la *acción SOAP* (valor
de la cabecera `SOAPAction` cuando se utilice como transporte HTTP) de cada uno,
y los parámetros de entrada y valores devueltos.


[SNELL2001]: http://shop.oreilly.com/product/9780596000950.do "James Snell, Doug Tidwell , Pavel Kulchenko. Programming Web Services with SOAP."
[W3SCHOOLSWS]: http://www.w3schools.com/webservices/default.asp "W3Schools Web Services Tutorial"
