# Anexo C - Reglas de Producción del *SEVAC*

En este anexo se presentan las **reglas de producción** obtenidas como resultado
de la etapa de **formalización** del **SEVAC** (ver capítulo 8).

## BCG

~~~text
IF NOT(bcg-aplicada = TRUE) AND edad < 5 años THEN
    SET aplicar-bcg = TRUE

IF aplicar-bcg = TRUE AND edad < 7 dias THEN
    SET reportar-esquema-normal = TRUE

IF aplicar-bcg = TRUE AND NOT(edad < 7 dias) AND edad < 1 año THEN
    SET reportar-bcg-esquema-alternativo = TRUE

IF aplicar-bcg = TRUE AND NOT(edad < 1 año) THEN
    SET reportar-bcg-fuera-esquema = TRUE
~~~

## Hepatitis B

~~~text
IF edad < 1 mes AND NOT( hb-1-aplicada = TRUE ) THEN
    SET aplicar-hb-1-recien-nacido = TRUE

IF edad > 11 años AND NOT( hb-1-aplicada = TRUE ) THEN
    SET aplicar-hb-1-mayores = TRUE

IF hb-1-aplicada = TRUE AND NOT( hb-2-aplicada = TRUE ) THEN
    SET esquema-hb-2-completo = FALSE

IF esquema-hb-2-completo = FALSE AND edad > 11 años
    AND DIFF( fecha-nacimiento, fecha-inmunizacion-hb-1 ) < 1 mes THEN
        SET chequear-dpthb-2 = TRUE

IF NOT( hb-2c-aplicada = TRUE ) AND NOT( dpthb-2-aplicada = TRUE ) THEN
    SET esquema-dpthb-2-completo = FALSE

IF chequear-dpthb-2 = TRUE AND esquema-dpthb-2-completo = FALSE THEN
    SET continuar-esquema-dpthb-2 = TRUE

IF DIFF( fecha-nacimiento, fecha-inmunizacion-hb-1 ) >= 11 años
    AND esquema-hb-2-completo = FALSE THEN
        SET chequear-diferencia-hb-1 = TRUE

IF DIFF( fecha-inmunizacion-hb-1, fecha-actual ) > 1 mes
    AND chequear-diferencia-hb-1 = TRUE THEN
        SET continuar-esquema-hb-2 = TRUE

IF chequear-diferencia-hb-1 = TRUE AND continuar-esquema-hb-2 = TRUE THEN
    SET aplicar-hb-2 = TRUE

IF aplicar-hb-2 = TRUE AND NOT( edad >= 12 años) THEN
    SET reportar-hb-2-normal = TRUE

IF aplicar-hb-2 = TRUE AND edad >= 12 años AND NOT( edad >= 18 años ) THEN
    SET reportar-hb-2-fuera-esquema = TRUE

IF aplicar-hb-2 = TRUE AND edad >= 18 años THEN
    SET reportar-hb-2-alternativo = TRUE

IF hb-2c-aplicada = TRUE OR dpthb-2-aplicada = TRUE THEN
    SET esquema-dpthb-2-completo = TRUE

IF hb-1-aplicada = TRUE AND hb-2-aplicada = TRUE THEN
    SET esquema-hb-2-completo = TRUE

IF edad < 12 horas AND aplicar-hb-1-recien-nacido = TRUE THEN
    SET reportar-hb-1-normal = TRUE

IF NOT( edad < 12 horas ) AND aplicar-hb-1-recien-nacido = TRUE THEN
    SET reportar-hb-1-fuera-esquema = TRUE

IF NOT( hb-3c-aplicada = TRUE ) AND NOT( dpthb-3-aplicada = TRUE ) THEN
    SET esquema-dpthb-3-completo = FALSE

IF esquema-dpthb-2-completo = TRUE
    AND esquema-dpthb-3-completo = FALSE
    AND NOT( hb-3-aplicada = TRUE ) THEN
        SET completar-esquema-dpthb-3 = TRUE

IF esquema-hb-2-completo = TRUE
    AND DIFF( fecha-inmunizacion-hb-2, fecha-actual ) > 1 mes
    AND NOT( hb-3-aplicada = TRUE ) THEN
        SET completar-esquema-hb-3 = TRUE

IF completar-esquema-dpthb-3 = TRUE OR completar-esquema-hb-3 = TRUE THEN
    SET aplicar-hb-3 = TRUE

IF aplicar-hb-3 = TRUE AND NOT( edad >= 12 años ) THEN
    SET reportar-hb-3-normal = TRUE

IF edad >= 12 años AND aplicar-hb-3 = TRUE AND NOT( edad >= 18 años ) THEN
    SET reportar-hb-3-fuera-esquema = TRUE

IF edad >= 18 años AND aplicar-hb-3 = TRUE THEN
    SET reportar-hb-3-alternativo = TRUE

IF aplicar-hb-1-mayores = TRUE AND NOT( edad >= 12 años ) THEN
    SET reportar-hb-1-dosis-pediatrica = TRUE

IF aplicar-hb-1-mayores = TRUE
    AND edad >= 12 años
    AND NOT( edad >= 18 años ) THEN
        SET reportar-hb-1-fuera-esquema = TRUE

IF aplicar-hb-1-mayores = TRUE AND edad >= 18 años THEN
    SET reportar-hb-1-adultos = TRUE
~~~

## Sabin

~~~text
IF edad >= 2 meses AND NOT( edad >= 4 meses ) THEN
    SET tiene-2-meses = TRUE

IF edad <= 18 años AND NOT( edad >= 2 años ) THEN
    SET tiene-12-18-anios = TRUE

IF tiene-2-meses = TRUE AND NOT( sabin-1-aplicada = TRUE ) THEN
    SET aplicar-sabin-1 = TRUE

IF tiene-12-18-anios = TRUE AND NOT( sabin-1-aplicada = TRUE ) THEN
    SET aplicar-sabin-1-fuera-esquema = TRUE

IF edad >= 4 meses AND NOT( edad >= 6 meses ) THEN
    SET tiene-4-meses = TRUE

IF sabin-1-aplicada = TRUE
    AND DIFF( sabin-1-fecha-inmunizacion, fecha-actual ) >= 1 mes
    AND tiene-4-meses = TRUE
    AND NOT( sabin-2-aplicada = TRUE ) THEN
        SET aplicar-sabin-2 = TRUE

IF sabin-1-aplicada = TRUE
    AND DIFF( sabin-1-fecha-inmunizacion, fecha-actual ) >= 1 mes
    AND tiene-12-18-anios = TRUE
    AND NOT( sabin-2-aplicada = TRUE ) THEN
        SET aplicar-sabin-2-fuera-esquema = TRUE

IF edad >= 6 meses AND NOT( edad >= 18 meses ) THEN
    SET tiene-6-meses = TRUE

IF sabin-2-aplicada = TRUE
    AND tiene-6-meses = TRUE
    AND DIFF( sabin-2-fecha-inmunizacion, fecha-actual ) >= 1 mes
    AND NOT( sabin-3-aplicada = TRUE )
        SET aplicar-sabin-3 = TRUE

IF sabin-2-aplicada = TRUE
    AND tiene-12-18-anios = TRUE
    AND DIFF( sabin-2-fecha-inmunizacion, fecha-actual ) >= 1 mes
    AND NOT( sabin-3-aplicada = TRUE ) THEN
        SET aplicar-sabin-3-fuera-esquema = TRUE

IF edad >= 18 meses AND NOT( edad >= 2 años ) THEN
    SET tiene-18-meses = TRUE

IF sabin-3-aplicada = TRUE
    AND tiene-18-meses = TRUE
    AND DIFF( sabin-3-fecha-inmunizacion, fecha-actual ) >= 6 meses
    AND NOT( sabin-4-aplicada = TRUE ) THEN
        SET aplicar-sabin-4 = TRUE

IF sabin-3-aplicada = TRUE
    AND tiene-12-18-anios = TRUE
    AND DIFF( sabin-3-fecha-inmunizacion, fecha-actual ) >= 6 meses
    AND NOT( sabin-4-aplicada = TRUE ) THEN
        SET aplicar-sabin-4-fuera-esquema = TRUE

IF sabin-4-aplicada = TRUE
    AND edad >= 5 años
    AND edad <= 6 años
    AND edad-inmunizacion-sabin-4 < 2 años
    AND NOT( sabin-99-aplicada = TRUE ) THEN
        SET aplicar-sabin-99 = TRUE
~~~

## Quíntuple (Pentavalente)

~~~text
IF edad >= 2 meses AND edad < 5 años THEN
    SET tiene-edad-dpthb = TRUE

IF tiene-edad-dpthb = TRUE AND NOT( dpthb-1-aplicada = TRUE ) THEN
    SET aplicar-dpthb-1 = TRUE

IF aplicar-dpthb-1 = TRUE AND NOT( edad >= 4 meses ) THEN
    SET reportar-dpthb-1-normal = TRUE

IF aplicar-dpthb-1 = TRUE AND edad >= 4 meses THEN
    SET reportar-dpthb-1-alternativo = TRUE

IF reportar-dpthb-1-normal = TRUE
    OR reportar-dpthb-2-normal = TRUE
    OR reportar-dpthb-3-normal = TRUE THEN
        SET reportar-dpthb-esquema-normal = "Aplicación Óptima"

IF reportar-dpthb-1-alternativo = TRUE
    OR reportar-dpthb-2-alternativo = TRUE
    OR reportar-dpthb-3-alternativo = TRUE THEN
    SET reportar-dpthb-esquema-alternativo = "Aplicación Esquema Alterantivo

IF dpthb-1-aplicada = TRUE
    AND tiene-edad-dpthb = TRUE
    AND edad >= 4 meses
    AND DIFF( fecha-inmunizacion-dpthb-1, fecha-actual ) >= 1 meses
    AND NOT( dpthb-2-aplicada = TRUE ) THEN
        SET aplicar-dpthb-2 = TRUE

IF aplicar-dpthb-2 = TRUE AND NOT( edad >= 6 meses ) THEN
    SET reportar-dpthb-2-normal = TRUE

IF aplicar-dpthb-2 = TRUE AND edad >= 6 meses THEN
    SET reportar-dpthb-2-alternativo = TRUE

IF tiene-edad-dpthb = TRUE
    AND dpthb-2-aplicada = TRUE
    AND edad >= 6 meses
    AND DIFF( fecha-inmunizacion-dpthb-2, fecha-actual ) >= 1 meses
    AND NOT( dpthb-3-aplicada = TRUE ) THEN
        SET aplicar-dpthb-3 = TRUE

IF aplicar-dpthb-3 = TRUE AND edad <= 18 meses THEN
    SET reportar-dpthb-3-normal = TRUE

IF aplicar-dpthb-3 = TRUE AND NOT( edad <= 18 meses ) THEN
    SET reportar-dpthb-3-alternativo = TRUE
~~~

## Cuádruple

~~~text
IF edad >= 15 meses
    AND edad <= 18 meses
    AND NOT( dpth-99-aplicada = TRUE )
    AND dpthb-3-aplicada = TRUE
    AND DIFF( fecha-inmunizacion-dpthb-3, fecha-actual ) >= 6 meses THEN
        SET aplicar-dpth-99 = TRUE
~~~

## Neumococo Conjugada (13 Valente)

~~~text
IF edad >= 2 meses AND edad < 18 meses THEN
    SET tiene-edad-neumo = TRUE

IF tiene-edad-neumo = TRUE AND NOT( neumo-1-aplicada = TRUE ) THEN
    SET aplicar-neumo-1 = TRUE

IF tiene-edad-neumo = TRUE
    AND neumo-1-aplicada = TRUE
    AND DIFF( fecha-inmunizacion-neumo-1, fecha-actual ) >= 2 meses
    AND NOT( neumo-2-aplicada = TRUE ) THEN
        SET aplicar-neumo-2 = TRUE

IF tiene-edad-neumo = TRUE
    AND neumo-2-aplicada = TRUE
    AND DIFF( fecha-inmunizacion-neumo-2, fecha-actual ) >= 2 meses
    AND NOT( neumo-3-aplicada = TRUE ) THEN
        SET aplicar-neumo-3 = TRUE
~~~

## Triple Viral

~~~text
IF edad = 12 meses AND NOT( srp-1-aplicada = TRUE ) THEN
    SET aplicar-srp-1 = TRUE

IF srp-1-aplicada = TRUE
    AND edad >= 5 años
    AND edad <= 6 años
    AND NOT( srp-2-aplicada = TRUE )
    AND NOT( sr-0-aplicada = TRUE ) THEN
        SET aplicar-srp-2 = TRUE

IF edad = 11 años
    AND NOT( srp-2-aplicada = TRUE )
    AND NOT( sr-0-aplicada = TRUE ) THEN
        SET aplicar-srp-2-esquema-alternativo = TRUE

~~~

## Doble Viral

~~~text
IF edad >= 18 años
    AND NOT( srp-2-aplicada = TRUE )
    AND NOT( sr-0-aplicada = TRUE ) THEN
        SET aplicar-sr-0 = TRUE
~~~

## Triple Bacteriana Celular

~~~text
IF edad >= 5 años
    AND edad <= 6 años
    AND dpth-99-aplicada = TRUE
    AND NOT( dpt-99-aplicada = TRUE ) THEN
        SET aplicar-dpt-99 = TRUE
~~~

## Triple Bacteriana Acelular

~~~text
IF edad = 11 años
    AND dpt-99-aplicada = TRUE
    AND NOT( dpta-99-aplicada = TRUE ) THEN
        SET aplicar-dpta-99 = TRUE
~~~

## Hepatitis A

~~~text
IF edad = 12 meses
    AND NOT( ha-0-aplicada = TRUE ) THEN
        SET aplicar-ha-0 = TRUE
~~~

## VPH

~~~text
IF edad = 11 años AND NOT( vph-1-aplicada = TRUE ) THEN
    SET aplicar-vph-1 = TRUE

IF edad = 11 años
    AND vph-1-aplicada = TRUE
    AND DIFF( fecha-inmunizacion-vph-1, fecha-actual ) >= 1 meses
    AND NOT( vph-2-aplicada = TRUE ) THEN
        SET aplicar-vph-2 = TRUE

IF edad = 11 años
    AND vph-2-aplicada = TRUE
    AND DIFF( fecha-inmunizacion-vph-2, fecha-actual ) >= 3 meses
    AND NOT( vph-3-aplicada = TRUE ) THEN
        SET aplicar-vph-3 = TRUE
~~~

