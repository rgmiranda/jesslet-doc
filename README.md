# JESSLET: Jess Como un Servicio

El objetivo principal de este proyecto es que sirva de repositorio para los
capítulos de mi trabajo final de graduación denominado **JESSLET: Jess como
un servicio**.

## Índice de Contenidos

El **capítulo 0** (cero) es el **[anteproyecto](00-anteproyecto.md
"Anteproyecto")**. Luego aparecen los capítulos del trabajo final, los
cuales tienen la siguiente estructura:

1. [Resumen e Introducción](00-introduccion.md "Resumen e Introducción")
2. [El Problema](01-el-problema.md "Especificación del problema"). Se explica la
situación problemática y cómo pretenderá ser abordada por la solución propuesta.
3. **Marco Teórico**
    - [Sistemas Expertos](02-sistemas-expertos.md"Introducción a los Sistemas
	Expertos"). Una breve introducción a los *sistemas expertos basados en
	reglas de inferencias* y los conceptos centrales dentro esta disciplina
	que se manejan en el proyecto.
4. **Marco Tecnológico**
    - [Jess](03-jess.md "Introducción al Shell Jess"). Presentación del shell
	*Jess*, su funcionamiento e implementación.
    - [Servicios Web](04-servicios-web.md "Introducción a los Servicios Web").
	Presentación de los servicios web y los estándares *SOAP* y *WSDL* usados en
	su especificación y consumo.
5. **Marco Metodológico**
    - [Programación Extrema](05-xp.md "Introducción a la Programación
	Extrema"). Introducción a la *XP*, sus reglas y su importancia.
6. **Marco Empírico**
    - [Métricas de Cohesión y Acoplamiento](06-metricas.md)
7. [Desarrollo del Jesslet](07-jesslet.md). Descripción del desarrollo del
**Jesslet** mediante las prácticas recomendadas de la **Programación Extrema**.
8. [SEVAC - Sistema de Sugerencia de Vacunas](08-vacunas-jess.md). Elaboración,
a modo de ejemplo, de un sistema experto sencillo sobre Jess para la
recomendación de inmunizaciones.
9. [Experimentación con el Jesslet](09-experimentacion-jesslet.md).
Experimentación con el servicio para corroborar o refutar las hipótesis
planteadas. Implementación del *sistema experto de inmunizaciones* sobre el
Jesslet desde otras plataformas.
10. [Conclusiones](10-conclusiones.md). Conclusiones finales del trabajo.
11. [Anexo A](11-anexoo-a-jess.md). El Lenguaje Jess.
12. [Anexo B](12-anexo-b-soap.md). SOAP.
13. [Anexo C](13-anexo-c-reglas-sevac.md). Reglas de Producción del SEVAC.

## Aclaraciones y otras notas

* Este documento está vivo y en continua evolución. El índice se va actualizando
a medida que se crean los capítulos.
* El capítulo titulado *"El Problema"* es un extracto del anteproyecto. Se
encuentra en proceso de ampliación y mejora.
* Los capítulos son los documentos en Markdown en la raíz. Nada raro.
* En `/imgs` se pueden encontrar las imágenes que utilizo dentro de éstos.
* En `/bin` hay algunos documentos ODT que serán actualizados a su versión final
en base al contenido de los Markdown.
* En `/docs` se irán guardando los archivos que sirvan como documentación de
todo lo que vaya apareciendo dentro de los capítulos.
* Cualquier aporte es bienvenido.

## Licencia

![Licencia Creative Commons](https://i.creativecommons.org/l/by/4.0/80x15.png)
Esta obra está bajo una 
[Licencia Creative Commons Atribución 4.0 Internacional](http://creativecommons.org/licenses/by/4.0/)

[jess]:http://jessrules.com/ "Jess"

