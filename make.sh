#!/bin/sh

helptext () {
    echo "Programa para compilar los archivos Markdonw (.md) a OpenDocument (.odt)."
    echo "Uso: $(basename "$0") [-s SOURCE] [-d DESTINATION] [-h]"
    echo "    -h        Muestra este texto de ayuda"
    echo "    -s        Directorio en donde se encuentran los fuentes"
    echo "    -d        Directorio de destino de los documentos generados"
}

SRC="."
DST="bin"
HELP=0

while getopts :hd:s: OP
do
    case $OP in
        s)
            SRC="$OPTARG"
            ;;
        d)
            DST="$OPTARG"
            ;;
        h)
            HELP=1
            ;;
        *)
            # unknown option
            ;;
    esac
done

RED='\033[0;31m'
NC='\033[0m'
BLUE='\033[1;34m'

if [ $HELP = 1 ]
then
    helptext;
else
    for MDFILE in $SRC/*.md
    do
        printf "Procesando ${BLUE}'$MDFILE'${NC}...\n"
        FNAME=$(basename "$MDFILE")
        FEXT="${FNAME##*.}"
        FNAME="${FNAME%.*}"
        pandoc -f markdown -t docx -o ${DST}/${FNAME}.docx $MDFILE
    done
fi
